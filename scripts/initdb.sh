#! /bin/bash
cd "$(dirname "$0")"
source ../instance/env
export PYTHONPATH="$PWD/.."
#export FLASK_APP=flaskr
mysql -v -u $DB_USER --password=$DB_PASS $DB_NAME < drop_tables.sql
flask --app flaskr init-db
mysql -v -u $DB_USER --password=$DB_PASS $DB_NAME < seeds.sql
python3 importevents.py
python3 importgames.py
cd ..
rm -rf uploads/*
mkdir uploads/256
mkdir uploads/128
mkdir uploads/64
mkdir uploads/32
mkdir uploads/thumb
mkdir uploads/base
mkdir uploads/orig
