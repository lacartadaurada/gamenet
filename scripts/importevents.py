from flaskr import create_app, db
app = create_app()
app.app_context()
app.app_context().push()

from datetime import datetime

from flaskr.models import Event

def create_event(title, date, end_date, venue, notes):
    params = {'title': title, 'user_id': 1, 'group_id': 1, 'calendar_id': 1, 'date_start': date, 'date_end': date, 'venue_id': 1}
    if notes:
        params['description'] = notes
    if 'torneo' in title.lower() or 'torneig' in title.lower():
        params['event_type'] = 'tournament'
    if 'jornada' in title.lower():
        params['event_type'] = 'event'
    if 'fiesta' in title.lower():
        params['event_type'] = 'social'
    if 'draft' in title.lower():
        params['event_type'] = 'league'
    if 'presentacion' in title.lower():
        params['event_type'] = 'presentation'
    params['slug'] = Event.generate_slug(params['title'])
    #params['venue_id'] = ''
    new_event = Event(**params)

    db.session.add(new_event)
    db.session.commit()


with open("events.txt") as f:
    for line in f.readlines():
        split_line = line.strip("\n").split("\t")
        #print(split_line)
        title = split_line[0].strip()
        date = split_line[1].strip()
        venue = split_line[2].strip()
        notes = ""
        if len(split_line) > 3:
            notes = split_line[3]
        #print(date, len(date.split("-")))
        if len(date.split("-")) == 2:
            split_date = date.split("-")
            date = datetime.strptime(split_date[0], '%Y')
            end_date = datetime.strptime(split_date[1], '%Y')
        else:
            date = datetime.strptime(date, '%d-%m-%Y')
            end_date = date
        create_event(title, date, end_date, venue, notes)
        #print(date)
    print("history imported ok")



#Event.create()

#db.create_all()

