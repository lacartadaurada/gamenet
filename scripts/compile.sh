#! /bin/bash
cd "$(dirname "$0")"
cd ..
export PYTHONPATH=$PWD

#rm /var/www/lacarta/js/*
#rm /var/www/lacarta/css/*


python3 scripts/tpl_compile.py

hugo --environment production --configDir hugo/config/ -D $*

cp /var/www/lacarta/flask/index.html flaskr/templates/carta.html
cp /var/www/lacarta/index.html flaskr/templates/carta-main.html

