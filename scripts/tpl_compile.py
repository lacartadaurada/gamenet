import os

dest_file = "src/js/all-tpls.js"
tpl_dir = 'src/templates/'


def export_template(tpl_file):
    res = "AngularTemplates['%s'] = " % ("/templates/"+tpl_file)
    with open(os.path.join(tpl_dir, tpl_file)) as f:
        res += "`" + f.read() + "`;\n"
    return res

def main():
    tpls = []
    data = "export default AngularTemplates = {};\n"


    for (root,dirs,files) in os.walk(tpl_dir,topdown=True):
        for file in files:
            if file.endswith(".html"):
                tpl_file = os.path.join(root, file)
                
                tpl_file = tpl_file.replace(tpl_dir, "")
                tpls.append(tpl_file)
                print("* add", tpl_file)
                export_template(tpl_file)
    for tpl in tpls:
        data += export_template(tpl)

    os.unlink(dest_file)
    with open(dest_file, "w") as f:
        f.write(data)



if __name__ == '__main__':
    main()
