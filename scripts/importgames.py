from flaskr import create_app, db
app = create_app()
app.app_context()
app.app_context().push()

from datetime import datetime

from flaskr.models import Game

def parse_players(par, params, key):
    res = [0,0]
    par.replace(",+", "")
    par = par.replace(".","-")
    par = par.replace(",","-")
    players_split = par.strip().strip('+').split("-")

    res[0] = int(players_split[0])
    if len(players_split) > 1:
        res[1] = int(players_split[1])
    else:
        res[1] = int(players_split[0])
    params['min_'+key] = res[0]
    params['max_'+key] = res[1]
    return res

def create_game(game, players, best, weight, time, notes):
    params = {'title': game.title(), 'user_id': 1, 'group_id': 1}
    if notes:
        params['description'] = notes
    if players:
        parse_players(players, params, 'players')
    if best:
        parse_players(best, params, 'best_players')
    if weight:
        params['difficulty'] = float(weight)
    if time:
        time_split = time.split("-")
        params['min_duration'] = int(time_split[0])
        if len(time_split) == 2:
            params['max_duration'] = int(time_split[1])
        else:
            params['max_duration'] = int(time_split[0])
    params['permissions'] = 'group'
    params['visibility'] = 'public'
    params['slug'] = Game.generate_slug(params['title'], raw=True)
    if len(list(db.session.execute(db.select(Game).where(Game.slug == params['slug'])).scalars())) > 0:
        print("Already present", params['slug'])
        return 

    new_game = Game(**params)

    db.session.add(new_game)
    db.session.commit()


#6 (['Juego', 'Jugadores', 'Best', 'Weight', 'Time', '\n'],)
# Stone Age       2-4     4       2.47    60-90
def process_line(game, players, best, weight, time, extras):
    game = game.strip()
    if game and not game.startswith("+"):
        create_game(game, players, best, weight, time, extras)
    

with open('allgames.txt') as f:
    for line in f.readlines():
        split_line = line.split("\t")
        if len(split_line) == 6:
            process_line(*split_line)
