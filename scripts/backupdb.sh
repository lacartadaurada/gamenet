#! /bin/bash
cd "$(dirname "$0")"
cd ..
source instance/env
TS=`date +%s`
export DEST=backup-$TS.sql
#export PYTHONPATH="$PWD/.."
#export FLASK_APP=flaskr
mkdir -p $BACKUP_DIR
mysqldump -u $DB_USER --password=$DB_PASS $DB_NAME > $BACKUP_DIR/$DEST
cd $BACKUP_DIR
gzip $DEST
echo "$BACKUP_DIR/$DEST created ok"
