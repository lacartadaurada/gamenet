from flaskr import create_app, db
app = create_app()
app.app_context()
app.app_context().push()

from datetime import datetime
import os

from flaskr.models import File
from PIL import Image

def save_thumbnails(orig_name, thumb_name):
  base_path = app.config['UPLOAD_DIR']

  mangled_thumb_name = thumb_name.rsplit(".", 1)[0] + "." + orig_name.rsplit(".", 1)[1]
  base_thumb_path = os.path.join(base_path, 'thumb', mangled_thumb_name)

  im = Image.open(base_thumb_path)
  rgb_im = im.convert("RGB")

  for size in [256, 128, 64, 32]:
      im_process = rgb_im.copy()
      im_sized = im_process.resize((size, size), Image.Resampling.LANCZOS)
      im_sized.save(os.path.join(base_path, str(size), thumb_name), quality=90)

all_files = db.session.scalars(db.select(File).where(File.thumbnail != None))

for afile in all_files:
    if afile.thumbnail:
        print("process", afile.name, afile.path, afile.thumbnail)
        save_thumbnails(afile.path, afile.thumbnail)


