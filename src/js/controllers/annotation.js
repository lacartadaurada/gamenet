export default angular.module('myApp').controller('AnnotationController',
  ['$scope', 'AnnotationResource', '$location', 'LoginService',
    function($scope, AnnotationResource, $location, LoginService) {

      $scope.annotationList = {};
      $scope.annotations = [];
      $scope.annotationCounts = {};
      $scope.annotationUsers = {};
      let loginData = LoginService.scope;

      $scope.viewProfile = function(user) {
        if (loginData.isLoggedIn && user.id == loginData.whoami.id) {
          $location.path('/profile/');
        } else {
          $location.path('/profile/'+user.id);
        }
      };
      let annotationTranslations = {
        'like': 'le gusta',
        'wannaplay': 'quiere jugar',
        'have': 'lo tiene'
      };
      let annotationTranslationsMe = {
        'like': 'me gusta',
        'wannaplay': 'quiero jugar',
        'have': 'lo tengo'
      };
      var gotAnnotations = function(res) {
        $scope.annotations = res;
        $scope.annotationCounts = {'like': 0, 'wannaplay': 0, 'have': 0};
        $scope.annotationUsers = {};
        res.forEach((annotation) => {
          let user = annotation.user;
          if (!$scope.annotationUsers[user.id]) {
            $scope.annotationUsers[user.id] = {user: annotation.user, annotations: []}
          }
          let translation = annotationTranslations;

          if (loginData.isLoggedIn) {
            translation = annotationTranslationsMe;
          } else {
            translation = annotationTranslations;
          }

          $scope.annotationUsers[user.id].annotations.push(translation[annotation.type]);
          $scope.annotationCounts[annotation.type] += 1;
        });
      };
      var gotUserAnnotations = function(res) {
        res.forEach(function(annotation) {
          onAnnotationAdded(annotation);
        });
      };

      var updateAllAnnotations = function() {
        return AnnotationResource.search({entity: {id: $scope.schema.model.id, type: $scope.schema.type}}).$promise.then(gotAnnotations);
      }

      var loadAnnotations = function() {
        let model = $scope.schema.model;
        // user annotations on this entity
        if (loginData.isLoggedIn) {
          AnnotationResource.query({entityType: $scope.schema.type, entityId: model.id}).$promise.then(gotUserAnnotations);
        }
        // entity annotations from all users
        return updateAllAnnotations();
      };

      var onAnnotationAdded = function(res) {
        $scope.annotationList[res.type] = res;
        $scope.annotationList = angular.copy($scope.annotationList);
        updateAllAnnotations();
      };

      var onAnnotationRemoved = function(annotationType) {
        delete $scope.annotationList[annotationType];
        $scope.annotationList = angular.copy($scope.annotationList);
        updateAllAnnotations();
      };

      let prepareArgs = function(type, obj, objType) {
        if (!obj) {
          obj = $scope.schema.model;
        }
        if (!objType) {
          objType = $scope.schema.type;
        }
        return { entityId: obj.id,
                 entityType: objType,
                 type: type };
      }

      $scope.deleteAnnotation = function(type, obj, objType) {
        let args = prepareArgs(type, obj, objType);

        args.annotationType = args.type;
        AnnotationResource.remove(args).$promise.then(() => onAnnotationRemoved(args.type));
      }

      $scope.addAnnotation = function(type, obj, objType) {
        let args = prepareArgs(type, obj, objType);

        let newElmt = new AnnotationResource(args);
        let promise = newElmt.$save().then(onAnnotationAdded);
      }

      $scope.toggleAnnotation = function(type, obj, objType) {
        if ($scope.annotationList[type]) {
          $scope.deleteAnnotation(type, obj, objType);
        } else {
          $scope.addAnnotation(type, obj, objType);
        }
      };

      $scope.$watch($scope.schema.modelName+'.id', function(val) {
        if (val) {
          loadAnnotations();
        }
      });

}]);
