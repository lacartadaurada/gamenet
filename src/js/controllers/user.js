export default angular.module('myApp').controller('UserController',
  ['$scope', 'UserResource', '$location', '$stateParams', 'LoginService', '$sce', 'AnnotationResource', 'GroupResource',
    function($scope, UserResource, $location, $stateParams, LoginService, $sce, AnnotationResource, GroupResource) {

      $scope.login = LoginService.scope;
      $scope.userModel = {};

      $scope.currentForm = 'edit';
      $scope.schema = {type: 'user', modelName: 'userModel', model: $scope.userModel, getResource: () => UserResource};
      $scope.passwordModel = {};
      $scope.passwordStatusOk = undefined;
      $scope.passwordStatusNok = undefined;

      /* Buttons */

      $scope.cancelEdit = function() {
        $scope.viewProfile();
      };

      $scope.viewProfile = function() {
        $location.path("/profile/");
      };

      $scope.editProfile = function() {
        $location.path("/profile/edit/");
      };

      $scope.editAvatar = function() {
        $location.path("/profile/avatar/");
      };

      $scope.openUrl = function(url) {
        $location.path(url);
      };

      $scope.setSection = function(section, $event) {
        $event.preventDefault();
        $scope.currentForm = section;
      };

      /* Annotations */

      var getUrl = function(annotation) {
        var baseUrl;
        if (annotation.entity.type == 'event') {
          baseUrl = '/actividades/';
        } else if (annotation.entity.type == 'game') {
          baseUrl = '/juegos/';
        }
        return baseUrl+annotation.entity.slug
      };

      var gotAnnotations = function(annotations) {
        $scope.annotationsDict = {};
        $scope.userAnnotations = annotations;

        annotations.forEach((annotation) => {
          if (!$scope.annotationsDict[annotation.type]) {
            $scope.annotationsDict[annotation.type] = [];
          }
          $scope.annotationsDict[annotation.type].push(annotation);
          annotation.url = getUrl(annotation);
        });
      };

      /* Load Element */

      let gotGroups = function(groups) {
        $scope.groups = groups;
      }

      var gotUser = function(user) {
        if ($scope.mode == 'self') {
          $scope.profileTitle = 'Tu Perfil';
        } else {
          $scope.profileTitle = 'Perfil de ' + user.name;
        }
        $scope.userModel = angular.copy(user);
        $scope.schema.model = $scope.userModel;
        $scope.userModel.privateName = user.privateName || "";
        $scope.userModel.trustedAvatar = user.avatar;
        if (user.motto && user.motto.trim()) {
          let splitMotto = user.motto.trim().split("\n");
          let trustedMotto = splitMotto.join("</p><p>");
          $scope.userModel.trustedMotto = $sce.trustAsHtml('<p>'+trustedMotto+'</p>');
        }
        AnnotationResource.search({userId: user.id}).$promise.then(gotAnnotations);
        GroupResource.search({userId: user.id}).$promise.then(gotGroups)
      };

      /* Password */

      var onPasswordChange = function(res, form) {
        $scope.passwordStatusOk = true;
        $scope.passwordModel = {};
        form.$setPristine();
      };

      var onPasswordError = function(err) {
        $scope.passwordStatusNok = (err.data && err.data.error) || err.statusText;
      };

      $scope.submitPassword = function(form) {
        if (!form.$valid) {
          return;
        }

        $scope.passwordStatusOk = undefined;
        $scope.passwordStatusNok = undefined;

        LoginService.password_change($scope.passwordModel).then(res => onPasswordChange(res, form), onPasswordError);
      };

      /* Edit */

      var onUserSaved = function(user) {
        $scope.viewProfile();
      };

      $scope.submit = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          console.log($scope.userModel);
          return;
        }

        let args = angular.copy($scope.userModel);
        delete args['id'];
        delete args['trustedMotto'];
        let newCard = new UserResource(args);

        newCard.$save().then(onUserSaved);
      };

      /* Initialization */

      if ($stateParams.userId) {
        $scope.mode = 'other';
        UserResource.get({userId: $stateParams.userId}).$promise.then(gotUser);
      } else {
        $scope.mode = 'self';
        UserResource.get().$promise.then(gotUser);
      }



    }]);
