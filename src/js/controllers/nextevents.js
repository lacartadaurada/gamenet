export default angular.module('myApp').controller('NexteventsController', ['$scope', '$location', '$rootScope', '$window', function($scope, $location, $rootScope, $window) {

  var handleRoute = function() {
    let currentPath = $location.path();
    if (currentPath == '/calendario/') {
      if ($scope.section != 'calendar')
        $scope.section = 'calendar';
    } else if (currentPath == '/actividades/') {
      if ($scope.section != 'listing')
        $scope.section = 'listing';
    } else {
      $window.location.href = currentPath;
    }
  };

  handleRoute();

  $rootScope.$on("$locationChangeStart", function(event, next, current) {
    handleRoute();
    // handle route changes
  });

  $scope.setSection = function(section) {
    $scope.section = section;
    if (section == 'calendar') {
      $location.path('/calendario/');
    } else {
      $location.path('/actividades/');
    }
  };
}]);

