export default angular.module('myApp').controller('GamesAnnotationsController',
  ['$scope', 'AnnotationResource',
    function($scope, AnnotationResource) {

      /* Annotations */
      let onAnnotationDeleted = function(gameId, type) {
        delete $scope.annotationList[gameId][type];
        // need to copy again whole game list so directive will get notified
        $scope.annotationList[gameId] = angular.copy($scope.annotationList[gameId]);
      };
      let onAnnotationCreated = function(annotation) {
        addNewAnnotation(annotation);
        // need to copy again whole game list so directive will get notified
        $scope.annotationList[annotation.entity.id] = angular.copy($scope.annotationList[annotation.entity.id]);
      };
      let addNewAnnotation = function(annotation) {
        if (!$scope.annotationList[annotation.entity.id]) {
          $scope.annotationList[annotation.entity.id] = {};
        }
        $scope.annotationList[annotation.entity.id][annotation.type] = annotation;
      }

      let deleteAnnotation = function(args) {
        args.annotationType = args.type;
        AnnotationResource.remove(args).$promise.then(() => onAnnotationDeleted(args.entityId, args.type));
      }

      let addAnnotation = function(args) {
        let newElmt = new AnnotationResource(args);
        let promise = newElmt.$save().then(onAnnotationCreated);
      }

      $scope.toggleAnnotation = function(gameId, action, $event) {
        $event.stopPropagation();
        let type = 'game';
        let args = { entityId: gameId,
                 entityType: type,
                 type: action };

        if ($scope.annotationList[gameId] && $scope.annotationList[gameId][action]) {
          deleteAnnotation(args);
        } else {
          addAnnotation(args);
        }
      };

      let gotUserAnnotations = function(annotations) {
        $scope.annotationList = {};
        annotations.forEach((annotation) => {
          addNewAnnotation(annotation);
        });
      };

      let loginReadySubscribed;
      let getAllUserAnnotations = function() {
        if ($scope.login.loginReady) {
          if ($scope.login.isLoggedIn) {
            AnnotationResource.search({userId: $scope.login.whoami.id, entity: {type: 'game'}}).$promise.then(gotUserAnnotations);
          }
        } else if (!loginReadySubscribed) {
          loginReadySubscribed = $scope.$watch('login.loginReady', function(val) {
            if (val) {
              loginReadySubscribed(); // unsubscribes
              if ($scope.login.isLoggedIn) {
                AnnotationResource.search({userId: $scope.login.whoami.id, entity: {type: 'game'}}).$promise.then(gotUserAnnotations);
              }
            }
          });
        }
      };

      let determineResource = function() {
        getAllUserAnnotations();
      };

      determineResource();
    }]);
