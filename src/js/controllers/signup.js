export default angular.module("myApp").controller('SignupController', ['$scope', '$window', 'LoginService', '$location', 'Utils',
  function($scope, $window, LoginService, $location, Utils) {
  var defaultData = {username: '', password: '', password_confirm: '', email: ''};

  $scope.whoami = {username: ''}
  $scope.status = "";

  $scope.cancelSignup = function() {
    $window.location.href = "/trastienda/";
  };

  var onError = function(err) {
    $scope.status = Utils.getErrorMessage(err);
    console.log(err)
  };

  $scope.reset = function() {
    if ($scope.signupForm) {
      $scope.signupForm.$setPristine();
    }
    $scope.signupModel = angular.copy(defaultData);

    LoginService.whoami().then((data) => {
      $scope.whoami = angular.copy(data);
    });
  };

  var onRegistered = function(user) {
    //$location.path('/profile/');
    $window.location.href = "/profile/";
  };

  $scope.submit = function(form) {
    if (!form.$valid) {
      return;
    }
    LoginService.register($scope.signupModel).then(onRegistered, onError);
  }

  $scope.reset();
}]);

