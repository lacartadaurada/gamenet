export default angular.module('myApp').controller('LoginController', ['$scope', 'LoginService', '$window', '$location', 'Utils',
  function($scope, LoginService, $window, $location, Utils) {
  var loginData = {user_id: '', password: ''};
  $scope.calendarSection = false;

  $scope.login = LoginService.scope;
  $scope.status = "";

  var onLogin = function(data) {
    $scope.reset();
    $window.location.href = "/trastienda/";
  };

  var onError = function(err) {
    $scope.status = Utils.getErrorMessage(err);
    console.log(err)
  };

  $scope.cancelLogin = function() {
    $window.location.href = "/trastienda/";
  };

  $scope.reset = function() {
    if ($scope.loginForm)
      $scope.loginForm.$setPristine();

    $scope.loginModel = angular.copy(loginData);
  };

  $scope.submit = function(form) {
    if (!form.$valid) {
      return;
    }
    LoginService.login($scope.loginModel).then(onLogin, onError);
  };

  $scope.logout = function() {
    LoginService.logout().then(() => $scope.reset(), onError);
  }

  $scope.reset();
}]);

