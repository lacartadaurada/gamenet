export default angular.module('myApp').controller('GameController',
  ['$scope', 'GameResource', '$stateParams', 'LoginService', '$sce', 'Modals', 'Localization', 'Utils',
    function($scope, GameResource, $stateParams, LoginService, $sce, Modals, Localization, Utils) {
      $scope.status = "";
      $scope.isEdit = false;
      $scope.origModel = undefined; // to keep original on edits

      $scope.login = LoginService.scope;
      $scope.schema = {type: 'game', modelName: 'gameModel', model: $scope.gameModel, getResource: () => GameResource};

      $scope.ready = false;
      $scope.sortGamesBy = 'slug';
      $scope.filters = {};
      $scope.searchReversed = false;

      let blueprint = {title: "", description: "", rating: 0.0,
                       visibility: "public", players: {min: 0, max: 0},
                       best: {min: 0, max: 0}, duration: {min: 0, max: 0},
                       editable: true, permissions: 'user'};

      $scope.setSortBy = function(key) {
        if ($scope.sortGamesBy == key) {
          $scope.searchReversed = !$scope.searchReversed;
        } else {
          $scope.sortGamesBy = key;
          $scope.searchReversed = false;
        }
      };

      let deregister = $scope.$on('$locationChangeSuccess', function(evt) {
        determineResource();
        $scope.reset();
      });


      /* Main actions */

      $scope.showCover = function() {
        Modals.openModal('coverModal', $scope.gameModel.title, "");
      };

      let onGameDeleteSuccess = function(gameId) {
        Utils.forceOut("/juegos/", deregister);
      };

      $scope.deleteGame = function(game) {
        Modals.openModal('confirmModal', 'Borrar juego', "Vas a borrar el juego '"+$scope.gameModel.title+"', seguro??").then((accepted) => {
          if (accepted) {
            GameResource.remove({gameId: game.id}).$promise.then(() => onGameDeleteSuccess(game.id));
          }
        });
      };

      $scope.editAvatar = function() {
        Utils.softOut("/juegos/"+$scope.gameModel.slug+"/avatar", deregister);
      };

      $scope.viewGame = function(gameId) {
        gameId = gameId || $scope.gameModel.slug
        Utils.softOut("/juegos/"+gameId, deregister);
      };

      $scope.editGame = function() {
        $scope.gameAction = 'edit';
        Utils.softOut("/juegos/"+$scope.gameModel.slug+'/edit', deregister);
      };

      $scope.cancelEdit = function() {
        if ($scope.gameAction == 'edit') {
          $scope.gameModel = angular.copy($scope.origModel);
          $scope.schema.model = $scope.gameModel;
        }
        Utils.backOut(deregister);
      };


      $scope.newGame = function() {
        //$scope.gameModel = angular.copy(cal);
        //$scope.gameAction = 'new';
        Utils.softOut("/juegos/new/", deregister);
      };

      $scope.selectGame = function(game) {
        $scope.viewGame(game.slug);
      };

      /* Filters */
      $scope.checkFilter = function(game) {
        if ($scope.filters.title) {
          if (game.title.toLowerCase().indexOf($scope.filters.title.toLowerCase()) != -1) {
          } else {
            return false;
          }
        }
        if ($scope.filters.players) {
          let nPlayers = $scope.filters.players;
          if (nPlayers < game.players.min || nPlayers > game.players.max) {
            return false;
          }
        }
        if ($scope.filters.best) {
          let nPlayers = $scope.filters.best;
          if (nPlayers < game.best.min || nPlayers > game.best.max) {
            return false;
          }
        }
        if ($scope.filters.difficulty) {
          let filter = $scope.filters.difficulty.trim();
          let inverse = false;
          if (filter.indexOf('>') != -1) {
            inverse = true;
            filter = filter.replace(">", "").trim()
          }
          let split = filter.split('-');
          let maxDifficulty = split[0];
          let minDifficulty = split[0];
          if (split.length > 1) {
            maxDifficulty = split[1];
            if (game.difficulty > maxDifficulty || game.difficulty < minDifficulty) {
              return false;
            }
          } else if (inverse) {
            if (game.difficulty <= maxDifficulty) {
              return false;
            }
          } else {
            if (game.difficulty >= maxDifficulty) {
              return false;
            }
          }
        }
        if ($scope.filters.duration) {
          let split = $scope.filters.duration.split('-');
          let maxDuration = split[0];
          let minDuration = split[0];
          if (split.length > 1) {
            maxDuration = split[1];
            if (game.duration.max > maxDuration || game.duration.min < minDuration) {
              return false;
            }
          } else {
            if (game.duration.max > maxDuration) {
              return false;
            }
          }
        }
        return true;
      };

      $scope.updateFilters = function(sel) {
      };
       /* End Filters */

      let gotGames = function(games) {
        $scope.ready = true;
        $scope.gameList = games;
      };
      let loadGames = function() {
        return GameResource.query({}).$promise.then(gotGames);
      };

      let determineResource = function() {
        let action = $stateParams.action;

        $scope.gameAction = action;

        $scope.actionTitle = Localization.actionTitles.game[action];

        if (action == 'list') {
          loadGames();
        }

        if ($stateParams.gameId) {
          $scope.gameId = $stateParams.gameId;
        }
      };

      let getGame = function(gameId) {
        let oneCard = GameResource.get({gameId: gameId});
        return oneCard.$promise;
      };

      let gotCurrentGame = function(gameData) {
        let model = angular.copy(gameData);

        $scope.origModel = angular.copy(model);

        $scope.gameModel = model;
        $scope.schema.model = $scope.gameModel;

        $scope.trustedHtml = $sce.trustAsHtml(model.description);
      };

      let onGameSaved = function(editedGame) {
        gotCurrentGame(editedGame.toJSON());
        $scope.gameAction = 'view';
        $scope.viewGame();
      };

      let loadCurrentGame = function() {
        getGame($scope.gameId).then((gameData) => gotCurrentGame(gameData.toJSON()), () => Utils.forceOut('/juegos/', deregister));
      };

      //Here your view content is fully loaded !!
      $scope.reset = function() {
        if ($scope.form)
          $scope.form.$setPristine();
        if (['avatar','view', 'edit'].includes($scope.gameAction)) {
          loadCurrentGame();
        } else {
          $scope.gameModel = angular.copy(blueprint);
          $scope.schema.model = $scope.gameModel;
        }
      };

      $scope.saveResource = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          console.log($scope.gameModel);
          return;
        }
        let args = angular.copy($scope.gameModel);
        // TODO
        delete args.releaseDate;
        let newCard = new GameResource(args);

        let promise = newCard.$save();
        promise.then(onGameSaved);
      };

      let init = function() {
        determineResource();
        $scope.reset();
      }

      init();

    }]);
