export default angular.module('myApp').controller('EventController',
  ['$scope', 'EventResource', '$stateParams', 'VenueResource', 'LoginService', '$sce', 'Modals', 'DateUtils', 'Localization', 'Utils',
    function($scope, EventResource, $stateParams, VenueResource, LoginService, $sce, Modals, DateUtils, Localization, Utils) {
      $scope.status = "";
      $scope.isEdit = false;
      $scope.origModel = undefined; // to keep original on edits

      $scope.login = LoginService.scope;
      $scope.schema = {type: 'event', modelName: 'eventModel', model: $scope.eventModel, getResource: () => EventResource};

      $scope.eventTypes = Localization.eventTypes;
      $scope.venues = {};

      let currentDay = DateUtils.formatDate(new Date());
      let blueprint = {calId: 1, title: "", description: "", 
                       startDate: currentDay, endDate: currentDay,
                       startTime: "08:00", endTime: "20:00",
                       eventType: "single", visibility: "public",
                       venueId: 1, allDay: false, color: '#3788d8',
                       participants: {current: 0, min: 0, max: 0},
                       editable: true, permissions: 'user'};

      $scope.showQr = function() {
        $scope.qrCodeUrl = DateUtils.formatVevent($scope.eventModel);
        $scope.vcsUrl = '/api/ical/' + $scope.eventModel.slug + '.ical';
        Modals.openModal('qrModal', 'Importar evento', "Escanea para importar en tu calendario:");
      };

      let deregister = $scope.$on('$locationChangeSuccess', function(evt) {
        determineResource();
        $scope.reset();
      });


      /* Main actions */

      $scope.openRegistrations = function() {
        Utils.softOut("/eventos/"+$scope.eventModel.slug+"/inbox", deregister);
      };

      $scope.openRegistrationConfig = function() {
        Utils.softOut("/eventos/"+$scope.eventModel.slug+"/config", deregister);
      };

      $scope.showCover = function() {
        Modals.openModal('coverModal', $scope.eventModel.title, "");
      };

      let onEventDeleteSuccess = function(eventId) {
        Utils.forceOut("/actividades/", deregister);
      };

      $scope.deleteEvent = function(event) {
        Modals.openModal('confirmModal', 'Borrar evento', "Vas a borrar el evento '"+$scope.eventModel.title+"', seguro??").then((accepted) => {
          if (accepted) {
            EventResource.remove({evtId: event.id}).$promise.then(() => onEventDeleteSuccess(event.id));
          }
        });
      };

      /* Buttons */

      $scope.editAvatar = function() {
        Utils.softOut("/eventos/"+$scope.eventModel.slug+"/avatar", deregister);
      };

      $scope.viewEvent = function() {
        $scope.eventAction = 'view';
        Utils.softOut("/eventos/"+$scope.eventModel.slug, deregister);
      };

      $scope.editEvent = function() {
        $scope.eventAction = 'edit';
        Utils.softOut("/eventos/"+$scope.eventModel.slug+"/edit", deregister);
      };

      $scope.cancelEdit = function() {
        if ($scope.eventAction == 'edit') {
          $scope.eventModel = angular.copy($scope.origModel);
          $scope.schema.model = $scope.eventModel;
        }
        Utils.backOut(deregister);
      };

      $scope.openCalendar = function() {
        Utils.softOut("/calendario/", deregister);
      };

      let gotVenues = function(venues) {
        venues.forEach((venue) => {
          $scope.venues[venue.id] = venue;
        });
      };

      /* Current Element */

      let getEvent = function(calId) {
        let oneCard = EventResource.get({evtId: calId});
        return oneCard.$promise;
      };

      let gotCurrentEvent = function(eventData) {
        let model = angular.copy(eventData);

        let start = new Date(eventData.start);
        let end = new Date(eventData.end);

        model.startDate = DateUtils.formatDate(start);
        model.endDate = DateUtils.formatDate(end);
        model.startTime = DateUtils.formatTime(start);
        model.endTime = DateUtils.formatTime(end);
        model.rawStartDate = model.start;
        model.rawEndDate = model.end;

        if (!model.color) {
          model.color = '#3788d8';
        }
        delete model.start;
        delete model.end;

        $scope.origModel = angular.copy(model);
        $scope.eventModel = model;
        $scope.schema.model = $scope.eventModel;

        $scope.trustedHtml = $sce.trustAsHtml(model.description);
      };

      let loadCurrentEvent = function() {
        getEvent($scope.eventId).then((eventData) => gotCurrentEvent(eventData.toJSON()), () => Utils.forceOut('/actividades/', deregister));
      };

      /* Save */

      let onEventSaved = function(editedEvent) {
        gotCurrentEvent(editedEvent.toJSON());

        $scope.viewEvent();
      };

      $scope.saveResource = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          console.log($scope.eventModel);
          return;
        }
        let args = angular.copy($scope.eventModel);
        delete args.rawStartDate;
        delete args.rawStartDate;
        delete args.startTime;
        delete args.endTime;
        args.startDate = DateUtils.buildDate($scope.eventModel.startDate, $scope.eventModel.startTime);
        args.endDate = DateUtils.buildDate($scope.eventModel.endDate, $scope.eventModel.endTime);
        console.log(args);
        let newCard = new EventResource(args);

        newCard.$save().then(onEventSaved);
      };

      /* Init */

      let determineResource = function() {
        let action = $stateParams.action;

        $scope.eventAction = action;
        $scope.actionTitle = Localization.actionTitles.event[action];

        if ($stateParams.eventId) {
          $scope.eventId = $stateParams.eventId;
        }
      };

      $scope.reset = function() {
        if ($scope.form)
          $scope.form.$setPristine();
        if (['view', 'edit', 'avatar'].includes($scope.eventAction)) {
          loadCurrentEvent();
        } else {
          $scope.eventModel = angular.copy(blueprint);
          $scope.schema.model = $scope.eventModel;
        }
      };

      let init = function() {
        VenueResource.list().$promise.then(gotVenues);
        determineResource();
        $scope.reset();
      }

      init();

    }]);
