export default angular.module('myApp', ['ngResource', 'ui.router', 'ngAnimate'])
  .config(['$locationProvider', '$compileProvider', '$stateProvider', function($locationProvider, $compileProvider, $stateProvider) {
    // https://docs.angularjs.org/guide/production
    $compileProvider.debugInfoEnabled(false);
    $compileProvider.commentDirectivesEnabled(false);
    $compileProvider.cssClassDirectivesEnabled(false);

    // https://stackoverflow.com/questions/16837704/angularjs-normal-links-with-html5mode
    //$locationProvider.html5Mode({enabled: true, rewriteLinks: false});
    $locationProvider.html5Mode({enabled: true, rewriteLinks: false});
    Dropzone.autoDiscover = false;

  }])
  .config(['$stateProvider', function($stateProvider) {
    let states = [
      {
        name: 'userprofile',
        controller: 'UserController',
        url: '/profile/',
        templateUrl: '/templates/user/view.html'
      }, 
      {
        name: 'profile',
        controller: 'UserController',
        url: '/profile/{userId}',
        templateUrl: '/templates/user/view.html',
        params: {action: 'view', type: 'user'}
      }, 
      {
        name: 'editprofile',
        controller: 'UserController',
        url: '/profile/edit/',
        templateUrl: '/templates/user/edit.html',
        params: {action: 'edit', type: 'user'}
      },
      {
        name: 'editar avatar',
        controller: 'UserController',
        url: '/profile/avatar/',
        template: '<avatar-editor login="login" main-model="userModel" model-type="user" schema="schema" view-object="viewProfile()"></avatar-editor>',
        params: {action: 'avatar', type: 'user'}
      },
      {
        name: 'group',
        component: 'groupView',
        url: '/g/{slug}',
        resolve: {
          groupModel: ['GroupResource', '$transition$', function(GroupResource, $transition$) {
            return GroupResource.get({groupId: $transition$.params().slug}).$promise;
          }]
        }
      }, 
      {
        name: 'editar imagen de evento',
        controller: 'EventController',
        url: '/eventos/{eventId}/avatar',
        template: '<avatar-editor login="login" main-model="eventModel" model-type="event" schema="schema" view-object="viewEvent()"></avatar-editor>',
        params: {action: 'avatar', type: 'event'}
      }, 
      {
        name: 'addevent',
        controller: 'EventController',
        url: '/eventos/new/',
        templateUrl: '/templates/event/edit.html',
        //template: '<event login="login" main-model="eventModel" model-type="event" schema="schema" view-object="viewEvent()"></event>',
        params: {action: 'new', type: 'event'}
      },
      {
        name: 'editevent',
        controller: 'EventController',
        url: '/eventos/{eventId}/edit',
        templateUrl: '/templates/event/edit.html',
        params: {action: 'edit', type: 'event'}
      },
      {
        name: 'viewevent',
        controller: 'EventController',
        url: '/eventos/{eventId}',
        templateUrl: '/templates/event/view.html',
        params: {action: 'view', type: 'event'}
      },
      {
        name: 'registerevent',
        url: '/eventos/{eventId}/registro',
        component: 'eventRegistration',
        resolve: {
          eventModel: ['EventResource', '$transition$', function(EventResource, $transition$) {
            return EventResource.get({evtId: $transition$.params().eventId}).$promise;
          }],
          eventConfig: ['EventResource', '$transition$', function(EventResource, $transition$) {
            return EventResource.getConfig({evtId: $transition$.params().eventId}).$promise;
          }]
        }
      },
      {
        name: 'configevent',
        url: '/eventos/{eventId}/config',
        component: 'eventRegistrationConfig',
        resolve: {
          eventModel: ['EventResource', '$transition$', function(EventResource, $transition$) {
            return EventResource.get({evtId: $transition$.params().eventId}).$promise;
          }],
          eventConfig: ['EventResource', '$transition$', function(EventResource, $transition$) {
            return EventResource.getConfig({evtId: $transition$.params().eventId}).$promise;
          }]
        }
      },
      {
        name: 'event-inbox',
        url: '/eventos/{eventId}/inbox',
        component: 'eventRegistrationInbox',
        resolve: {
          eventModel: ['EventResource', '$transition$', function(EventResource, $transition$) {
            return EventResource.get({evtId: $transition$.params().eventId}).$promise;
          }],
          eventConfig: ['EventResource', '$transition$', function(EventResource, $transition$) {
            return EventResource.getConfig({evtId: $transition$.params().eventId}).$promise;
          }]
        }
      },
      {
        name: 'gameslist',
        controller: 'GameController',
        url: '/juegos/',
        templateUrl: '/templates/game/list.html',
        params: {action: 'list', type: 'game'}
      },
      {
        name: 'addgame',
        controller: 'GameController',
        url: '/juegos/new/',
        templateUrl: '/templates/game/edit.html',
        params: {action: 'new', type: 'game'}
      },
      {
        name: 'editgame',
        controller: 'GameController',
        url: '/juegos/{gameId}/edit',
        templateUrl: '/templates/game/edit.html',
        params: {action: 'edit', type: 'game'}
      },
      {
        name: 'viewgame',
        controller: 'GameController',
        url: '/juegos/{gameId}',
        templateUrl: '/templates/game/view.html',
        params: {action: 'view', type: 'game'}
      },
      {
        name: 'editar imagen de juego',
        controller: 'GameController',
        url: '/juegos/{gameId}/avatar',
        template: '<avatar-editor login="login" main-model="gameModel" model-type="game" schema="schema" view-object="viewGame()"></avatar-editor>',
        params: {action: 'avatar', type: 'game'}
      }, 
      {
        name: 'uploads',
        controller: 'UploadController',
        url: '/uploads/',
        templateUrl: '/templates/upload.html'
      },
      {
        name: 'activities',
        url: '/actividades/',
        external: true
      },
      {
        name: 'calendar',
        url: '/calendario/',
        external: true
      },
      {
        name: 'backstage',
        url: '/trastienda/',
        external: true
      },
      {
        name: 'accounting',
        url: '/accounting/',
        component: 'accounting',
        resolve: {
          list: ['AccountingResource', '$transition$', function(AccountingResource, $transition$) {
            return AccountingResource.query({groupId: 1}).$promise;
            //return EventResource.get({evtId: $transition$.params().eventId}).$promise;
          }]
        }
      },

    ];

    states.forEach(function(state) {
      $stateProvider.state(state);
    });

  }])
  .run(['$rootScope', '$anchorScroll', '$timeout', '$http', '$window', '$templateCache', function ($rootScope, $anchorScroll, $timeout, $http, $window, $templateCache) {
    // https://stackoverflow.com/questions/22290570/angular-ui-router-scroll-to-top-not-to-ui-view
    $rootScope.$on('$viewContentLoaded', function(){
      $anchorScroll('top');
    });
    /*$rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams) {
        console.log(toState);
        if (toState.external) {
          event.preventDefault();
          $window.open(toState.url, '_self');
        }
      });*/
    Object.keys(AngularTemplates).forEach(function(tpl_file) {
      $templateCache.put(tpl_file, AngularTemplates[tpl_file]);
    });
    //$http.defaults.headers.common['X-CSRFToken'] = csrf_token;
  }]);
