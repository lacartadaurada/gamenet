import angularTemplates from './all-tpls';

import app from './app';

import cryptoUtils from './services/crypto';
import dateUtils from './services/dateutils';
import loginService from './services/login';
import modalsService from './services/modals';
import localizationService from './services/localization';
import utilsService from './services/utils';

import venueResource from './resources/venue';
import calendarResource from './resources/calendar';
import eventResource from './resources/event';
import commentResource from './resources/comment';
import bookmarkResource from './resources/bookmark';
import attachmentResource from './resources/attachment';
import annotationResource from './resources/annotation';
import userResource from './resources/user';
import mediaResource from './resources/media';
import gameResource from './resources/game';
import siteMessageResource from './resources/site-message';
import groupResource from './resources/group';
import accountingResource from './resources/accounting';
import riverResource from './resources/river';

import tempusDominusDirective from './directives/tempusdominus';
import richEditorDirective from './directives/richeditor';
import dropperCropperDirective from './directives/dropper-cropper';
import dropzoneDirective from './directives/dropzone';
import privacyIconsDirective from './directives/privacyicons';
//import tooltipDirective from './directives/tooltip';
import tooltipDirective from './directives/popper';
import bsTooltipDirective from './directives/tooltip';
import modalDirective from './directives/modal';
import imgModalDirective from './directives/imgmodal';
import qrCodeDirective from './directives/qrcode';
import annotationControlsDirective from './directives/annotation-controls';
import userMenu from './directives/usermenu';

import eventController from './controllers/event';
import gameController from './controllers/game';
import loginController from './controllers/login';
import signupController from './controllers/signup';
import nexteventsController from './controllers/nextevents';
import uploadController from './controllers/upload';
import userController from './controllers/user';
import annotationController from './controllers/annotation';
import gamesAnnotationsController from './controllers/games-annotations';

import minmaxFilter from './filters/minmax-print';
import zeroInvalidFilter from './filters/zero-invalid';
import startFromFilter from './filters/start-from';

import paginationComponent from './components/pagination';

import siteMessageComponent from './components/site-message';
import eventRegistrationComponent from './components/event-registration/form';
import './components/event-registration/config-form';
import eventInboxComponent from './components/event-registration/inbox';
import bookmarkComponent from './components/bookmark';
import attachmentComponent from './components/attachment';
import avatarEditorComponent from './components/avatar-editor';
import avatarImageComponent from './components/avatar-image';
import calendarComponent from './components/calendar';
import commentsComponent from './components/comments';
import accountingMainComponent from './components/accounting/main';
import accountingListComponent from './components/accounting/list';
import accountingFormComponent from './components/accounting/form';
import accountingFeeFormComponent from './components/accounting/form-fee';
import groupViewComponent from './components/group/view';
import riverComponent from './components/river';
import entityPopmenu from './components/entity-popmenu';
//import signupComponent from './components/signup';

