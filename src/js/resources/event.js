export default angular.module('myApp').factory('EventResource', ['$resource', function($resource) {
  let resourceBase = '/api/calendar/:calId/event/:evtId';
  let resourceDefault = {evtId: '@id', calId: 1};
  let methods = {
    listRegistrations: {method: 'POST', url: '/api/calendar/:calId/event/:evtId/inbox', isArray: true},
    register: {method: 'POST', url: '/api/calendar/:calId/event/:evtId/register'},
    getConfig: {method: 'GET', url: '/api/calendar/:calId/event/:evtId/config', isArray: true},
    setConfig: {method: 'POST', url: '/api/calendar/:calId/event/:eventId/config', params: {eventId: data=>data.eventId }}
  };

  return $resource(resourceBase, resourceDefault, methods);
}]);
