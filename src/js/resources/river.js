export default angular.module('myApp').factory('River', ['$resource', function($resource) {
  let resourceBase = '/api/river/:riverId';
  let resourceDefault = {riverId: '@id'};
  let methods = {};

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
