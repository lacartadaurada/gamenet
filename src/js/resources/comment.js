export default angular.module('myApp').factory('CommentResource', ['$resource', function($resource) {
  let resourceBase = '/api/comment/:commentId';
  let resourceDefault = {commentId: '@id'};
  let methods = {list: {cache: true, isArray: true}};

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
