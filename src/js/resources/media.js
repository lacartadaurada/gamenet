export default angular.module('myApp').factory('MediaResource', ['$resource', function($resource) {
  let resourceBase = '/api/media/';
  let resourceDefault = {};
  let methods = {list: {cache: true, isArray: true}};

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
