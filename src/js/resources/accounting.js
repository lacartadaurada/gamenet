export default angular.module('myApp').factory('AccountingResource', ['$resource', function($resource) {
  let resourceBase = '/api/accounting/:groupId/:id';
  let resourceDefault = {id: '@id', groupId: '@groupId'};
  let methods = {list: {cache: true, isArray: true}};

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
