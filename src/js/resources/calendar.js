//import { app } from "./app.js";

export default angular.module("myApp").factory('CalendarResource', ['$resource', function($resource) {
  let resourceBase = '/calendar/:calId';
  let resourceDefault = {calId: '@id', groupId: 1, title: '', description: '', startDate: '', endDate: ''};

  return $resource(resourceBase, resourceDefault);
}]);
