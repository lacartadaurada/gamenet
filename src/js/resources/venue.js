
export default angular.module("myApp").factory('VenueResource', ['$resource', function($resource) {
  let resourceBase = '/api/venue/:venueId';
  let resourceDefault = {venueId: '@id'};

  return $resource(resourceBase, resourceDefault, {list: {cache: true, isArray: true}}, {stripTrailingSlashes: false});
}]);
