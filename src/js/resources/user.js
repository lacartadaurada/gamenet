export default angular.module('myApp').factory('UserResource', ['$resource', function($resource) {
  let resourceBase = '/api/user/:userId';
  let resourceDefault = {userId: '@id'};
  let methods = {};

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
