export default angular.module('myApp').factory('GameResource', ['$resource', function($resource) {
  let resourceBase = '/api/game/:gameId';
  let resourceDefault = {gameId: '@id'};
  let methods = {};
  let options = {stripTrailingSlashes: false};

  return $resource(resourceBase, resourceDefault, methods, options);
}]);
