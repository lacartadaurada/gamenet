export default angular.module('myApp').factory('BookmarkResource', ['$resource', function($resource) {
  let resourceBase = '/api/bookmark/:bookmarkId';
  let resourceDefault = {bookmarkId: '@id'};
  let methods = {list: {cache: true, isArray: true}};

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
