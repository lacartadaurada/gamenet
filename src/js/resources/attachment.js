export default angular.module('myApp').factory('AttachmentResource', ['$resource', function($resource) {
  let resourceBase = '/api/attachment/:entityType/:entityId/:attachmentId';
  let resourceDefault = {attachmentId: '@id', entityType: '@entityType', entityId: '@entityId'};
  let methods = {list: {cache: true, isArray: true}};

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
