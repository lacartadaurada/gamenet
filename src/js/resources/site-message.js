export default angular.module('myApp').factory('SiteMessage', ['$resource', function($resource) {
  let resourceBase = '/api/site/message/:msgId';
  let resourceDefault = {msgId: '@id'};
  let methods = {list: {cache: true, isArray: true}};

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
