export default angular.module('myApp').factory('AnnotationResource', ['$resource', function($resource) {
  let resourceBase = '/api/annotation/:entityType/:entityId/:annotationType';
  let resourceDefault = {entityId: '@entityId', entityType: '@entityType', annotationType: '@type'};
  let methods = {
    search: {method: 'POST', url: '/api/annotation/search/', isArray: true},
  };

  return $resource(resourceBase, resourceDefault, methods, {stripTrailingSlashes: false});
}]);
