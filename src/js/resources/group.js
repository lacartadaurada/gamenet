export default angular.module('myApp').factory('GroupResource', ['$resource', function($resource) {
  let resourceBase = '/api/group/:groupId';
  let resourceDefault = {groupId: '@id'};
  let methods = {
    search: {method: 'GET', isArray: true},
    members: {method: 'GET', isArray: true, url: '/api/group/:groupId/members/'}
  };
  let options = {stripTrailingSlashes: false};

  return $resource(resourceBase, resourceDefault, methods, options);
}]);
