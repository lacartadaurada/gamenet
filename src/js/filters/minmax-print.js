export default angular.module('myApp').filter('minmax', function() {
  return function(input) {
    if (!input || input.min == 0) {
      return '-';
    }

    let res = input.min;
    if (input.max != input.min) {
      res += '-'+input.max;
    }
    return res;
  }
});
