export default angular.module('myApp').filter('zeroInvalid', function() {
  return function(input) {
    if (!input || input == 0) {
      return '-';
    }
    return input;
  }
});
