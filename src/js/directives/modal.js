export default angular.module('myApp').directive('modal', ['Modals', function(Modals) {
  return {
    template: `
    <div class="modal fade" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen-sm-down">
            <div class="modal-content" ng-click="contentClick()">
                <div class="modal-header" ng-if="showHeader">
                    <h1 class="modal-title fs-5" id="confirmModalLabel">{{title}}</h1>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    {{body}}<ng-transclude></ng-transclude>
                </div>
                <div class="modal-footer" ng-if="showFooter">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal" ng-click="setResult(true)">Ok</button>
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal" ng-click="setResult(false)">Cancelar</button>
                </div>
            </div>
        </div>
    </div>`,
    replace: true,
    transclude: true,
    restrict: 'E',
    scope: {
    },
    link: function postLink(scope, element, attrs, ngModel) {
      scope.showHeader = attrs.hideHeader != 'true';
      scope.showFooter = attrs.hideFooter != 'true';
      scope.result = false;
      let closeOnBody = attrs.closeOnBody == 'true';
      let modal = Modals.registerModal(attrs.id, scope);

      scope.setResult = function(val) {
        scope.result = val;
      };

      scope.contentClick = function() {
        if (closeOnBody) {
          modal.hide();
        }
      };
    }
  };
}]);
