export default angular.module('myApp').directive('annotationControls', ['LoginService', function(LoginService) {
  return {
    template: `<span ng-show="login.isLoggedIn">
                    <button ng-repeat="action in actionList" ng-click="onClick({action: action, '$event': $event})" class="btn btn-link" title="{{titles[action]}}"><i ng-class="classes[action]" class="annotation-button fas"></i></button>
                </span>`,
    replace: true,
    restrict: 'E',
    transclude: false,
    scope: {
      onClick: '&',
      annotationList: '='
    },
    link: function postLink(scope, element, attrs, ngModel) {
      scope.titles = {like: 'Me gusta', have: 'Lo tengo', wannaplay: 'Quiero jugar', going: 'Voy a ir'}
      scope.icons = { like: 'fa-heart', have: 'fa-check', wannaplay: 'fa-hand-pointer', going: 'fa-user-plus' };
      scope.actionList = attrs.actions.split(",");
      scope.classes = angular.copy(scope.icons);

      // find a way to watch annotationList without overwriting in parent scope all the time.
      scope.$watch('annotationList', function(val) {
        if (!val) {
          return;
        }
        scope.classes = {}
        scope.actionList.forEach(function(action) {
          scope.classes[action] = scope.icons[action] + (val[action]?' annotated':'');
        });
      });

      scope.login = LoginService.scope;

    }
  };
}]);
