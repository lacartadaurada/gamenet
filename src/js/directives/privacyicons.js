export default angular.module('myApp').directive('privacyIcons', ['LoginService', 'Localization', function(LoginService, Localization) {
  return {
    template: `<span><img class="privacy-icon" ng-show="selIcon&&login.isLoggedIn" title="{{selTooltip}}" ng-src="{{selIcon}}" class="x-align-bottom x-mb-2 x-ms-2" width="16px" height="16px"></img>
              <img class="privacy-icon" ng-show="selPrivIcon&&login.isLoggedIn" title="{{selPrivTooltip}}" ng-src="{{selPrivIcon}}" class="x-align-bottom x-mb-2 x-ms-2" width="16px" height="16px"></img></span>`,
    replace: true,
    restrict: 'E',
    scope: {
      visibility: '@',
      permissions: '@',
    },
    link: function postLink(scope, element, attrs, ngModel) {
      var icons = {
        private: '/images/icons/private.svg',
        members: '/images/icons/network.png',
        group: '/images/icons/group.svg',
        public: '/images/icons/world.svg'
      };
      var tooltips = Localization.visibilityTooltips;

      var privIcons = {
        user: '/images/icons/locked.svg',
        group: '/images/icons/unlocked.svg'
      };
      var privTooltips = Localization.permissionsTooltips;

      privIcons.members = privIcons.group;

      scope.login = LoginService.scope;
      scope.$watch('visibility', function(val) {
        scope.selIcon = icons[val];
        scope.selTooltip = tooltips[val];
      });
      scope.$watch('permissions', function(val) {
        scope.selPrivIcon = privIcons[val];
        scope.selPrivTooltip = privTooltips[val];
      });

    }
  };
}]);
