export default angular.module('myApp').directive('dropzone', function() {
  return {
    //require: ['ngModel'],
    require: '?ngModel',
    template: '<div class="dropzone"><div class="dz-message" data-dz-message><div>Haz click o arrastra un fichero</div><div><small><small>(Max: {{maxFileSize}} MB)</small></small></div></div></div>',
    replace: true,
    restrict: 'E',
    scope: {
      uploadUrl: '@'
    },
    link: function postLink(scope, element, attrs, ngModel) {
      // to use with form: https://docs.dropzone.dev/configuration/tutorials/combine-form-data-with-files
      // config options: https://docs.dropzone.dev/configuration/basics/configuration-options
      scope.maxFileSize = attrs.maxFileSize || 50;
      let config = {
        url: function() {
          // uploadUrl might not be initialized initially, so
          // return it just in time.
          return scope.uploadUrl || "/api/upload/";
        },
        paramName: "file",
        maxFilesize: scope.maxFileSize, // MB
        // maxFiles: 1,
        chunking: true,
        chunkSize: 500000,
        init: function() {
        },
        success: function(file, response) {
              scope.$emit('dropzone.addedfile', response);
              this.removeFile(file);
        }
        // resizeWidth: 800,
      };
      if (attrs.maxFiles) {
        config.maxFiles = parseInt(attrs.maxFiles);
      }

      if (attrs.fileFilter) {
        // like image/*,application/pdf,.psd
        config.acceptedFiles = fileFilter;
      }

      var myDropzone = $(element).dropzone(config);
    }
  };
});
