export default angular.module('myApp').directive('bstooltip', function() {
  return {
    transclude: false,
    replace: false,
    restrict: 'AE',
    scope: {
    },
    link: function postLink(scope, element, attrs, ngModel) {
      // data-bs-toggle="tooltip" data-bs-placement="top"
      // data-bs-title="tooltip text"
      // data-bs-html="true"
      let internalHTML = "";
      let internalNode = undefined;
      let tooltipFor = element[0];
      if (attrs.tooltipFor) {
        tooltipFor = document.getElementById(attrs.tooltipFor);
        internalNode = element[0];
        internalHTML = internalNode.innerHTML.replace(/ng-transclude/g, "div").replace(/ng-src/g, "src");
      } else {
        internalHTML = attrs.bstooltip;
      }

      let options = {
        title: internalHTML,
        html: true,
        placement: attrs.placement || 'top',
        trigger: 'hover focus',
        container: 'body'
      };

      if (attrs.delayShow && attrs.delayHide) {
        options.delay = {show: parseInt(attrs.delayShow), hide: parseInt(attrs.delayHide)};
      }

      let tooltip = new bootstrap.Tooltip(tooltipFor, options);
      if (internalNode) {
        internalNode.innerHTML = "";
      }
    }
  };
});
