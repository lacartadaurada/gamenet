export default angular.module('myApp').directive('tooltip', function() {
  return {
    template: '<div class="popper-tooltip" role="tooltip" tooltip-placement="right"><ng-transclude></ng-transclude><div id="arrow" data-popper-arrow></div></div>',
    transclude: true,
    replace: true,
    restrict: 'E',
    scope: {
    },
    link: function postLink(scope, element, attrs, ngModel) {
      // data-bs-toggle="tooltip" data-bs-placement="top"
      // data-bs-title="tooltip text"
      // data-bs-html="true"

      const button = document.querySelector(attrs.tooltipFor);
      const tooltip = element[0];

      const popperInstance = Popper.createPopper(button, tooltip, {
        placement: attrs.tooltipPlacement || 'top',
        modifiers: [
          {
            name: 'offset',
            options: {
              offset: [0, 6],
              //offset: [0, 8],
            },
          },
          {
            name: 'preventOverflow',
            options: {
              altAxis: true,
              padding: 40
            },
          },
        ],
      });

      function show() {
        // Make the tooltip visible
        tooltip.setAttribute('data-show', '');

        // Enable the event listeners
        popperInstance.setOptions((options) => ({
          ...options,
          modifiers: [
            ...options.modifiers,
            { name: 'eventListeners', enabled: true },
          ],
        }));

        // Update its position
        popperInstance.update();
      }

      function hide() {
        // Hide the tooltip
        tooltip.removeAttribute('data-show');

        // Disable the event listeners
        popperInstance.setOptions((options) => ({
          ...options,
          modifiers: [
            ...options.modifiers,
            { name: 'eventListeners', enabled: false },
          ],
        }));
      }

      const showEvents = ['mouseenter', 'focus'];
      const hideEvents = ['mouseleave', 'blur'];

      showEvents.forEach((event) => {
        button.addEventListener(event, show);
      });

      hideEvents.forEach((event) => {
        button.addEventListener(event, hide);
      });

    }
  };
});
