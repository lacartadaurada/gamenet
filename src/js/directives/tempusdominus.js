// https://plnkr.co/edit/6ma6EDd5dme2pZzuCgRR?p=preview&preview

export default angular.module('myApp').directive('tempusDominus', ['DateUtils', function(DateUtils) {
  return {
    require: '?ngModel',
    template: '<input type="text" class="form-control"></input>',
    replace: true,
    restrict: 'E',
    link: function postLink(scope, element, attrs, ngModel) {
      let viewType = attrs.view == 'time' ? 'clock' : 'calendar';

      let parseDay = function(date) {
        let dateParts = date.split("/");
        let finalDate = new Date(dateParts[2], dateParts[1]-1, dateParts[0], 0, 0);
        return finalDate;
      };

      let parseTime = function(time) {
        let timeParts = time.split(":");
        let finalDate = new Date(0, 0, 0, timeParts[0], timeParts[1]);
        return finalDate;
      };

      let formatDate = function(date) {
        if (viewType == 'calendar') {
          return DateUtils.formatDate(date);
        } else {
          return DateUtils.formatTime(date);
        }
      };
      let parseDate = function(text) {
        if (viewType == 'calendar') {
          return parseDay(text);
        } else {
          return parseTime(text);
        }
      }

      tempusDominus.loadLocale(tempusDominus.locales.es);
      tempusDominus.locale(tempusDominus.locales.es.name);

      let components = {
        calendar: true,
        decades: false,
        year: true,
        month: true,
        date: true,
        hours: false,
        minutes: false,
        seconds: false,
        clock: false,
        useTwentyfourHour: true
      };
      
      if (attrs.view == 'time') {
        components = {
          calendar: false,
          decades: false,
          year: true,  //
          month: true, //
          date: true,
          hours: true,
          minutes: true,
          seconds: false,
          clock: true,
          useTwentyfourHour: true
        };
      }

      let calIcons ={
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-check',
        clear: 'fa fa-trash',
        close: 'fa fa-times'
      };

      // Initialization

      // alternative way to set format:
      //picker.dates.formatInput = date => formatDate(date);
      let calPickerOptions = {
        localization: {startOfTheWeek: 1, format: viewType == 'clock' ? 'HH:mm' : 'dd/MM/yyyy'},
        display: {viewMode: viewType, icons: calIcons, buttons: {close: true}, components: components},
      };

      let picker = new tempusDominus.TempusDominus(element[0], calPickerOptions);
      element.data('picker', picker)
      element.data('model', ngModel)

      let unregister = scope.$watch(function () {
        return ngModel.$modelValue;
      }, function(newValue) {
        if (!newValue) {
          return;
        }
        let currentDate = parseDate(newValue);
        try {
          picker.updateOptions({defaultDate: currentDate, display: {viewMode: viewType}});
        } catch(e) {
          console.log(viewType, 'picker error setting date', currentDate);
        }
        unregister();
      });

      if (attrs.linkEnd) {
        element[0].addEventListener("change.td", (e) => {
          let prevDate = e.detail.oldDate;
          if (!prevDate) {
            // initializing
            return;
          }
          let prevValue = formatDate(prevDate);
          let myVal = formatDate(e.detail.date);

          let destElement = $(attrs.linkEnd);
          if (prevValue == destElement.val()) {
            let destPicker = destElement.data('picker');
            destPicker.dates.setFromInput(myVal);
            //let destModel = destElement.data('model');
            //destModel.$setViewValue(myVal);
          }
        });
      }
    } // end link
  };
}]);
