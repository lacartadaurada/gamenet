export default angular.module('myApp').directive('userMenu', ['LoginService', '$location', '$state', '$window', function(LoginService, $location, $state, $window) {
  return {
    template: `<div ng-cloak>
        <li class="nav-item dropdown class dropdown-user rounded-circle" ng-if="login.isLoggedIn">
            <a class="nav-link dropdown-toggle dropdown-toggle-user" id="dropdown-user" href="#" data-bs-toggle="dropdown"
                                                                                    aria-haspopup="true" aria-expanded="false">
                <div>
                    <avatar-image avatar-class="rounded-circle" avatar="login.whoami.avatar" title="{{login.whoami.name}}" width="32"></avatar-image>
                </div>
            </a>
            <ul class="dropdown-menu" aria-labelledby="dropdown-user">
                <li><button class="btn-link btn dropdown-item" ng-click="viewProfile()"><i class="fa fa-user aria-hidden="true"></i> Perfil</button></li>
                <li><button class="btn-link btn dropdown-item" ng-click="logout()"><i class="fa fa-power-off" aria-hidden="true"></i> Logout</button></li>
            </ul>
        </li>
        <li ng-if="login.isLoggedIn" class="dropdown-user-mobile d-flex justify-content-between flex-row d-md-none">
          <div><a href="/profile/" class="btn dropdown-item nav-link p-2">
              <avatar-image avatar-class="rounded-circle" avatar="login.whoami.avatar" title="{{login.whoami.name}}" width="32"></avatar-image>
          </a></div>
          <div><button class="btn dropdown-item nav-link p-2 mx-2 fs-3" ng-click="logout()"><i class="fa fa-power-off"></i></button></div>
        </li>
    </div>`,
    replace: true,

    replace: true,
    restrict: 'E',
    transclude: false,
    link: function postLink(scope, element, attrs, ngModel) {

      scope.login = LoginService.scope;
      let unregister = scope.$watch(function () {
        return LoginService.scope;
      }, function(newValue) {
        scope.login = LoginService.scope;
      });

      scope.editProfile = function() {
        //$state.go("editprofile", { } );
        $window.location.href = "/profile/edit/";
      };

      scope.viewProfile = function() {
        //$state.go("userprofile", { } );
        //params: {action: 'view', type: 'user'}
        $window.location.href = "/profile/";
      };

      let onLogout = function() {
        $window.location.href = "/";
      };

      scope.logout = function() {
        LoginService.logout().then(onLogout);
      }

      scope.$watch('login.isLoggedIn', function(val) {
        scope.login = LoginService.scope;
      });

    }
  };
}]);
