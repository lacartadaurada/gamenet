export default angular.module('myApp').directive('qrCode', function() {
  return {
    template: '<div></div>',
    replace: true,
    restrict: 'E',
    scope: {
      qrUrl: "@",
    },
    link: function postLink(scope, element, attrs, ngModel) {
      let url = scope.qrUrl;

      let qrcode = new QRCode(element[0], {
        text: url,
        width: attrs.width || 128,
        height: attrs.height || 128,
        colorDark : "#000000",
        colorLight : "#ffffff",
        correctLevel : QRCode.CorrectLevel.M
      });
      scope.$watch('qrUrl', function(val) {
        //qrcode.clear();
        qrcode.makeCode(val);
      });
    }
  };
});
