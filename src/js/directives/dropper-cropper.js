// https://itnext.io/integrating-dropzone-with-javascript-image-cropper-optimise-image-upload-e22b12ac0d8a
export default angular.module('myApp').directive('dropperCropper', ['$q', function($q) {
  let dataURItoBlob = function(dataurl) {
    let parts = dataurl.split(',');
    let mime = parts[0].match(/:(.*?);/)[1];

    if(parts[0].indexOf('base64') !== -1) {
      let bstr = atob(parts[1]), n = bstr.length, u8arr = new Uint8Array(n)
      while(n--){
        u8arr[n] = bstr.charCodeAt(n)
      }

      return new Blob([u8arr], {type:mime})
    } else {
      let raw = decodeURIComponent(parts[1])
      return new Blob([raw], {type: mime})
    }
  };
  let dropzoneBlob = function(dropzone) {
    return {blob: dataURItoBlob(dropzone.files[0].dataURL), name: dropzone.files[0].name}
  };

  return {
    //require: ['ngModel'],
    require: '?ngModel',
    template: '<div class="dropzone-container"><div class="dropzone"><div class="dz-message" data-dz-message><div>Haz click o arrastra un fichero</div><div><small><small>(Max: {{maxFileSize}} MB)</small></small></div></div></div></div>',
    replace: true,
    restrict: 'E',
    scope: {
      uploadUrl: '@',
      info: '='
    },
    link: function postLink(scope, element, attrs, ngModel) {
      let doneCb = undefined;
      let addBlob = undefined;
      let extras = {};
      let setParam = function(key, value) {
        if (value != undefined) {
          extras[key] = value;
        }
      }
      scope.maxFileSize = attrs.maxFileSize || 50;
      let config = {
        url: function() {
          // uploadUrl might not be initialized initially, so
          // return it just in time.
          return scope.uploadUrl || "/api/upload/";
        },
        paramName: "file",
        maxFilesize: scope.maxFileSize, // MB
        // maxFiles: 1,
        chunking: true,
        chunkSize: 500000,
        init: function() {
          element.data('dropzone', this);
          this.on("sending", function(file, xhr, formData) {
            Object.keys(extras).forEach(function(key) {
              formData.append(key, extras[key]);
            });
          });
        },
        transformFile: function(file, done) {
          let myDropZone = this;
          doneCb = function (newBlob) {
            done(newBlob||file);
            //myDropZone.processQueue();
          };
          addBlob = function(blob) {
            blob.name = 'myfilename.png'
            myDropZone.addFile(blob);
          };
          let editor = document.createElement('div');
          editor.classList.add('cropper-dropper-container');
          let parentNode = element[0].parentElement;

          parentNode.appendChild(editor);

          // Create an image node for Cropper.js
          let image = new Image();
          image.src = URL.createObjectURL(file);
          image.classList.add('cropper-image')
          editor.appendChild(image);
          element[0].style.display = 'none';

          // Create Cropper.js
          let cropperOptions = {viewMode:2,
            aspectRatio:1,
            zoomable: false,
            /*guides: false,*/
            background: false,
            ready: () => cropper.setDragMode('move'),
            toggleDragModeOnDblclick: false,
            preview: ".cropper-preview"};

          let cropper = new Cropper(image, cropperOptions);
          scope.$apply(() => {
            scope.info.status = 'cropping';
          });
          element.data('cropper', {api: cropper, done: doneCb, addBlob: addBlob, setParam: setParam});
        },
        success: function(file, response) {
          scope.$emit('dropzone.addedfile', response);
          this.removeFile(file);
        }
        // resizeWidth: 800,
      };
      if (attrs.maxFiles) {
        config.maxFiles = parseInt(attrs.maxFiles);
      }

      config.acceptedFiles = "image/*";
      if (attrs.fileFilter) {
        // like image/*,application/pdf,.psd
        config.acceptedFiles = fileFilter;
      }

      let myDropzone = element.children('.dropzone').dropzone(config);
    }
  };
}]);
