// https://plnkr.co/edit/6ma6EDd5dme2pZzuCgRR?p=preview&preview

export default angular.module('myApp').directive('datePicker2X', function() {
  return {
    require: '?ngModel',
    template: '<input type="text" class="input-sm form-control"></input>',
    //template: '<span><input type="text" class="input-sm form-control" /></span>',
    replace: true,
    restrict: 'E',
    scope: {
      //model: '=pickerModel',
      //view: '=view',
      //model: '=ngModel',
      //title: '=title'

    },
    link: function postLink(scope, element, attrs, ngModel) {
      let format = "DD/MM/YYYY";
      if (attrs.view == 'time') {
        format = "HH:mm";
      }
      let calIcons ={
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-check',
        clear: 'fa fa-trash',
        close: 'fa fa-times'
      };
      let calPickerOptions = {
        locale: 'es',
        format: format,
        icons: calIcons,
        showClose: true
      };

      let currentValue = scope.$modelValue;

      // initialization
      element.datetimepicker(calPickerOptions)
        .data("DateTimePicker").defaultDate(currentValue);

      /*$rootScope.$on('resetFormEvent', function(e, data){
        console.log("should reset");
      });*/

      // link start calendar
      if (attrs.linkStart) {
        element.on("dp.change", function (e) {
          let formatted = e.date.format(format);
          let currentValue = ngModel.$modelValue;

          if (formatted!=currentValue) {
            $(attrs.linkStart).data("DateTimePicker").maxDate(e.date);
          }
        });
      }
      // Link end calendar
      if (attrs.linkEnd) {
        element.on("dp.change", function (e) {
          var destElement = $(attrs.linkEnd);
          let formatted = e.date.format(format);
          let prevValue = ngModel.$modelValue;
          let destPicker = destElement.data("DateTimePicker");
          let destValue;
          if (destPicker.date()) {
            destValue = destElement.val();
          }

          if (formatted!=prevValue) {
            var needsChangeDest = prevValue == destValue;
            destPicker.minDate(e.date);

            if (needsChangeDest) {
              destElement = $(attrs.linkEnd);
              destElement.val(formatted);
              //destElement.datepicker("setDate", e.date.toDate());
              destElement.trigger(e)
            }
          }
        });
      }

      // Forward datepicker changes to angular.
      element.on("dp.change", function (e) {
        ngModel.$setViewValue(e.date.format(format));
      });

    }
  };
});
