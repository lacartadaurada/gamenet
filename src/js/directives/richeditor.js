// https://plnkr.co/edit/6ma6EDd5dme2pZzuCgRR?p=preview&preview

export default angular.module('myApp').directive('richEditor', function() {
  return {
    require: '?ngModel', // get a hold of NgModelController
    template: '<div></div>',
    replace: true,
    restrict: 'E',
    link: function postLink(scope, element, attrs, ngModel) {
      $.trumbowyg.svgPath = '/images/trumbo/icons.svg';
      let trumbo = element.trumbowyg({
        autogrow: true,
        autogrowOnEnter: true,
        statusPropertyName: 'success',
        lang: 'es',
        btnsDef: {
          // Create a new dropdown
          justify: {
            dropdown: ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
            ico: 'justifyLeft'
          },
          image: {
            dropdown: ['insertImage', 'upload'],
            ico: 'insertImage'
          }
        },
        btns: [
          ['h2', 'h3', 'bold', 'italic', 'del'],
          ['link'],
          //['image'],
          ['justify'],
          ['unorderedList', 'orderedList'],
          ['horizontalRule'],
          ['removeformat'],
          ['fullscreen']
        ],
        plugins: {
          upload: {
            serverPath: '/api/upload-image',
            fileFieldName: 'file',
            urlPropertyName: 'url',
            //success: (data) => console.log('file uploaded', data)
          }
        }
      });
      let editing = false;

      ngModel.$render = function () {
        var newValue = ngModel.$viewValue;
        if (newValue != undefined && !editing) {
          element.trumbowyg("html", newValue);
        }
      };

      trumbo.on('tbwchange', function() {
        if (editing) {
          let data = element.trumbowyg("html");
          //ngModel.$setViewValue(data);
        }
      })

      trumbo.on('tbwfocus', function(){
        editing = true;
      });

      trumbo.on('tbwblur', function(){
        editing = false;
        let data = element.trumbowyg("html");
        ngModel.$setViewValue(data);
      });
    }
  };
});
