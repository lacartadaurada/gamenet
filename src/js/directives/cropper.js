// https://itnext.io/integrating-dropzone-with-javascript-image-cropper-optimise-image-upload-e22b12ac0d8a

export default angular.module('myApp').directive('cropper', function() {
  return {
    template: '<div><img class="cropper-image" ng-src="{{src}}"></div>',
    replace: true,
    restrict: 'E',
    scope: {
      src: '@'
    },
    link: function postLink(scope, element, attrs, ngModel) {
      let imageElmt = element.children('img')[0];

      let cropper;
      let options = {viewMode:2,
                     aspectRatio:1,
                     zoomable: false,
                     /*guides: false,*/
                     background: false,
                     ready: () => cropper.setDragMode('move'),
                     toggleDragModeOnDblclick: false,
                     preview: "#preview"};
      let setup = function(img) {
        cropper = new Cropper(imageElmt, options);
        cropper.setDragMode('move');
        element.data('cropper', cropper);
      };

      scope.$watch('src', function(val) {
        if (val) {
          setup(val);
        }
      });
    }
  }});
