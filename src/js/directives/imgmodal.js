export default angular.module('myApp').directive('imgModal', ['Modals', function(Modals) {
  return {
    template: `
    <div class="modal fade" tabindex="-1" aria-labelledby="confirmModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen-sm-down" ng-click="contentClick()">
        <div class="modal-content">
           <ng-transclude></ng-transclude>
       </div>
       </div>
    </div>`,
    replace: true,
    transclude: true,
    restrict: 'E',
    scope: {
    },
    link: function postLink(scope, element, attrs, ngModel) {
      let closeOnBody = attrs.closeOnBody == 'true';
      let modal = Modals.registerModal(attrs.id, scope);
      scope.contentClick = function() {
        if (closeOnBody) {
          modal.hide();
        }
      }
    }
  };
}]);
