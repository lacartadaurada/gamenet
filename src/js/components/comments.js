export default angular.module('myApp').component('comments', {
  templateUrl: '/templates/comments.html',
  bindings: {
    model: '<mainModel',
    modelType: '@',
    login: '<',
  },
  controller: ['$scope', 'CommentResource', '$sce', 'Modals', 'Utils',
    function($scope, CommentResource, $sce, Modals, Utils) {
      let ctrl = this;
      Utils.disableAnimation('.comment-section');
      ctrl.commentModel = {};
      ctrl.commentsList = undefined;

      /* Parse Element */

      var addComment = function(comment) {
        var elmt = angular.copy(comment);
        elmt = {};
        elmt.userId = comment.userId;
        elmt.created = comment.created;
        elmt.id = comment.id;
        elmt.user = comment.user;
        elmt.editable = comment.editable;
        elmt.trustedContent = $sce.trustAsHtml(comment.content);
        return elmt;
      };

      /* List */

      var gotComments = function(comments) {
        var elmts = [];
        comments.forEach(function(elmt) {
          elmts.push(addComment(elmt));
        });
        ctrl.commentsList = elmts;
        Utils.enableAnimation('.comment-section');
      };

      var loadComments = function() {
        return CommentResource.query({id: ctrl.model.id, type: ctrl.modelType}).$promise.then(gotComments);
      };

      /* Create and Edit */

      var onCommentPostSuccess = function(data) {
        let elmt = addComment(data);
        ctrl.commentsList.push(elmt);
        ctrl.commentModel.content = "";
      };

      ctrl.postComment = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          console.log(ctrl.model);
          return;
        }
        let args = {content: ctrl.commentModel.content, entity: {id: ctrl.model.id, type: ctrl.modelType}}
        let newCard = new CommentResource(args);

        let promise = newCard.$save().then(onCommentPostSuccess);
      };

      /* Deletion */

      var onCommentDeleteSuccess = function(commentId) {
        let index = ctrl.commentsList.findIndex((element) => element.id == commentId );

        if (index > -1) {
          ctrl.commentsList.splice(index, 1);
        } else {
          console.log("not found on array!");
        }
      };

      ctrl.deleteComment = function(comment) {
        Modals.openModal('confirmModal', 'Vas a borrar el comentario', 'Seguro?').then((accepted) => {
          if (accepted) {
            CommentResource.remove({commentId: comment.id}).$promise.then(() => onCommentDeleteSuccess(comment.id));
          }
        });
      }

      /* Initialization */

      let init = function() {
          loadComments();
      };

      ctrl.$onChanges = function(changes) {
        if (changes.model.currentValue) {
          init();
        }
      };
  }] // end controller
});
