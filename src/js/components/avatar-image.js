export default angular.module('myApp').component('avatarImage', {
  template: `<img class="shadow-1-strong" ng-class="$ctrl.avatarClass" ng-src="{{$ctrl.avatarFile}}" width="{{$ctrl.avatarWidth}}" height="{{$ctrl.avatarHeight}}"></img>`,
  bindings: {
    avatar: '<',
    avatarClass: '@'
  },
  replace: true,
  controller: ['$attrs',
    function($attrs) {
      let ctrl = this;

      ctrl.avatarWidth = $attrs.width;
      ctrl.avatarHeight = $attrs.height || $attrs.width;

      let applyChanges = function() {
        if (!ctrl.avatar) {
          return;
        }
        if (ctrl.avatar.includes('/256/') && $attrs.width <= 128) {
          let destSize = 256;
          if ($attrs.width > 64) {
            destSize = 128;
          } else if ($attrs.width > 32) {
            destSize = 64;
          } else if ($attrs.width) {
            destSize = 32;
          }
          ctrl.avatarFile = ctrl.avatar.replace("/256/", "/"+destSize+"/");
        } else {
          ctrl.avatarFile = ctrl.avatar;
        }
      };

      ctrl.$onChanges = function(changes) {
        if (changes.avatar.currentValue) {
          applyChanges();
        }
      };


    }] // end controller
});
