export default angular.module('myApp').component('river', {
  templateUrl: '/templates/river.html',
  bindings: {
    access: '<',
    riverTitle: '@?'
  },
  controller: ['River', '$attrs', 'Localization',
    function(River, $attrs, Localization) {
      let ctrl = this;

      let getUrl = function(river) {
        switch (river.entity.subtype) {
          case 'user':
            return '/profile/'+river.entity.id;
            break;
          case 'event':
            return '/eventos/'+river.entity.slug;
          case 'calendar':
            return '/calendario/';
          case 'file':
            return '/calendario/';
          case 'game':
            return '/juegos/'+river.entity.slug;
            break;
          case 'comment':
            return '/eventos/'+river.entity.entity.slug;
            break;
          case 'accounting_record':
            return '/accounting/';
            break;
          case 'unowned_message':
            return '/site_messages/';
            break;
          case 'bookmark':
            return '/xxxx';
            return '/eventos/'+river.entity.entity.slug;
          case 'unowned_registration':
            return '/xxxx';
            return '/eventos/'+river.entity.entity.slug+'/inbox';
            break;
        }
      };

      let getEntity = function(data) {
        if (data.entity.entity) {
          return data.entity.entity;
        } else {
          return data.entity;
        }
      };

      let onRiver = function(data) {
        data.forEach((data)=>data.description=Localization.describeRiver(data));
        data.forEach((data)=>data.date=new Date(data.updated));
        data.forEach((data)=>data.entity=getEntity(data));
        data.forEach((data)=>data.url=getUrl(data));
        ctrl.list = data;
      }

      let init = function() {
        let args = angular.copy(ctrl.access);
        if ($attrs.limit) {
          args.limit = parseInt($attrs.limit);
        }
        River.query(args, onRiver)
      };

      /*ctrl.$onChanges = function(changes) {
        if (changes.access && (changes.access.currentValue.userId != undefined || changes.access.currentValue.groupId != undefined)) {
          console.log("changes init")
          init();
        }
      };*/

      ctrl.$onInit = function() {
        if (ctrl.access && (ctrl.access.userId != undefined || ctrl.access.groupId != undefined)) {
          init();
        }
      };

    }] // end controller
});

