export default angular.module('myApp').component('accounting', {
  templateUrl: '/templates/accounting/main.html',
  //template: AngularTemplates['/templates/accounting/main.html'],
  bindings: {
    list: '<'
  },
  controller: ['AccountingResource', '$timeout', 'Modals', 'Localization', 'Utils', 'DateUtils',
    function(AccountingResource, $timeout, Modals, Localization, Utils, DateUtils) {
      let ctrl = this;
      ctrl.section = 'general';
      ctrl.action = 'list';
      ctrl.totals = {};
      ctrl.memberFeesObj = {};
      ctrl.memberFees = [];
      ctrl.filters = {sortBy: 'billDate', reversed: false};
      ctrl.$onInit = function() {
        ctrl.updateTotals();
      }
      ;
      /* Sorting */

      ctrl.sortRecord = function(record) {
        if (ctrl.filters.sortBy == 'billDate') {
          return new Date(record.billDate);
        } else {
          return record.title;
        }
      };

      ctrl.setSortBy = function(key) {
        if (ctrl.filters.sortBy == key) {
          ctrl.filters.reversed = !ctrl.filters.reversed;
        } else {
          ctrl.filters.sortBy = key;
          ctrl.filters.reversed = false;
        }
      };

      /* List */

      let parseRecord = function(record) {
        if (record.type == 'member_fees') {
          if (!ctrl.memberFeesObj[record.paidBy]) {
            ctrl.memberFeesObj[record.paidBy] = record;
          } else {
            ctrl.memberFeesObj[record.paidBy] = ctrl.memberFeesObj[record.paidBy].billDate > record.billDate ? ctrl.memberFeesObj[record.paidBy] : record;
          }
        }
      };

      ctrl.updateTotals = function() {
        ctrl.totals.amount = 0;
        ctrl.memberFeesObj = {};
        ctrl.memberFees = [];
        ctrl.totals.count = ctrl.list.length;
        ctrl.list.forEach((elmt) => {
          if (elmt.type != 'deleted') {
            ctrl.totals.amount += parseFloat(elmt.amount);
            parseRecord(elmt);
          }
        });
        ctrl.memberFees = Object.keys(ctrl.memberFeesObj).map((elmt) => ctrl.memberFeesObj[elmt]);
      };

    }] // end controller
});

