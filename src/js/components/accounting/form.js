import formCore from './form-core';

export default angular.module('myApp').component('accountingForm', {
  templateUrl: '/templates/accounting/form.html',
  //template: AngularTemplates['/templates/accounting/form.html'],
  bindings: {
    onCreate: '&',
    onCancel: '&',
    editModel: '<',
    memberFees: '=',
    focus: '@'
  },
  controller: formCore
});
