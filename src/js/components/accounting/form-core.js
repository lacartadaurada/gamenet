export default ['AccountingResource', 'Utils', 'DateUtils', 'Modals', '$attrs',
  function(AccountingResource, Utils, DateUtils, Modals, $attrs) {
    let ctrl = this;
    ctrl.statusOk = false;

    let currentDay = DateUtils.formatDate(new Date());
    let blueprint = {amount: undefined, title: '', type: 'generic', paymentDate: currentDay, billDate: currentDay}

    let onError = function(err) {
      ctrl.statusNok = Utils.getErrorMessage(err);
      console.log(err)
    };


    let onSave = function(form, result) {
      ctrl.onCreate({record: result})
      ctrl.statusOk = true;
      reset(form);
    };

    let findLastMonth = function(who) {
      for (let i=0; i<ctrl.memberFees.length; i++) {
        let elmt = ctrl.memberFees[i];
        if (who == elmt.paidBy) {
          return elmt;
        }
      };
    };

    let onMultiSave = function(results) {
      let lastId = results[results.length-1].id;
      results.forEach((data) => {
        ctrl.onCreate({record: data, last: data.id==lastId});
      });
      ctrl.statusOk = true;
    };

    ctrl.postFee = function(form) {
      if (form.$invalid || !ctrl.model.months || ctrl.model.months == 0) {
        return;
      }
      let lastRecord = findLastMonth(ctrl.model.paidBy);
      if (!lastRecord) {
        Modals.openModal('confirmModal', 'Nuevo socio', '"'+ctrl.model.paidBy + '" es un nuevo socio, seguro?').then((accepted) => {
          if (accepted) {
            let billDate = new Date();
            billDate.setDate(1);
            finishPostFee(form, billDate);
          } else {
            ctrl.statusNok = "El pagador no es correcto"
          }
        });
        // wait for modal to finish
        return;
      }
      finishPostFee(form, lastRecord.billDate);
    };

    let finishPostFee = function(form, billDate) {
      let months = parseInt(ctrl.model.months);
      let nowIso = (new Date()).toISOString();
      let args = angular.copy(ctrl.model);
      args.amount = String(15*months);
      args.groupId = 1;
      delete args.months;

      let nextDate = new Date(billDate);

      let firstDate = DateUtils.addMonths(nextDate, 1);
      let lastDate = DateUtils.addMonths(nextDate, months);

      args.billDate = lastDate.toISOString();
      args.paymentDate = nowIso;
      args.type = 'member_fees';
      let monthsFragment = firstDate.toLocaleDateString('es-ES', {month: 'long'});
      if (months > 1) {
        monthsFragment += "-"+lastDate.toLocaleDateString('es-ES', {month: 'long'});
      }
      args.title = args.paidBy + ' ' + monthsFragment;

      let record = new AccountingResource(args);

      let promise = record.$save().then((res) => onSave(form, res), onError);
    };

    ctrl.post = function(form) {
      if (form.$invalid || !ctrl.model.amount || ctrl.model.amount == 0) {
        return;
      }
      let args = angular.copy(ctrl.model);
      if (!ctrl.editModel) {
        args.groupId = 1;
      }

      args.paymentDate = DateUtils.buildDate(args.paymentDate, "00:00");
      args.billDate = DateUtils.buildDate(args.billDate, "00:00");
      let record = new AccountingResource(args);

      record.$save().then((res) => onSave(form, res), onError);
    };

    ctrl.$onChanges = function(changes) {
      if (changes.editModel.currentValue) {
        ctrl.model = angular.copy(changes.editModel.currentValue);
        ctrl.model.amount = parseFloat(ctrl.model.amount);
        ctrl.model.paymentDate = DateUtils.formatDate(new Date(ctrl.model.paymentDate));
        ctrl.model.billDate = DateUtils.formatDate(new Date(ctrl.model.billDate));
      }
    };

    /* Initialization */

    let reset = function() {
      ctrl.model = angular.copy(blueprint);
    };

    ctrl.$onInit = function() {
      if (ctrl.editModel) {
        ctrl.model = angular.copy(ctrl.editModel);
        ctrl.model.amount = parseFloat(ctrl.model.amount);
        ctrl.model.paymentDate = DateUtils.formatDate(new Date(ctrl.model.paymentDate));
        ctrl.model.billDate = DateUtils.formatDate(new Date(ctrl.model.billDate));
      } else {
        reset();
      }
      Utils.focusSelector($attrs.focus);
    };
  }]; // end controller

