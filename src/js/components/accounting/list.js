export default angular.module('myApp').component('accountingList', {
  templateUrl: '/templates/accounting/list.html',
  //template: AngularTemplates['/templates/accounting/list.html'],
  bindings: {
    list: '=',
    action: '=',
    updateTotals: '&',
    memberFees: '='
  },
  controller: ['AccountingResource', '$timeout', 'Modals', 'Localization', 'Utils', 'DateUtils',
    function(AccountingResource, $timeout, Modals, Localization, Utils, DateUtils) {
      let ctrl = this;
      ctrl.statusOk = false;
      ctrl.editModel = undefined;
      ctrl.accountingTypes = Localization.accountingTypes;
      ctrl.totals = {amount: 0};
      ctrl.filter = {text: '', type: false};
      //ctrl.pagination = {pageSize: 10, currentPage: 0, total: 0, pages: 0};
      ctrl.pagination = {};
      ctrl.filteredList = [];

      /* Filter */

      ctrl.expand = function(registration) {
        registration.expanded = !registration.expanded;
      }

      ctrl.filterChange = function() {
        updateTotals();
      };

      ctrl.setFilterType = function(type, $event) {
        ctrl.filter.type = type == 'all' ? false : type;
        $event.preventDefault();
        updateTotals();
      };

      ctrl.sortRecord = function(record) {
        return new Date(record.paymentDate);
      };

      ctrl.checkFilter = function(elmt) {
        let filter = ctrl.filter.text.toLowerCase();
        if (!(elmt.title.toLowerCase().includes(filter) || (elmt.actor && elmt.actor.toLowerCase().includes(filter)) || Localization.accountingTypes[elmt.type].includes(filter))) {
          return false;
        }
        return !ctrl.filter.type || ctrl.filter.type == elmt.type;
      };

      /* Buttons */

      ctrl.editRegistration = function(registration) {
        ctrl.editModel = registration;
        ctrl.action = 'edit';
      };

      ctrl.onCancelEdit = function(data) {
        ctrl.action = 'list';
        ctrl.editModel = undefined;
      };

      /* List */

      let applyFilter = function() {
        let resultList = [];
        ctrl.list.forEach((elmt) => {
          if (elmt.type != 'deleted' && ctrl.checkFilter(elmt)) {
            resultList.push(elmt);
          }
        });
        ctrl.filteredList = resultList;
      };

      let updateTotals = function() {
        applyFilter();

        ctrl.totals.amount = 0;
        ctrl.filteredList.forEach((elmt) => {
            ctrl.totals.amount += parseFloat(elmt.amount);
        });
        ctrl.totals.count = ctrl.filteredList.length;
      };

      let onListSuccess = function(data) {
        ctrl.list = data;
        updateTotals();
        ctrl.updateTotals();
      };

      /* Delete */

      var onDeleteSuccess = function(id) {
        let index = ctrl.list.findIndex((element) => element.id == id );

        if (index > -1) {
          ctrl.list[index].type = 'deleted';
        } else {
          console.log("not found on array!");
        }
        updateTotals();
        ctrl.updateTotals();
      };

      ctrl.deleteRegistration = function(registration) {
        Modals.openModal('confirmModal', 'Borrar apunte', "Vas a borrar el apunte '"+registration.title+"', seguro??").then((accepted) => {
          if (accepted) {
            AccountingResource.remove({id: registration.id, groupId: registration.groupId}).$promise.then(() => onDeleteSuccess(registration.id));
          }
        });
      };

      /* Create */

      ctrl.onCreate = function(data, last) {
        let isEdit = ctrl.editModel && ctrl.editModel.id && ctrl.editModel.id == data.id;
        ctrl.editModel = undefined;
        ctrl.action = 'list';
        $timeout(function() {
          if (isEdit) {
            let index = ctrl.list.findIndex((element) => data.id == element.id );
            ctrl.list[index] = angular.copy(data);
          } else {
            ctrl.list.push(data);
          }
          $timeout(() => Utils.pulseElement('#registration-'+data.id, 0, 'pulse-element', false));
          if (last == true || last == undefined) {
            updateTotals();
            ctrl.updateTotals();
          }
        });
      };

      /* Initialization */

      let reset = function(form) {
        if (form) {
          form.$setPristine();
        }
        AccountingResource.query({groupId: 1}).$promise.then(onListSuccess);
      };

      ctrl.$onInit = function() {
        updateTotals();
      };

    }] // end controller
});

