import formCore from './form-core';

export default angular.module('myApp').component('accountingFeeForm', {
  templateUrl: '/templates/accounting/form-fee.html',
  //template: AngularTemplates['/templates/accounting/form.html'],
  bindings: {
    onCreate: '&',
    onCancel: '&',
    editModel: '<',
    memberFees: '=',
    focus: '@'
  },
  controller: formCore
});
