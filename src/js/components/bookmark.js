export default angular.module('myApp').component('bookmarks', {
  templateUrl: '/templates/bookmarks.html',
  bindings: {
    model: '<mainModel',
    modelType: '@',
    login: '<'
  },
  controller: ['$scope', 'BookmarkResource', 'VenueResource', 'Modals', 'Utils',
    function($scope, BookmarkResource, VenueResource, Modals, Utils) {
      let ctrl = this;
      Utils.disableAnimation('.bookmark-section');

      var defaultModel = {visibility: 'public'};
      ctrl.bookmarkModel = angular.copy(defaultModel);
      ctrl.bookmarksList = undefined;
      ctrl.isAdding = false;

      /* Buttons */

      ctrl.resetForm = function() {
        ctrl.bookmarkModel = angular.copy(defaultModel);
      };

      ctrl.startAddBookmark = function(selector) {
        ctrl.resetForm();
        ctrl.isAdding = true;
        Utils.focusSelector(selector);
      }

      ctrl.editBookmark = function(bookmark, selector) {
        ctrl.bookmarkModel = angular.copy(bookmark);
        ctrl.isAdding = true;
        Utils.focusSelector(selector);
      }

      ctrl.endAddBookmark = function() {
        ctrl.isAdding = false;
      }

      /* Css helpers */

      var cssClasses = {'generic': 'fas fa-bookmark',
        'facebook': 'fab fa-facebook',
        'instagram': 'fab fa-instagram',
        'whatsapp': 'fab fa-whatsapp',
        'bgg': 'fas fa-bookmark',
        'email': 'fas fa-envelope',
        'youtube': 'fab fa-youtube'}

      var determineCssClass = function(bookmark) {
        return cssClasses[bookmark.type];
      };

      /* List Elements */

      var addBookmark = function(bookmark) {
        var elmt = angular.copy(bookmark);
        elmt = {};
        elmt.userId = bookmark.userId;
        elmt.id = bookmark.id;
        elmt.created = bookmark.created;
        elmt.editable = bookmark.editable;
        elmt.id = bookmark.id;
        elmt.user = bookmark.user;
        elmt.url = bookmark.url;
        elmt.title = bookmark.title;
        elmt.type = bookmark.type;
        elmt.visibility = bookmark.visibility;
        elmt.cssClass = determineCssClass(bookmark);
        return elmt;
      };

      var gotBookmarks = function(bookmarks) {
        var elmts = [];
        bookmarks.forEach(function(elmt) {
          elmts.push(addBookmark(elmt));
        });
        ctrl.bookmarksList = elmts;

        Utils.enableAnimation('.bookmark-section');
      };

      var loadBookmarks = function() {
        return BookmarkResource.query({id: ctrl.model.id, type: ctrl.modelType}).$promise.then(gotBookmarks);
      };

      /* Create and Edit */

      var onBookmarkPostSuccess = function(data, isEdit) {
        let elmt = addBookmark(data);
        if (isEdit) {
          let index = ctrl.bookmarksList.findIndex((element) => element.id == elmt.id );
          ctrl.bookmarksList[index] = elmt;
        } else {
          ctrl.bookmarksList.push(elmt);
        }
        ctrl.resetForm();
        ctrl.endAddBookmark();
      };

      ctrl.postBookmark = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          console.log(ctrl.model);
          return;
        }
        let isEdit = false;
        let args = {title: ctrl.bookmarkModel.title,
          url: ctrl.bookmarkModel.url,
          entity: {id: ctrl.model.id,
            type: ctrl.modelType},
          visibility: ctrl.bookmarkModel.visibility}
        if (ctrl.bookmarkModel.id) {
          args.id = ctrl.bookmarkModel.id;
          isEdit = true;
        }
        let newCard = new BookmarkResource(args);

        let promise = newCard.$save().then((bookmark) => onBookmarkPostSuccess(bookmark, isEdit));
      };

      /* Deletion */

      var onBookmarkDeleteSuccess = function(bookmarkId) {
        let index = ctrl.bookmarksList.findIndex((element) => element.id == bookmarkId );

        if (index > -1) {
          ctrl.bookmarksList.splice(index, 1);
        } else {
          console.log("not found on array!");
        }
      };

      ctrl.deleteBookmark = function(bookmark) {
        Modals.openModal('confirmModal', 'Vas a borrar el enlace', bookmark.url).then((accepted) => {
          if (accepted) {
            BookmarkResource.remove({bookmarkId: bookmark.id}).$promise.then(() => onBookmarkDeleteSuccess(bookmark.id));
          }
        });
      };

      /* Initialization */

      let init = function() {
        loadBookmarks();
        ctrl.bookmarkModel.visibility = ctrl.model.visibility;
      };

      ctrl.$onChanges = function(changes) {
        if (changes.model.currentValue) {
          init();
        }
      };

      ctrl.$onInit = function() {
        // init
      };

    }] // end controller
});
