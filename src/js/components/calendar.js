export default angular.module('myApp').component('calendar', {
  template: `<div class="calendar-container">
                <div id="calendar"></div>
                </div>`,
  controller: ['$scope', 'CalendarResource', '$location', '$state', 'LoginService',
  function($scope, CalendarResource, $location, $state, LoginService) {
    let ctrl = this;
    ctrl.login = LoginService.scope;
    ctrl.createEvent = function() {
      //$location.path("/addevent/");
      $location.path("/event/new/");
    };

    let eventSelected = function(eventData) {
      let event = eventData.event.toJSON();
      //$location.path("/viewevent/"+event.id);
      $state.go("viewevent", { eventId: event.extendedProps.slug });
    };

    let formatEventHook = function(args) {
      let startTime = args.event.start.getHours().toString();
      return { html: "<span class='calendar-event-hour'>" + startTime + "</span> " + args.event.title };
    }

    let formatTooltip = function(args) {
      let startTime = args.event.start.toLocaleTimeString([], {hour: '2-digit', minute:'2-digit'});
      //info.event.extendedProps.slug,
      return "<b>"+args.event.title+"</b><p class='calendar-event-hour'>" + startTime + "</p>"
    }

    let createTooltip = function(info) {
      let tooltip = new bootstrap.Tooltip(info.el, {
        title: formatTooltip(info),
        html: true,
        placement: 'top',
        trigger: 'hover focus',
        container: 'body'
      });
      return tooltip;
    }

    function hookCalendar() {
      document.title = "Calendario - La Carta Daurada"
      var calendarEl = document.getElementById('calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        themeSystem: 'bootstrap5',
        themeSystem: 'standard',
        headerToolbar: { center: 'dayGridMonth,multiMonthYear', right: 'prev,today,next' },
        //height: 600,
        //aspectRatio: 1.8,
        firstDay: 1,
        editable: false,
        selectable: true,
        eventDidMount: function(info) {
          let tooltip = createTooltip(info);
        },
        nowIndicator: true,
        now: new Date().toISOString(),
        nextDayThreshold: '05:00:00',
        eventClick: function(info) {eventSelected(info)},
        // footer controls
        events: '/api/calendar/1/event',
        eventDisplay: 'block',
        eventColor: '#3788d8',
        eventContent: formatEventHook,
        eventDataTransform: function(eventData) {
          delete eventData.editable; // don't allow dragging
          return eventData;
        },
        eventTimeFormat: { // like '14:30:00'
          hour: '2-digit',
          minute: '2-digit',
          omitZeroMinute: true,
          meridiem: false
        },
        views: {
          multiMonthYear: {
            eventContent: formatEventHook,
            selectable: true,
            eventClick: function(info) {eventSelected(info)}
          }
        }
      });
      calendar.setOption('locale', 'es');
      calendar.render();
    };

    ctrl.$onInit = function() {
      hookCalendar();
    }
    }] // end controller
});
