export default angular.module('myApp').component('eventRegistration', {
  templateUrl: '/templates/event-registration/form.html',
  bindings: {
    eventModel: '<',
    eventConfigs: '<eventConfig',
  },
  controller: ['EventResource', 'Localization', 'DateUtils', '$sce',
    function(EventResource, Localization, DateUtils, $sce) {
      let ctrl = this;
      ctrl.statusOk = false;
      ctrl.eventTypes = Localization.eventTypes;

      let cleanModel = {age: '', email: '', sender: ''};

      /* Buttons */

      ctrl.cancel = function(form) {
        reset(form);
      };

      /* Post */

      ctrl.onPostSuccess = function(form) {
        ctrl.statusOk = true;
        reset(form);
      };

      ctrl.postMessage = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          return;
        }
        let pars = angular.copy(ctrl.model);
        pars.id = ctrl.eventModel.id;
        pars.age = parseInt(ctrl.model.age);
        EventResource.register(pars).$promise.then(() => ctrl.onPostSuccess(form));
      };

      /* Initialization */

      let reset = function(form) {
        ctrl.model = angular.copy(cleanModel);
        if (form) {
          form.$setPristine();
        }
      };

      ctrl.$onInit = function() {
        let start = new Date(ctrl.eventModel.start);
        let end = new Date(ctrl.eventModel.end);
        ctrl.eventModel.startDate = DateUtils.formatDate(start);
        ctrl.eventModel.endDate = DateUtils.formatDate(end);
        ctrl.eventModel.startTime = DateUtils.formatTime(start);
        ctrl.eventModel.endTime = DateUtils.formatTime(end);

        if (ctrl.eventConfigs && ctrl.eventConfigs[0]) {
          ctrl.eventConfig = ctrl.eventConfigs[0];
          if (ctrl.eventConfig.description) {
            ctrl.eventConfig.trustedDescription = $sce.trustAsHtml(ctrl.eventConfig.description)
          }
        } else {
          ctrl.eventConfig = undefined;
        }

        reset();
      };
  }] // end controller
});

