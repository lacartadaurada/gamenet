export default angular.module('myApp').component('eventRegistrationInbox', {
  templateUrl: '/templates/event-registration/inbox.html',
  bindings: {
    eventModel: '<',
    eventConfig: '<',
  },
  controller: ['EventResource',
    function(EventResource) {
      let ctrl = this;
      ctrl.statusOk = false;

      /* List */

      let onListSuccess = function(data) {
        ctrl.list = data;
      };

      /* Post */

      ctrl.onPostSuccess = function(form) {
        ctrl.statusOk = true;
        reset(form);
      };

      /* Initialization */

      let reset = function(form) {
        if (form) {
          form.$setPristine();
        }
        EventResource.listRegistrations({id: ctrl.eventModel.id}).$promise.then(onListSuccess);
      };

      ctrl.$onInit = function() {
        reset();
      };
  }] // end controller
});

