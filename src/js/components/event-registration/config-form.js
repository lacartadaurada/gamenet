export default angular.module('myApp').component('eventRegistrationConfig', {
  templateUrl: '/templates/event-registration/config-form.html',
  bindings: {
    eventModel: '<',
    eventConfigs: '<eventConfig'
  },
  controller: ['EventResource', 'Localization', 'Utils',
    function(EventResource, Localization, Utils) {
      let ctrl = this;
      ctrl.statusOk = false;
      ctrl.eventTypes = Localization.eventTypes;

      let cleanModel = {age: 'enabled', email: 'enabled', phone: 'enabled', title: undefined, description: undefined};

      /* Buttons */

      ctrl.cancel = function(form) {
        Utils.softOut("/eventos/"+ctrl.eventModel.slug);
      };

      /* Post */

      ctrl.onPostSuccess = function(form) {
        Utils.softOut("/eventos/"+ctrl.eventModel.slug);
      };

      ctrl.postMessage = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          return;
        }
        let pars = angular.copy(ctrl.model);
        pars.eventId = ctrl.eventModel.id;
        EventResource.setConfig(pars).$promise.then(() => ctrl.onPostSuccess(form));
      };

      /* Initialization */

      let reset = function(form) {
        if (ctrl.eventConfig) {
          ctrl.model = angular.copy(ctrl.eventConfig);
        } else {
          ctrl.model = angular.copy(cleanModel);
        }
        if (form) {
          form.$setPristine();
        }
      };

      ctrl.$onInit = function() {
        if (ctrl.eventConfigs && ctrl.eventConfigs[0]) {
          ctrl.eventConfig = ctrl.eventConfigs[0];
        } else {
          ctrl.eventConfig = undefined;
        }
        reset();
      };
  }] // end controller
});

