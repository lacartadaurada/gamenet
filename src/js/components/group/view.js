export default angular.module('myApp').component('groupView', {
  templateUrl: '/templates/group/view.html',
  bindings: {
    groupModel: '<'
  },
  controller: ['GroupResource', '$timeout', 'Modals', 'Localization', 'Utils', 'DateUtils', 'LoginService',
    function(GroupResource, $timeout, Modals, Localization, Utils, DateUtils, LoginService) {
      let ctrl = this;
      ctrl.login = LoginService.scope;

      let onMembers = function(members) {
        ctrl.members = members;
      };

      ctrl.$onInit = function() {
        GroupResource.members({groupId: ctrl.groupModel.id}).$promise.then(onMembers)
      };

    }] // end controller
});

