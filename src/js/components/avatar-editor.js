export default angular.module('myApp').component('avatarEditor', {
  templateUrl: '/templates/avatar-editor.html',
  bindings: {
    model: '<mainModel',
    modelType: '@',
    login: '<',
    schema: '<',
    viewObject: '&viewObject'
  },
  controller: ['$scope', 'UserResource', 'EventResource', 'GameResource', '$http', 'MediaResource', '$location', 'LoginService', '$rootScope', '$stateParams', 'Localization', 'Utils',
    function($scope, UserResource, EventResource, GameResource, $http, MediaResource, $location, LoginService, $rootScope, $stateParams, Localization, Utils) {
      let ctrl = this;

      ctrl.dropzoneInfo = {status: ''};
      ctrl.filters = {title: ''};

      let categories = {
        game: 'game,event',
        event: 'game,event',
        user: 'user'
      };

      var getFileUrl = function(avatar) {
        return avatar.directUrl || avatar.avatar || '/media/'+avatar.name;
      };

      /* Buttons */

      ctrl.setSection = function(section) {
        ctrl.section = section;
      };

      ctrl.cancelEdit = function() {
        Utils.backOut();
      };

      /* Get List */

      var onMediaResult = function(data) {
        ctrl.avatarList = data;
        if (data.length == 0 && (!ctrl.genericList || ctrl.genericList.length == 0)) {
          ctrl.section = 'add';
        } else {
          ctrl.section = 'listing';
        }
      };

      /* Select avatar */

      var onAvatarSaved = function(data) {
        if (ctrl.modelType == 'event') {
          ctrl.viewObject();
        } else if (ctrl.modelType == 'game') {
          ctrl.viewObject();
        } else {
          ctrl.login.whoami.avatar = data.avatar;
          ctrl.viewObject();
        }
      }

      var avatarParams = function(model) {
        let args = {id: model.id, avatar: model.avatar};
        if (ctrl.modelType == 'user') {
          delete args.id;
        }
        return args;
      }

      var onAvatarUploaded = function(data) {
        let model = ctrl.model;
        let resourceClass = ctrl.schema.getResource();

        model.avatar = getFileUrl(data);

        let args = avatarParams(model);
        let newCard = new resourceClass(args);

        newCard.$save().then(onAvatarSaved);
      };

      ctrl.selectAvatar = function(avatar) {
        onAvatarUploaded(avatar);
      };

      /* Post new Image */

      var onMainFileUploaded = function(file) {
        let cropOptions = { width: 256,
          height: 256,
          imageSmoothingQuality: "medium",
          imageSmoothingEnabled: true };

        let cropper = $('#cropper').data('cropper');

        cropper.api.getCroppedCanvas(cropOptions).toBlob((blob) => {
          let res = $http({
            url: "/api/upload/avatar/",
            method: "POST",
            headers: { "Content-Type": undefined },
            transformRequest: function(data) {
              var formData = new FormData();
              formData.append('file', blob, file.name);
              formData.append('category', ctrl.modelType);
              formData.append('mainFile', file.id);
              return formData;
            },
            data: { }
          }).then(response => onAvatarUploaded(response.data));
        });

      };

      $rootScope.$on('dropzone.addedfile', function(event, data) {
        $scope.$apply(function() {
          onMainFileUploaded(data);
        });
      });

      ctrl.submitAvatar = function() {
        let uploadThumbail = true;

        let cropper = $('#cropper').data('cropper');

        cropper.setParam('alt', ctrl.model.slug);
        cropper.setParam('category', ctrl.modelType);
        cropper.done();
      };
 
      /* Initialization */

      let determineResource = function() {
        let section = ctrl.modelType;

        ctrl.avatarListingType = Localization.actionTitles.avatar[section];
      };

      var getBaseAvatars = function() {
        var baseList = [];
        for (let i=1; i < 11; i++) {
          baseList.push({directUrl: '/images/avatars/avatar0'+i.toString().padStart(2, '0')+'.png', id: 'a'+i})
        }
        return baseList;
      };

      ctrl.$onInit = function() {
        determineResource();

        if (ctrl.modelType == 'user') {
          ctrl.genericList = getBaseAvatars();
        }
 
        let query = {section: 'avatar', category: categories[ctrl.modelType]};
        if (query.category == 'user') {
          query.user_id = ctrl.login.whoami.id;
        }
        MediaResource.query(query).$promise.then(onMediaResult);
      };


    }] // end controller
});
