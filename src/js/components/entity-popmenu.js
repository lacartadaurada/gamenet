export default angular.module('myApp').component('entityPopmenu', {
    template: `<div class="mb-3">
        <div class="dropdown">
          <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-cog"></i>
          </button>
          <ul class="dropdown-menu">
            <li><button ng-click="$ctrl.onEdit()" class="dropdown-item"><i class="fa fa-fw fa-edit"></i>Editar</button></li>
            <li><button ng-click="$ctrl.onEditAvatar()" class="dropdown-item"><i class="fa fa-fw fa-image"></i> Cambiar imagen</button></li>
            <ng-transclude></ng-transclude>
            <li ng-if="$ctrl.onDelete&&$ctrl.model.editable&&$ctrl.login.whoami.id==$ctrl.ownerId"><button ng-click="$ctrl.onDelete({model: $ctrl.model})" class="dropdown-item" type="button"><i class="fa fa-fw fa-times"></i> Eliminar</button></li>
          </ul>
        </div>
`,
  templatex: `
    <div class="input-group mb-3" >
        <button ng-click="$ctrl.onEdit()" class="btn btn-primary">Editar</button>
        <button type="button" class="btn btn-primary dropdown-toggle dropdown-toggle-split" data-bs-toggle="dropdown" aria-expanded="false">
            <span class="visually-hidden">Abrir menú</span>
        </button>
        <ul class="dropdown-menu dropdown-menu-end">
            <li><button ng-click="$ctrl.onEditAvatar()" class="dropdown-item" type="button"><i class="fa fa-fw fa-image"></i> Cambiar imagen</button></li>
            <ng-transclude></ng-transclude>
            <li ng-if="$ctrl.onDelete&&$ctrl.model.editable&&$ctrl.login.whoami.id==$ctrl.ownerId"><button ng-click="$ctrl.onDelete({model: $ctrl.model})" class="dropdown-item" type="button"><i class="fa fa-fw fa-times"></i> Eliminar</button></li>
        </ul>
    </div>
  `,
  transclude: true,
  bindings: {
    model: '<',
    onEdit: '&',
    onEditAvatar: '&',
    onDelete: '&?',
  },
  //replace: true,
  controller: ['LoginService',
    function(LoginService) {
      let ctrl = this;
      ctrl.login = LoginService.scope;

      ctrl.$onInit = function() {
        ctrl.ownerId = ctrl.model.userId || ctrl.model.id;
      };

    }] // end controller
});
