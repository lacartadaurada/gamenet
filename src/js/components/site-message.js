export default angular.module('myApp').component('siteMessage', {
  templateUrl: '/templates/site-message-form.html',
  bindings: {
    hero: '=?'
  },
  controller: ['$element', '$attrs', 'SiteMessage', '$scope',
    function($element, $attrs, SiteMessage, $scope) {
      let ctrl = this;
      ctrl.statusOk = false;

      let cleanModel = {title: '', description: '', sender: ''};

      /* Buttons */

      ctrl.cancel = function(form) {
        reset(form);
      };

      /* Post */

      ctrl.onPostSuccess = function(form) {
        ctrl.statusOk = true;
        reset(form);
      };

      ctrl.postMessage = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          return;
        }
        let args = {description: ctrl.model.description, title: ctrl.model.title, sender: ctrl.model.sender}
        let newCard = new SiteMessage(args);

        let promise = newCard.$save().then(() => ctrl.onPostSuccess(form));
      };

      /* Initialization */

      let reset = function(form) {
        ctrl.model = angular.copy(cleanModel);
        if (form) {
          form.$setPristine();
        }
      };

      ctrl.$onInit = function() {
        reset();
      };
  }] // end controller
});

