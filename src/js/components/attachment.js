export default angular.module('myApp').component('attachments', {
  templateUrl: '/templates/attachments.html',
  bindings: {
    model: '<mainModel',
    modelType: '@',
    login: '<'
  },
  controller: ['$scope', '$rootScope', 'VenueResource', 'AttachmentResource', 'Modals', 'Utils',
    function($scope, $rootScope, VenueResource, AttachmentResource, Modals, Utils) {
      let ctrl = this;

      Utils.disableAnimation('.attachment-section');
      ctrl.attachmentModel = {};
      ctrl.attachmentsList = undefined;
      ctrl.isAdding = false;
      ctrl.isEditing = false;

      /* Buttons */

      ctrl.startAddAttachment = function() {
        ctrl.isAdding = true;
        ctrl.isEditing = false;
      }
      ctrl.endAddAttachment = function() {
        ctrl.isAdding = false;
        ctrl.isEditing = false;
      }

      ctrl.editAttachment = function(attachment, selector) {
        ctrl.attachmentModel = angular.copy(attachment);
        ctrl.isAdding = false;
        ctrl.isEditing = true;
        Utils.focusSelector(selector);
      }

      /* List */

      var addAttachment = function(attachment) {
        let elmt = {};
        elmt.id = attachment.id;
        elmt.name = attachment.name;
        elmt.alt = attachment.alt;
        elmt.editable = attachment.editable;
        elmt.size = attachment.size;
        elmt.visibility = attachment.visibility;
        return elmt;
      };

      var gotAttachments = function(attachments) {
        var elmts = [];
        attachments.forEach(function(elmt) {
          elmts.push(addAttachment(elmt));
        });
        ctrl.attachmentsList = elmts;
        Utils.enableAnimation('.attachment-section');
      };

      var loadAttachments = function() {
        return AttachmentResource.query({entityId: ctrl.model.id, entityType: ctrl.modelType}).$promise.then(gotAttachments);
      };

      /* Create and Edit */

      var onAttachmentPostSuccess = function(data, isEdit) {
        let elmt = addAttachment(data);
        let index = ctrl.attachmentsList.findIndex((element) => element.id == elmt.id );
        if (index == -1) {
          ctrl.attachmentsList.push(elmt);
        } else {
          // TODO: Should flash the element in some way
          if (!isEdit) {
            Utils.pulseElement('.attachment-element', index, 'pulse-element');
          }
          ctrl.attachmentsList[index] = elmt;
        }
        ctrl.attachmentModel.content = "";
        ctrl.endAddAttachment();
      };

      $rootScope.$on('dropzone.addedfile', function(event, data) {
        $scope.$apply(function() {
          onAttachmentPostSuccess(data);
        });
      });

      ctrl.postAttachment = function(form) {
        if (!form.$valid) {
          console.log("Not valid");
          console.log(form);
          console.log(ctrl.model);
          return;
        }
        let args = {entityId: ctrl.model.id,
          entityType: ctrl.modelType,
          id: ctrl.attachmentModel.id,
          childType: 'file',
          alt: ctrl.attachmentModel.alt,
          visibility: ctrl.attachmentModel.visibility};

        let isEdit = false;
        if (ctrl.attachmentModel.id) {
          args.id = ctrl.attachmentModel.id;
          isEdit = true;
        }
        let newCard = new AttachmentResource(args);

        let promise = newCard.$save().then((data) => onAttachmentPostSuccess(data, isEdit));
      };

      /* Deletion */

      var onAttachmentDeleteSuccess = function(attachmentId) {
        let index = ctrl.attachmentsList.findIndex((element) => element.id == attachmentId );

        if (index > -1) {
          ctrl.attachmentsList.splice(index, 1);
        } else {
          console.log("not found on array!");
        }
      };

      ctrl.deleteAttachment = function(attachment) {
        Modals.openModal('confirmModal', 'Vas a borrar el fichero', attachment.name).then((accepted) => {
          if (accepted) {
            AttachmentResource.remove({entityId: ctrl.model.id, entityType: ctrl.modelType, attachmentId: attachment.id}).$promise.then(() => onAttachmentDeleteSuccess(attachment.id));
          }
        });
      }

      /* Startup */

      let init = function() {
        loadAttachments();
        ctrl.attachmentModel.visibility = ctrl.model.visibility;
      };

      ctrl.$onChanges = function(changes) {
        if (changes.model.currentValue) {
          init();
        }
      };

      ctrl.$onInit = function() {
        //init();
      }
    }] // end controller
});
