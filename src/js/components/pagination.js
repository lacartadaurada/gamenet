export default angular.module('myApp').component('pagination', {
  template: `<nav aria-label="Navegación">
                <ul class="pagination">
                    <li class="page-item" ng-class="{disabled: $ctrl.pagination.currentPage==0}" ng-click="$ctrl.prevPage($event)"><a class="page-link" href="#" aria-label="Anterior"><span aria-hidden="true">&laquo;</span></a></li>
                    <li class="page-item" ng-class="{disabled: $ctrl.pagination.currentPage==$index}" ng-repeat="i in [].constructor($ctrl.pagination.pages) track by $index"><a ng-click="$ctrl.setPage($index, $event)" class="page-link" href="#">{{$index+1}}</a></li>
                    <li class="page-item" ng-class="{disabled: $ctrl.pagination.currentPage&gt;=$ctrl.pagination.pages-1}" ng-click="$ctrl.nextPage($event)"><a class="page-link" href="#" aria-label="Siguiente"><span aria-hidden="true">&raquo;</span></a></li>
                </ul>
            </nav>`,
  bindings: {
    list: '<',
    pagination: '='
  },
  controller: ['$scope',
    function($scope) {
      let ctrl = this;

      let initPagination = function() {
        if (ctrl.pagination.currentPage == undefined) {
          // initialization
          ctrl.pagination.currentPage = 0;
          ctrl.pagination.pageSize = 10;
        }
      };

      let updatePagination = function() {
        initPagination();
        let count = ctrl.list.length;
        ctrl.pagination.total = count;
        ctrl.pagination.pages = parseInt(count/ctrl.pagination.pageSize);

        if (count%ctrl.pagination.pageSize) {
          ctrl.pagination.pages += 1;
        }

        if (ctrl.pagination.currentPage >= ctrl.pagination.pages) {
          ctrl.pagination.currentPage = 0;
        }
      };

      ctrl.setPage = function(n, $event) {
        $event.preventDefault();
        ctrl.pagination.currentPage = n;
      };

      ctrl.nextPage = function($event) {
        $event.preventDefault();
        ctrl.pagination.currentPage += 1;
      };

      ctrl.prevPage = function($event) {
        $event.preventDefault();
        ctrl.pagination.currentPage -= 1;
      };

      ctrl.$onChanges = function(changes) {
        if (changes.list.currentValue) {
          updatePagination();
        }
      };

    }] // end controller
});
