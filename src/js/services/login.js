export default angular.module('myApp').factory('LoginService', ['$http', 'CryptoUtils', '$q', function($http, CryptoUtils, $q) {

  let hashSalt = 'lacartadaurada';
  var scope = {loginReady: false};
  scope.whoami = {username: ''};
  scope.isLoggedIn = false;

  var checkUser = function(user) {
    scope.whoami = angular.copy(user);
    scope.isLoggedIn = user.username != 'anon';
    scope.loginReady = true;
  };

  var onLogin = function(data) {
    checkUser(data.data);
    return data.data;
  };

  var onLogout = function(data) {
    scope.whoami = {username: 'anon'};
    scope.isLoggedIn = false;
    return data.data;
  };

  var onError = function(err) {
    console.log("error on whoami!");
  }

  var fetchCurrentUser = function() {
    service.whoami().then((data) => {
      checkUser(data);
    }, onError);
  };

  let hashPasswords = function(data, password_fields) {
    let promises = password_fields.map(field => CryptoUtils.passwordHash(data[field], hashSalt));
    return $q.all(promises).then(results => {
      let temp = angular.copy(data);
      let i = 0;
      password_fields.forEach(field => { temp[field] = results[i++]; });
      return temp;
    });
  };

  let hashAndPost = function(data, password_fields, url) {
    return $q((resolve, reject) => {
      hashPasswords(data, password_fields).then(newData => {
        $http.post(url, newData).then((res) => resolve(res.data), reject);
      }, reject);
    });
  };

  var service = {
    scope: scope,
    whoami: function() {
      return $http.post("/api/whoami").then((res) => res.data);
    },
    login: function(data) {
      let temp = angular.copy(data);
      return $q((resolve, reject) => {
        CryptoUtils.passwordHash(temp.password, hashSalt).then((res) => {
          temp.password = res;
          $http.post("/api/login", temp).then(onLogin).then(resolve, reject);
        }, reject);
      });
    },
    register: function(data) {
      return hashAndPost(data, ['password', 'password_confirm'], "/api/register");
    },
    password_reset_request: function(data) {
      // sends userId (email or username)
      return $http.post("/api/password/reset_request", data).then((res) => resolve(res.data), reject);
    },
    password_reset: function(data) {
      // also sends token
      return hashAndPost(data, ['password', 'password_confirm'], "/api/password/reset");
    },
    password_change: function(data) {
      return hashAndPost(data, ['password', 'new_password', 'new_password_confirm'], "/api/password/change");
    },
    logout: function() {
      return $http.post("/api/logout").then(onLogout);
    }
  };

  fetchCurrentUser();
  return service;

}]);
