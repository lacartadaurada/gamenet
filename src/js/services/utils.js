export default angular.module('myApp').factory('Utils', ['$timeout', '$animate', '$window', '$location', function($timeout, $animate, $window, $location) {
  let getErrorMessage = function(err) {
    let errorMsg;
    if (err.data && err.data.error) {
      errorMsg = err.data.error;
    } else {
      errorMsg = "error: " + err.statusText;
    }
    return errorMsg;
  };
  let focusSelector = function(selector) {
    if (selector) {
      $timeout(() => $(selector).focus());
    }
  };
  let disableAnimation = function(selector) {
    $animate.enabled($(selector)[0], false);
  };

  let enableAnimation = function(selector) {
    $timeout(() => $animate.enabled($(selector)[0], true));
  };

  let pulseElement = function(selector, index, pulseClass, doTimeout) {
    pulseClass = pulseClass || 'pulse-element';
    doTimeout = doTimeout==undefined ? true : doTimeout;

    let doPulse = () => {
      $($(selector)[index]).addClass(pulseClass)
    };

    if (doTimeout) {
      $timeout(doPulse);
    } else {
      doPulse();
    }
  };
  var forceOut = function(path, deregister) {
    if (deregister) {
      deregister();
    }
    $window.location.href = path;
  };

  var softOut = function(path, deregister) {
    if (deregister) {
      deregister();
    }
    $location.path(path);
  };

  var backOut = function(deregister) {
    if (deregister) {
      deregister();
    }
    $window.history.back();
  };

  var service = {
    getErrorMessage: getErrorMessage,
    focusSelector: focusSelector,
    enableAnimation: enableAnimation,
    disableAnimation: disableAnimation,
    pulseElement: pulseElement,
    forceOut: forceOut,
    softOut: softOut,
    backOut: backOut
  }
  return service;
}]);
