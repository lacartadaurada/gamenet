export default angular.module('myApp').factory('Modals', ['$q', function($q) {

  let service = {};
  let allModals = [];

  service.registerModal = function(nodeId, scope) {
    let modal = new bootstrap.Modal('#'+nodeId, {})

    allModals[nodeId] = {modal: modal, scope: scope};
    return modal;
  };

  service.getModal = function(nodeId) {
    return allModals[nodeId];
  };

  service.openModal = function(nodeId, title, body) {
    let modalData = allModals[nodeId];
    let modalElement = $('#'+nodeId);
    let modal = modalData.modal;
    let deferred = $q.defer();

    modalData.scope.title = title;
    modalData.scope.body = body;
    modal.show();
    modalElement.on('hidden.bs.modal', function() {
      modalElement.off('hidden.bs.modal');

      modalData.scope.$apply(() => deferred.resolve(modalData.scope.result));
    });
    return deferred.promise;
  };

  return service;
}]);
