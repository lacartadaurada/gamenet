export default angular.module('myApp').factory('Localization', ['$q', function($q) {
  let service = {};

  service.eventTypes = {
    single: 'Partida',
    tournament: 'Torneo',
    league: 'Liga',
    event: 'Evento',
    social: 'Evento social',
    workshop: 'Taller',
    proposal: 'Propuesta',
    presentation: 'Presentación',
  };

  service.entityTypes = {
    user: 'usuario',
    event: 'evento',
    calendar: 'calendario',
    file: 'fichero',
    bookmark: 'marcador',
    game: 'juego',
    comment: 'comentario',
    accounting_record: 'apunte de contabilidad',
    unowned_message: 'mesaje del sitio',
    unowned_registration: 'registro en evento'
  };

  service.entityPaths = {
    user: '/profile/:id',
    event: '/eventos/:slug',
    calendar: '/calendario/',
    file: '<parent>',
    bookmark: '<parent>',
    game: '/juegos/:slug',
    comment: '<parent>',
    accounting_record: '/accounting/',
    unowned_message: '/site_messages/',
    unowned_registration: '<parent>/inbox'
  };


  service.riverActionVerbs = {
    create: 'creado',
    update: 'editado',
    'comment:create': 'comentado',
    'bookmark:create': 'creado un marcador',
    'bookmark:update': 'editado un marcador',
    'file:create': 'añadido un fichero',
    'file:update': 'editado un fichero',
  };

  service.describeRiver = function(river) {
    let action = service.riverActionVerbs[river.action];
    if (river.entity.entity) {
      return service.riverActionVerbs[river.entity.subtype+':'+river.action] + ' en';
    } else {
      return action+' el ' + service.entityTypes[river.entity.subtype];
    }
  };

  service.actionTitles = {
    event: {
      edit: 'Editar Evento',
      new: "Crear Evento"
    },
    game: {
      edit: 'Editar Juego',
      new: "Crear Juego",
      list: "Lista Juego"
    },
    avatar: {
      event: 'Tus imágenes de evento',
      profile: 'Tus avatars',
      game: 'Tus imágenes de juegos'
    }
  };

  service.accountingTypes = {
    generic: 'Genérico',
    member_fees: 'Cuota',
    donation: 'Donación',
    event_expense: 'Gasto evento',
    event_income: 'Ganancias evento',
    coinpot: 'Hucha',
    deleted: 'Borrado',
    place: 'Local',
    consumables: 'Consumibles'
  };

  service.visibilityTooltips = {
    private: 'Privado: Solo tu puedes verlo.',
    members: 'Red: Solo registrados pueden verlo.',
    group: 'Grupo: Solo grupo.',
    public: 'Público: Todo el mundo puede verlo.'
  };
  service.permissionsTooltips = {
    user: 'Solo quien lo ha creado puede editarlo.',
    group: 'Cualquiera en el grupo la carta daurada puede editarlo.',
    members: 'Los usuarios registrados pueden editarlo.'
  };

  return service;
}]);
