export default angular.module('myApp').factory('CryptoUtils', ['$http', function($http) {

  function failAndLog(err) {
    console.log(err);
  };

  function bytesToASCIIString(bytes)
  {
    return String.fromCharCode.apply(null, new Uint8Array(bytes));
  }

  function utf8ToUint8Array(str)
  {
    let encoder = new TextEncoder();
    let buffer = new Uint8Array(str.length*4);
    let res = encoder.encodeInto(str, buffer);
    return buffer.slice(0, res.written);
  }

  var generateSalt = function(length) {
    length = length || 32;
    salt = crypto.getRandomValues(new Uint8Array(length));
  };

  var pbkdf2 = function(key, salt)
  {
    return crypto.subtle.importKey("raw", key, "PBKDF2", false, ["deriveBits"]).then(function(baseKey) {
      return crypto.subtle.deriveBits({name: "PBKDF2", salt: salt, iterations: 260000, hash: "sha-256"}, baseKey, 256);
    }, failAndLog);
  };

  function sha256(data) {
    return crypto.subtle.digest("SHA-256", data);
  }

  function passwordHash(key, salt) {
    let saltEnc = utf8ToUint8Array(salt);
    let keyEnc = utf8ToUint8Array(key);

    return sha256(saltEnc).then((digest) => pbkdf2(keyEnc, digest).then((derived) => btoa(bytesToASCIIString(derived))));
  }

  var service = {
    utf8ToUint8Array: utf8ToUint8Array,
    pbkdf2: pbkdf2,
    sha256: sha256,
    generateSalt: generateSalt,
    passwordHash: passwordHash
  }
  return service;
}]);
