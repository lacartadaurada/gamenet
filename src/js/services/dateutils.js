export default angular.module('myApp').factory('DateUtils', ['$http', function($http) {
  let formatVdate = function(date) {
    res = date.getFullYear()+(date.getMonth()+1).toString().padStart(2, '0')+date.getDate().toString().padStart(2, '0');
    return res;
  };
  let formatVevent = function(event) {
    // geo 40.05210576181041, 0.061076507346650585
    // uri for qr: geo:52.5162699,13.3777034
    let startDate = buildDate(event.startDate, event.startTime, true);
    let endDate = buildDate(event.endDate, event.endTime, true);

    let res = "BEGIN:VEVENT\n"
    res += "SUMMARY:"+event.title+"\n"
    res += "DESCRIPTION:\n"
    res += "LOCATION:\n"
    res += "DTSTART:"+formatVdate(startDate)+"\n"
    res += "DTEND:"+formatVdate(endDate)+"\n"
    res += "END:VEVENT"
    return res
  };

  let formatDate = function(aDate) {
    return aDate.getDate().toString().padStart(2, '0')+'/'+(aDate.getMonth()+1).toString().padStart(2, '0')+'/'+aDate.getFullYear();
  };

  let formatTime = function(aDate) {
    return aDate.getHours().toString().padStart(2, '0')+':'+aDate.getMinutes().toString().padStart(2, '0')
  };

  let buildDate = function(date, time, raw) {
    var dateParts = date.split("/");
    var timeParts = time.split(":");
    var finalDate = new Date(dateParts[2], dateParts[1]-1, dateParts[0], timeParts[0], timeParts[1]);
    if (raw)
      return finalDate;
    return finalDate.toISOString();
  };

  let addMonths = function(date, months) {
    date = new Date(date);
    var d = date.getDate();
    date.setMonth(date.getMonth() + months);
    if (date.getDate() != d) {
      date.setDate(0);
    }
    return date;
  };

  var service = {
    formatVevent: formatVevent,
    formatDate: formatDate,
    formatTime: formatTime,
    buildDate: buildDate,
    addMonths: addMonths
  }
  return service;
}]);
