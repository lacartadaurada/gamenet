# lacarta

## About

Website to manage rpg/gaming club.

### Features:

- Events / Calendar
- Game DB
- Image editing for element thumbnails

### Built using:

* hugo: for static templating
* flask: for api server
* angularjs: for client side application
* bootstrap 5: css framework
* other js libs
  * fullcalendar
  * dropzone
  * cropperjs
  * trumbowyg
  * tempus-dominus
  * ...

## Project structure

* Main dirs:
  * flaskr: flask application
  * hugo: hugo files
  * src: js application
  * db: db init scripts
  * docs: documentation
  * instance: private flask app configuration
  * migrations: flask app migrations
* Other files:
  * requirements.txt: pip dependencies
  * run.sh: runs the flask app
  * compile.sh: compiles the hugo app
    * tpl_compile.py

## Setting up

### configuration

hugo: Set values in hugo/config/production/config.toml if you want to override defaults.
flask: Set db values and others inside instance/application.cfg
shell: Set db values inside instance/env

### dependencies and python environment

For debian/ubuntu:

```
apt install python3-venv language-pack-es hugo

python -m venv ../venv/
source ../venv/bin/activate

pip install -r requirements.txt
```

### database

apt install mariadb-server

```
create database 'lacartadaurada';
CREATE USER 'user'@'localhost' IDENTIFIED BY 'foobarpass';
GRANT ALL PRIVILEGES ON lacartadaurada.* TO 'user'@'localhost';
```

### nginx

There's a sample vhost file at docs/lacarta.nginx.conf, place at /etc/nginx/sites-available/.

### systemd

There's a .service file you can adapt at [docs/lacarta.service](docs/lacarta.service). It should be placed at /etc/systemd/system/lacarta.service.

#### Systemd commmands

```
systemctl restart lacarta
systemctl status lacarta
journalctl -f -n 100 -u lacarta
```

## ctlcarta

Use the ctlcarta command to perform admin tasks instead of systemd/shell directly:

```
./ctlcarta restart
./ctlcarta status
./ctlcarta log
```

## Other

DB MIGRATIONS

flask --app flaskr db init
flask --app flaskr db migrate -m "Initial migration."

## License

AGPLv3 Copyright 2023 Pablo Martín

See COPYING file for more details.


