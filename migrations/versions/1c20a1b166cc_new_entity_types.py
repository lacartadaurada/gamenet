"""New Entity Types

Revision ID: 1c20a1b166cc
Revises: 06107bd5deb1
Create Date: 2023-12-17 23:59:27.664843

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1c20a1b166cc'
down_revision = '06107bd5deb1'
branch_labels = None
depends_on = None


def upgrade():
    values = "'user','event','calendar','group','bookmark','file','game','comment','accounting_record','unowned_message','unowned_registration'"

    for table in ['annotations', 'comments', 'river', 'relations', 'registration_configs', 'unowned_messages', 'unowned_registrations', 'comments']:
        op.execute("ALTER TABLE %s MODIFY COLUMN entity_type enum(%s)" % (table, values))

def downgrade():
    pass

