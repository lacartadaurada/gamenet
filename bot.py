#!/usr/bin/env python

import logging
import asyncio
import aiohttp
from aiohttp import web
application = None
import json

from telegram import ForceReply, Update, helpers
from telegram.ext import Application, CommandHandler, ContextTypes, MessageHandler, filters

from flaskr import create_app, db
from flaskr.models import User, TelegramSubscription
app = create_app()
app.app_context()
app.app_context().push()

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

registered_users = []

# set higher logging level for httpx to avoid all GET and POST requests being logged

logging.getLogger("httpx").setLevel(logging.WARNING)
logger = logging.getLogger(__name__)

def remove_job_if_exists(name: str, context: ContextTypes.DEFAULT_TYPE) -> bool:
    """Remove job with given name. Returns whether job was removed."""
    current_jobs = context.job_queue.get_jobs_by_name(name)
    if not current_jobs:
        return False
    for job in current_jobs:
        job.schedule_removal()
    return True


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a deep-linked URL when the command /start is issued."""
    bot = context.bot
    text = "hecho!"
    chat_id = update.effective_message.chat_id

    if not TelegramSubscription.is_subscribed(chat_id):
        TelegramSubscription.create(chat_id)

    await update.message.reply_text(text)

async def stop(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a deep-linked URL when the command /start is issued."""
    bot = context.bot
    text = "hecho!"
    chat_id = update.effective_message.chat_id

    if TelegramSubscription.is_subscribed(chat_id):
        TelegramSubscription.remove(chat_id)

    await update.message.reply_text(text)


async def send_mention(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /start is issued."""
    user = update.effective_user
    await update.message.reply_html(
        rf"Hi {user.mention_html()}!",
        reply_markup=ForceReply(selective=True),
    )

async def help_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Send a message when the command /help is issued."""
    chat_id = update.effective_message.chat_id
    if TelegramSubscription.is_subscribed(chat_id):
        await update.message.reply_text("Usa /stop para desuscribirte.")
    else:
        await update.message.reply_text("Usa /start para suscribirte.")

async def echo(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Echo the user message."""
    await update.message.reply_text(update.message.text)

async def unknown(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="No entiendo ese comando.")

async def process_message(context: ContextTypes.DEFAULT_TYPE, *args) -> None:
    job = context.job
    msg = job.data
    for user_id in TelegramSubscription.list():
        print("notify", user_id)
        await context.bot.send_message(int(user_id), text=f"{msg}")
   
async def handle(request):
    data_json = await request.text()
    data = json.loads(data_json)
    name = request.match_info.get('name', "Anonymous")
    text = "Hello, " + name
    application.job_queue.run_once(process_message, 0, data=data['text'], name=str(text))

    return web.Response(text=text)

def start_web_server(application):
    wapp = web.Application()
    wapp.add_routes([web.post('/', handle),
                     web.post('/{name}', handle)])

    loop = asyncio.get_event_loop()
    runner = aiohttp.web.AppRunner(wapp)
    loop.run_until_complete(runner.setup())
    site = aiohttp.web.TCPSite(runner, app.config['TELEGRAM_BOT_HOST'], app.config['TELEGRAM_BOT_PORT'])
    loop.run_until_complete(site.start())

def main() -> None:
    global application
    """Start the bot."""

    # Create the Application and pass it your bot's token.
    application = Application.builder().token(app.config['TELEGRAM_TOKEN']).build()

    # on different commands - answer in Telegram
    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("stop", stop))
    application.add_handler(CommandHandler("help", help_command))


    # on non command i.e message - echo the message on Telegram
    #application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, echo))
    application.add_handler(MessageHandler(filters.COMMAND, help_command))
    application.add_handler(MessageHandler(filters.TEXT, help_command))
    start_web_server(application)

    # Run the bot until the user presses Ctrl-C
    application.run_polling(allowed_updates=Update.ALL_TYPES)



if __name__ == "__main__":

    main()


