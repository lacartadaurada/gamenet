import os
import logging

from flask import Flask

from flask import g
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager, current_user
from flask_apscheduler import APScheduler
#from flask_wtf import CSRFProtect
#csrf = CSRFProtect()

db = SQLAlchemy()
login_manager = LoginManager()
from werkzeug.middleware.proxy_fix import ProxyFix

def create_app(test_config=None):
    from . import models
    from . import database
    from .views import register_blueprints
    from .auth.views import auth_blueprint
    from .models import User, Group, AnonymousUser
    from .config import ProductionConfig
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True)

    @app.before_request
    def load_user():
        g.group_ids = list(current_user.get_group_ids())

    if __name__ != '__main__':
        gunicorn_logger = logging.getLogger('gunicorn.error')
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)
    #csrf.init_app(app)

    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_object(ProductionConfig())
        app.config.from_pyfile('application.cfg', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    nproxy = app.config['NPROXIES']
    app.wsgi_app = ProxyFix(
        app.wsgi_app, x_for=nproxy, x_proto=nproxy, x_host=nproxy, x_prefix=nproxy
    )
    scheduler = APScheduler()
    scheduler.init_app(app)
    scheduler.start()
    db.init_app(app)
    migrate = Migrate(app, db)
    database.init_app(app)
    login_manager.anonymous_user = AnonymousUser
    login_manager.init_app(app)

    # Set up flask login.
    @login_manager.user_loader
    def get_user(id):
        return User.query.get(int(id))

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.register_blueprint(auth_blueprint)
    register_blueprints(app)
    app.add_url_rule('/', endpoint='index')

    return app

