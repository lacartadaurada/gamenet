from html_sanitizer import Sanitizer

sanitizer = Sanitizer({'whitespace': set(),
                       'empty': {'hr', 'a', 'br', 'p'},})

