from .. import db

from ..models.entity_link import EntityLink
from ..models.user import User


def fill_entities(elements):
    for elmt in elements:
        if not 'entity' in elmt:
            continue
        entity_type = elmt['entity']['type']
        entity = EntityLink.get_entity(entity_type, elmt['entity']['id'])
        # try
        elmt['entity'] = entity.to_avatar_json()
        #except:
        #    elmt['entity'] = entity.to_json()
        #elmt['entity']['entity_type'] = entity_type
    return elements

def fill_users(elements):
    ids = map(lambda s: s['userId'], elements)

    elmts = db.session.scalars(db.select(User).filter(User.id.in_(ids)))
    avatars = {}
    for elmt in elmts:
        avatars[elmt.id] = elmt.to_avatar_json()
    for element in elements:
        user = avatars[element['userId']]
        element['user'] = avatars[element['userId']]
        del element['userId']
    return elements


