from flask import Blueprint, render_template, url_for, redirect, flash, request, g
from flask_login import login_user, logout_user, login_required
from werkzeug.security import check_password_hash
import flask_login

from .. import db
#from .. import csrf
from ..models import User
from ..models import ValidationToken
from .forms import LoginForm, RegistrationForm
from ..email import send_site_email

from ..notify import notify_text

import random

auth_blueprint = Blueprint('auth', __name__)

#@csrf.exempt
@auth_blueprint.route('/whoami', methods=['POST'])
def whoami():
    user = flask_login.current_user
    #print(dir(request))
    #print('host:',request.headers['Host'])
    #print('for:',request.headers['X-Forwarded-For'])
    #print('proto:', request.headers['X-Forwarded-Proto'])
    #print('host:', request.headers['X-Forwarded-Host'])
    #print('prefix', request.headers['X-Forwarded-Prefix'])
    #X-Forwarded-For: 192.168.3.105
    #X-Forwarded-Proto: https
    #X-Forwarded-Host: lacarta
    #X-Forwarded-Prefix: /
    #print('remote', request.remote_addr)
    return {'username': user.username, 'id': user.id, 'avatar': user.avatar, 'groups': g.group_ids}

def default_avatar():
    n = random.randint(1, 10)
    n = str(n).rjust(2, '0')
    return '/images/avatars/avatar0'+n+'.png'

@auth_blueprint.route('/register', methods=['POST'])
def register():
    form = request.get_json()

    username = form.get('username', None)
    email = form.get('email', None)
    if not email:
        email = None
    password = form.get('password', None)
    password_confirm = form.get('password_confirm', None)
    avatar = form.get('avatar', default_avatar())

    if not password == password_confirm:
        return {"result": "error", "error": "Las contraseñas no coinciden"}, 400
    if not username.isidentifier() or not username.islower() or len(username) < 2:
        return {"result": "error", "error": "El nombre de usuario es inválidos"}, 400
    if username and password and password_confirm:
        user = User(username=username, password=password, email=email, avatar=avatar, activated=True)
        user.save()
        login_user(user)
        notify_text("Nuevo usuario registrado")
        return {"result": "ok"}
    return {"result": "error", "error": "Credenciales Incorrectas"}, 400


@auth_blueprint.route('/login', methods=['POST'])
def login():
    form = request.get_json()
    user_id = form.get('user_id', None)
    password = form.get('password', None)

    if user_id and password:
        user = User.authenticate(user_id, password)
        if user is not None:
            login_user(user)
            return {'username': user.username, 'id': user.id}
    return {"result": "error", "error": "Credenciales Incorrectas"}, 401


@auth_blueprint.route('/logout', methods=['POST'])
@login_required
def logout():
    logout_user()
    return {"result": "ok"}

###############################
# password change

@auth_blueprint.route('/password/change', methods=['POST'])
@login_required
def password_change():
    args = request.get_json()
    password = args['password']
    new_password = args['new_password']
    new_password_confirm = args['new_password_confirm']
    if new_password != new_password_confirm:
        return {"error": "Las contraseñas no coinciden"}, 400
    if check_password_hash(flask_login.current_user.password, password):
        flask_login.current_user.assign({'password': new_password}, ['password'])
        db.session.commit()
        return {"result": "ok"}
    return {"error": "Credenciales incorrectas"}, 401

@auth_blueprint.route('/password/reset', methods=['POST'])
@login_required
def password_reset():
    args = request.get_json()
    password = args['password']
    password_confirm = args['password_confirm']
    token = args['token']
    res = ValidationToken.search(ValidationToken.action == 'password',
                                 ValidationToken.code == token).first()
    if res and password == password_confirm:
        user = db.get_or_404(User, res.user_id)
        user.assign({'password': password}, ['password'])
        db.session.commit()
        return {"result": "ok"}
    else:
        return {"error": "bad parameters"}, 400
    return {'code': token.code}

@auth_blueprint.route('/password/reset_request', methods=['POST'])
@login_required
def password_reset_request():
    args = request.get_json()
    user_id = args.get("userId")
    user = User.search(db.or_(User.email == user_id, User.username == user_id)).first()
    if user and user.email:
        token = ValidationToken.create(user.id, 'password')
        #send_site_email(to, subject, body)
    return {"result": "ok"}

