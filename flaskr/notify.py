import urllib.request
import json
import time

from flask import current_app as app

def send_request(data, host, port):
    #time.sleep(5)
    # Convert string to byte
    data = json.dumps(data)
    # Convert to String
    data = str(data)
    data = data.encode('utf-8')
    req = urllib.request.Request(f"http://{host}:{port}/connected", data, method="POST")
    req.add_header('Content-Type', 'application/json; charset=utf-8')
    try:
        urllib.request.urlopen(req)
    except:
        print("Problem notifying telegram bot")


def notify(data):
    host = app.config['TELEGRAM_BOT_HOST']
    port = app.config['TELEGRAM_BOT_PORT']
    app.apscheduler.add_job(func=send_request, trigger='date', args=[data, host, port], id='j4')

def notify_text(text):
    notify({'text': text})
