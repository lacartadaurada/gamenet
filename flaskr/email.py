# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.message import EmailMessage

def send_email(from_email, to_email, subject, body):
    msg = EmailMessage()
    msg.set_content(body)
    #msg.add_alternative(html)

    msg['Subject'] = f'Test'
    msg['From'] = from_email
    msg['To'] = to_email

    s = smtplib.SMTP('localhost')
    #smtp.connect('localhost', 26)
    #smtp.login('user@domain', 'pass')

    s.send_message(msg)
    s.quit()

def send_site_email(to_email, subject, body):
    site_email = 'lacarta@lacartadaurada.org'
    send_email(site_email, to_email, subject, body)

