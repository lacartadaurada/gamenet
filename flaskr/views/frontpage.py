from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

#from flaskr.auth import login_required
from flaskr.database import get_db
import flask_login
from ..models import Event, Comment, Bookmark
from flask import current_app as app
import dateutil

bp = Blueprint('frontpage', __name__)

import locale

locales = {'single': 'Partida', 'tournament': 'Torneo', 'league': 'Liga', 'social': 'Evento social', 'workshop': 'Taller', 'event': 'Evento', 'presentation': 'Presentación', 'proposal': 'Propuesta'}

def format_date(date, format):
    return date.astimezone(dateutil.tz.gettz("Europe/Madrid")).strftime(format)

@bp.route('/')
def index():
    app.jinja_env.globals.update(format_date=format_date)
    locale.setlocale(locale.LC_ALL, 'es_ES.UTF-8')
    events = list(Event.get_upcoming(1, 10))
    has_nextevents = len(events) > 0
    return render_template("pages/front.html", events=events, current_user_id=flask_login.current_user.id, i18n=locales, has_nextevents=has_nextevents)

@bp.route('/actividades/')
def upcoming_events():
    app.jinja_env.globals.update(format_date=format_date)
    locale.setlocale(locale.LC_ALL, 'es_ES.UTF-8')
    events = list(Event.get_upcoming(1, 10))

    event_ids = list(map(lambda s: s.id, events))
    annotations = Event.entities_annotation_count(Event.id.in_(event_ids))
    for event_id, annotation_type, count in annotations:
        event = [x for x in events if x.id == event_id][0]
    comments = Comment.count(Comment.entity_id.in_(event_ids), Comment.entity_type == 'event')

    comment_dict = {comment[0]:comment[1] for comment in comments}
    bookmark_dict = {}
    for event_id in event_ids:
        bookmark_list = Bookmark.list_related(event_id, 'event')
        bookmark_list = [x for x in bookmark_list if x.type.name in ['facebook', 'whatsapp', 'instagram']]
        bookmark_dict[event_id] = list(map(lambda s: s.to_json(), bookmark_list))
    # list doesn't allow to relink to original entity :(
    #bookmarks = Bookmark.list({'entity': {'id': event_ids, 'type': 'event'}})
    #bookmark_list = [x for x in bookmarks if x.type.name in ['facebook', 'whatsapp', 'instagram']]
    #bookmark_list = filter(lambda s: s.type.name in ['facebook', 'whatsapp', 'instagram'], bookmarks)
    #bookmark_dict = {}
    #for bookmark in bookmark_list:
    #    if not bookmark.entity_id in bookmark_dict:
    #        bookmark_dict[bookmark.entity_id] = []
    #    bookmark_dict[bookmark.entity_id].append(bookmark.to_json())
    #print(bookmark_dict)

    #events = sorted(events, key=lambda s: s.date_start)
    return render_template('pages/events.html', events=events, current_user_id=flask_login.current_user.id, comments=comment_dict, bookmarks=bookmark_dict, i18n=locales)

@bp.route('/calendario/')
def calendar_events():
    return upcoming_events()


