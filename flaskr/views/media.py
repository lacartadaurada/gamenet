from flask import Blueprint, request
from flask import redirect, render_template, send_from_directory
from flask import current_app as app
from flask_login import login_required
import flask_login
from werkzeug.exceptions import abort

import shutil
from ..models import File, Relation
from .. import db

bp = Blueprint('media', __name__)

import os
import re
import string
import random
import hashlib
import filetype


###################################
# Files

@bp.route('/')
def fetch_list():
    params = request.args.to_dict()
    include_user = False
    if ',' in params.get('category', ''):
        params['category'] = params['category'].split(',')
        # user category can only be listed by the owner
        if 'user' in params['category']:
            params['user_id'] = flask_login.current_user.id
    elif 'category' in params and params['category'] == 'user':
        params['user_id'] = flask_login.current_user.id
    files = File.search(params)
    files_data = map(lambda s: s.to_json(), files)
    return list(files_data)


@bp.route('/<image_name>', methods=['GET'])
def fetch_image(image_name):
    query = File.get_visibility_filter()
    afile = db.first_or_404(db.select(File).filter(query).filter(File.name == image_name))

    if afile.mime.startswith("image/"):
        file_path = afile.path.rsplit(".", 1)[0] + '.jpg'
    else:
        file_path = afile.path

    return send_from_directory(os.path.join(app.config['UPLOAD_DIR'], 'base'), file_path)

@bp.route('/<size>/<image_name>', methods=['GET'])
def fetch_thumb(size, image_name):
    query = File.get_visibility_filter()
    asize = int(size) # crash if not convertible
    afile = db.first_or_404(db.select(File).filter(query).filter(File.name == image_name))

    return send_from_directory(os.path.join(app.config['UPLOAD_DIR'], size), afile.thumbnail)


