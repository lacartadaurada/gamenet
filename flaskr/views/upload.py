from flask import Blueprint, request
from flask import redirect, render_template, send_from_directory
from flask import current_app as app
from flask_login import login_required

import shutil
from ..models import File, Relation
from .. import db
import pillow_avif
from PIL import Image

bp = Blueprint('upload', __name__)

import os
import re
import string
import random
import hashlib
import filetype

MAX_SIZE = 50136071 # ~ 50MB

###################################
# Files

def safe_name(filename, add_rand=True):
  safe_file_name = filename.split("/")[-1]
  safe_file_name = re.sub(r"[/\\?%*:|\"<>\x7F\x00-\x1F]", "-", safe_file_name)
  if add_rand:
      rand_part = random_string(6)
      safe_file_name = rand_part + '-' + safe_file_name
  return safe_file_name

def random_string(length):
    pool = string.ascii_letters + string.digits
    return ''.join(random.choice(pool) for i in range(length))

def file_hash(file_path):
    buf_size = 65536 # limit memory usage by reading 64kb chunks
    sha256 = hashlib.sha256()

    with open(file_path, 'rb') as f:
        # TODO: file_digest only on python 3.11!!
        #sha256 = hashlib.file_digest(f, "sha256")
        while True:
            data = f.read(buf_size)
            if not data:
                break
            sha256.update(data)

    return sha256.digest()

def save_thumbnails(filedata, safe_file_name):
  base_path = app.config['UPLOAD_DIR']

  base_thumb_path = os.path.join(base_path, 'thumb', safe_file_name)
  filedata.save(base_thumb_path)

  im = Image.open(base_thumb_path)
  rgb_im = im.convert("RGB")

  thumb_file_name = safe_file_name.rsplit(".", 1)[0] + '.jpg'
  for size in [256, 128, 64, 32]:
      im_process = rgb_im.copy()
      im_sized = im_process.resize((size, size), Image.Resampling.LANCZOS, None, 3.0)
      im_sized.save(os.path.join(base_path, str(size), thumb_file_name), quality=95)
  return os.path.join(base_path, '256', thumb_file_name)

def save_base(dest_file):
    base_name = os.path.basename(dest_file).rsplit(".", 1)[0] + '.jpg'
    dest_path = os.path.join(app.config['UPLOAD_DIR'], 'base', base_name)

    im = Image.open(dest_file).convert("RGB")

    if im.size[1] > im.size[0]:
        vsize = 800
        wpercent = (vsize / float(im.size[1]))
        hsize = int((float(im.size[0]) * float(wpercent)))
    else:
        hsize = 800
        wpercent = (hsize / float(im.size[0]))
        vsize = int((float(im.size[1]) * float(wpercent)))

    img = im.resize((hsize, vsize), Image.Resampling.LANCZOS)
    img.save(dest_path, quality=95)
    return base_name
  

def receive_data(section='generic', category=None, mime_start=None):
  # ImmutableMultiDict([('dzuuid', 'e142cece-bdce-4964-9d34-a823209f4014'), ('dzchunkindex', '10'), ('dztotalfilesize', '2136071'), ('dzchunksize', '200000'), ('dztotalchunkcount', '11'), ('dzchunkbyteoffset', '2000000')])
  filedata = request.files['file']
  thumbnail = None
  print(request.files, filedata, request.files.keys())
  if 'thumbnail' in request.files:
    thumbnail = request.files['thumbnail']
  description = request.form.get('alt')
  main_file = request.form.get('mainFile')

  # temporal mime
  mime = filedata.content_type

  base_path = app.config['UPLOAD_DIR']
  safe_file_name = safe_name(filedata.filename)
  thumb_file_name = safe_file_name
  name = filedata.filename
  if File.exists(name):
      # TODO watch out with visibility here
      name = safe_file_name

  mime = filedata.content_type
  is_image = mime.startswith("image/")
  if is_image and not main_file:
    dest_file = os.path.join(base_path, 'orig', safe_file_name)
  else:
    dest_file = os.path.join(base_path, 'base', safe_file_name)
  dest_thumb = os.path.join(base_path, '256', safe_file_name)
  dzuuid = request.form.get('dzuuid') # dropzone chunking
  saved = False
  if dzuuid:
      print("dztransfer")
      orig_file = os.path.join('/tmp/', safe_name(dzuuid, False))
      with open(orig_file, 'ab') as f:
          #f.seek(int(request.form['dzchunkbyteoffset']))
          f.write(filedata.stream.read())
      filesize = os.path.getsize(orig_file)
      if filesize > int(request.form['dztotalfilesize']) or filesize > MAX_SIZE:
          raise "too large!!"
      # last chunk
      if int(request.form['dzchunkindex']) == int(request.form['dztotalchunkcount'])-1:
          shutil.move(orig_file, dest_file)
          saved = True
  else:
      if main_file:
          dest_thumb = save_thumbnails(filedata, safe_file_name)
          thumb_file_name = os.path.basename(dest_thumb)
          saved = True
      else:
          filedata.save(dest_file)
          saved = True
          if thumbnail:
              dest_thumb = save_thumbnails(thumbnail, safe_file_name)
              thumb_file_name = os.path.basename(dest_thumb)
  if saved:
      res_file = None
      if main_file:
          res_file = File.get(int(main_file))
      else:
          # TODO: should save .jpg name?
          digest = file_hash(dest_file)
          res_file = File.find_by_digest(digest, section, category)
      if res_file:
          if main_file:
              res_file.thumbnail = thumb_file_name
              db.session.commit()
          else:
              os.unlink(dest_file)
              if thumbnail:
                  # TODO needs to delete other thumbnails too!!
                  os.unlink(dest_thumb)
      else:
          filesize = os.path.getsize(dest_file)
          kind = filetype.guess(dest_file)
          if kind is not None:
              mime = kind.mime
              if mime_start and not mime.startswith(mime_start):
                  raise "Bad mime"
              #ext = kind.extension
          if is_image:
              save_base(dest_file)
          file_pars = {'name': name, 'path': safe_file_name, 'mime': mime, 'alt': description, 'section': section, 'size': filesize, 'digest': digest}
          if category:
              file_pars['category'] = category
          if section == 'avatar':
              file_pars['visibility'] = 'public'
          if thumbnail:
              file_pars['thumbnail'] = thumb_file_name

          res_file = File.create(file_pars, update_river=(not category=='user'))
      return res_file
  return None

def serialize_for_dropzone(afile):
    res = afile.to_json()
    # needed for widget:
    res['url'] = app.config['HOST']+'/media/'+afile.name
    res['success'] = True
    return res

@bp.route('/upload/', methods=['POST'])
@login_required
def upload_file(section='generic', category=None, mime_start=None):
    res_file = receive_data(section, category, mime_start)
    if res_file:
        return serialize_for_dropzone(res_file)
    return "ok"

@bp.route('/upload/<parent_type>/<int:parent_id>', methods=['POST'])
@login_required
def upload_attachment(parent_type, parent_id, section='generic'):
    res_file = receive_data(section)
    if res_file:
        parent = Relation.get_entity(parent_type, parent_id)
        is_attached = Relation.is_related(parent, res_file)

        if not is_attached:
            Relation.create_relation(parent, res_file)
        return serialize_for_dropzone(res_file)
    return "ok"

@bp.route('/upload/image/', methods=['POST'])
@login_required
def upload_image():
    filedata = request.files['file']
    mime = filedata.content_type
    if not mime.startswith("image/"):
      print("BAD MIME", mime)
      return {'error': 'Can only upload images'}, 400

    return upload_file('document');

@bp.route('/upload/avatar/', methods=['POST'])
@login_required
def upload_avatar():
    category = request.form.get('category')

    #filedata = request.files['file']
    #print(filedata.mimetype)
    #mime = filedata.content_type
    #if not mime.startswith("image/"):
    #  print("BAD MIME", mime)
    #  return {'error': 'Can only upload images'}, 400

    return upload_file('avatar', category, "image/");
