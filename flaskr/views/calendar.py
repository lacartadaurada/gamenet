from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required

from ..notify import notify_text
from ..models import Calendar, Event, UnownedRegistration, RegistrationConfig
from .. import db

bp = Blueprint('calendar', __name__)

###################################
# Calendars
@bp.route('/', methods=['GET'])
def get_calendars():
  elements = Calendar.all()
  return list(map(lambda s: s.to_json(), elements))

@bp.route('/', methods=['POST'])
@login_required
def create_calendar():
  params = request.get_json()
  new_cal = Calendar.create(params)
  return {"result": "ok", "id": new_cal.id}

@bp.route('/<int:calid>', methods=['POST'])
@login_required
def update_calendar(calid):
  args = request.get_json()
  cal = Calendar.update(calid, args)
  return calendar_item

@bp.route('/<calid>', methods=['GET'])
def get_calendar(calid):
  return Calendar.get(calid).to_json()

###################################
# Events

# get calendar events
@bp.route('/<int:calid>/event', methods=['GET'])
def get_events(calid):
  start = request.args.get('start', None)
  end = request.args.get('end', None)
  events = Event.get_for_calendar(calid, start, end)
  events_data = map(lambda s: s.to_json(), events)
  return list(events_data)

# get event
@bp.route('/<int:calid>/event/<int:eventid>', methods=['GET'])
def get_event(calid, eventid):
  return Event.get(eventid).to_json()

# create event
@bp.route('/<int:calid>/event', methods=['POST'])
@login_required
def post_calendar_event(calid):
  params = request.get_json()
  new_event = Event.create(calid, params)
  notify_text(f"Nuevo evento: {new_event.title}")
  return new_event.to_json(), 201
  #return {"success": True, "id": new_event.id}, 201

# update event
@bp.route('/<int:calid>/event/<int:eventid>', methods=['POST'])
@login_required
def update_calendar_event(calid, eventid):
  params = request.get_json()
  edited_event = Event.update(eventid, params)
  #notify_text(f"Evento editado: {edited_event.title}")
  return edited_event.to_json()

@bp.route('/<int:calid>/event/<int:id>', methods=['DELETE'])
@login_required
def delete_event(calid, id):
    Event.remove(id, True, True)
    return {'result': 'ok'}

#########################################
# Event Registrations

# register attendance to event
@bp.route('/<int:calid>/event/<int:event_id>/register', methods=['POST'])
def register_to_calendar_event(calid, event_id):
  params = request.get_json()
  params['entity'] = {'type': 'event', 'id': event_id}

  new_registration = UnownedRegistration.create(params)

  return new_registration.to_json()

@bp.route('/<int:calid>/event/<int:event_id>/inbox', methods=['POST'])
@login_required
def list_registrations(calid, event_id):
  params = request.get_json()
  params['entity'] = {'type': 'event', 'id': event_id}

  elements = UnownedRegistration.list_related(event_id, 'event')

  return list(map(lambda s: s.to_json(), elements))

@bp.route('/<int:calid>/event/<slug>/config', methods=['GET'])
def get_registration_config(calid, slug):
  elements = RegistrationConfig.get_for_slug(slug, 'event')
  #print("list", list(elements))
  return list(map(lambda s: s.to_json(), elements))

@bp.route('/<int:calid>/event/<int:event_id>/config', methods=['POST'])
@login_required
def post_registration_config(calid, event_id):
  params = request.get_json()
  params['entity'] = {'type': 'event', 'id': event_id}

  elmt = RegistrationConfig.create_or_update(params)

  return elmt.to_json()

