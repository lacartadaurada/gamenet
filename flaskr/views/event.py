from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required

from ..notify import notify_text
from ..models import Event
from .. import db

bp = Blueprint('event', __name__)

# get calendar events
@bp.route('/<int:calid>/event', methods=['GET'])
def get_events(calid):
  start = request.args.get('start', None)
  end = request.args.get('end', None)
  events = Event.get_for_calendar(calid, start, end)
  events_data = map(lambda s: s.to_json(), events)
  return list(events_data)

# get event
@bp.route('/<int:calid>/event/<event_slug>', methods=['GET'])
def get_event(calid, event_slug):
  return Event.by_slug(event_slug).to_json()

# create event
@bp.route('/<int:calid>/event', methods=['POST'])
@login_required
def post_calendar_event(calid):
  params = request.get_json()
  new_event = Event.create(calid, params)
  notify_text(f"Nuevo evento: {new_event.title}")
  return new_event, 201
  #return {"success": True, "id": new_event.id}, 201

# update event
@bp.route('/<int:calid>/event/<int:eventid>', methods=['POST'])
@login_required
def update_calendar_event(calid, eventid):
  params = request.get_json()
  edited_event = Event.update(eventid, params)
  notify_text(f"Nuevo evento: {edited_event.title}")
  return edited_event.to_json()

