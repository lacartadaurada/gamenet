from flask import Blueprint, request

from ..models import Event
from .. import db

bp = Blueprint('ical', __name__)

# https://xkcd.com/2170/, coordinate resolution
# https://stackoverflow.com/questions/47317197/icalendar-event-in-qr-code
# https://github.com/kozakdenys/qr-code-styling
# https://github.com/qrcode-js/qrcode

def format_dtstamp(date):
    #"19970714T170000Z"
    return date.strftime("%Y%m%d%H%M%SZ")

def write_vcard(event):
    # 40.05210576181041, 0.061076507346650585
    contact_coords = ("40.052105", "0.061076")
    contact_name = "La Carta Daurada"
    contact_email = "lacartadaurada@gmail.com"
    res = "BEGIN:VEVENT\n"
    res += "UID:%s@lacartadaurada.org" % event.slug + "\n"
    res += "DTSTAMP:%s" % format_dtstamp(event.updated_at) + "\n"
    res += "ORGANIZER;CN=%s:MAILTO:%s" % (contact_name, contact_email)
    res += "DTSTART:%s" % format_dtstamp(event.date_start) + "\n"
    res += "DTEND:%s" % format_dtstamp(event.date_end) + "\n"
    res += "SUMMARY:%s" % event.title + "\n"
    res += "GEO:%s;%s" % contact_coords + "\n"
    res += "END:VEVENT" + "\n"
    return res

def write_ical(data):
    res = "BEGIN:VCALENDAR\n"
    res += "VERSION:2.0\n"
    res += "PRODID:-//hacksw/handcal//NONSGML v1.0//EN\n"
    res += data
    res += "END:VCALENDAR\n"
    return res

# get games
@bp.route('/<slug>.ical', methods=['GET'])
def get_event(slug):
  # https://lacarta/api/ical/twilight-imperium.ical
  event = Event.by_slug(slug)
  data = write_vcard(event)
  data = write_ical(data)
  return data, 200, {'Content-Type': 'text/calendar; charset=utf-8'}

# get games
@bp.route('/<date_start>/<date_end>', methods=['GET'])
def get_calendar_events(date_start, date_end):
  # date format: 2023-11-27T00:00:00+01:00
  events = Event.get_for_calendar(1, date_start, date_end)
  data = ""
  for event in events:
      data += write_vcard(event)
  data = write_ical(data)
  return data


