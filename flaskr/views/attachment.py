from flask import Blueprint, request
from flask import render_template
from flask import current_app as app
from flask_login import login_required
from sqlalchemy import func
import flask_login

from ..models import Event, Relation, File, User, Event, Game
from .. import db

bp = Blueprint('attachment', __name__)

import os
import re
import string
import random
import traceback

###################################
# Attachments

@bp.route('/<entity_type>/<int:entity_id>/', methods=['POST'])
@login_required
def post_attachment(entity_type, entity_id):
  params = request.get_json()
  file_id = params['childId']
  parent = Relation.get_entity(entity_type, entity_id)
  child = File.get(file_id)

  new_rel = Relation.create_relation(parent, child)
  return new_rel.to_json(), 201

@bp.route('/<entity_type>/<int:entity_id>/<int:id>', methods=['POST'])
@login_required
def update_attachment(entity_type, entity_id, id):
  params = request.get_json()
  new_cal = File.update(id, params)
  return new_cal.to_json()

@bp.route('/<entity_type>/<int:entity_id>/', methods=['GET'])
def get_attachments(entity_type, entity_id):
  elements = File.list_related(entity_id, entity_type)
  res = list(map(lambda s: s.to_json(), elements))
  return res

def delete_disk_files_for(afile):
    upload_dir = app.config['UPLOAD_DIR']
    paths = [os.path.join('base', afile.path),
             os.path.join('orig', afile.path),
             os.path.join('thumb', afile.path)]
    if afile.thumbnail:
        paths += [os.path.join('256', afile.thumbnail),
                  os.path.join('128', afile.thumbnail),
                  os.path.join('64', afile.thumbnail),
                  os.path.join('32', afile.thumbnail)]
    for path in paths:
        dest_file = os.path.join(upload_dir, path)
        if os.path.exists(dest_file):
            print("- delete", dest_file)
            os.unlink(dest_file)

def get_related_files(afile):
    # check files sharing path
    stmt = db.select(File).where(File.path == afile.path)
    other_files = db.session.scalar(db.select(func.count()).select_from(stmt))
    if other_files:
        return True
    # check if any objects have this as avatar.
    if afile.thumbnail:
        for cls in [User, Event, Game]:
            stmt = db.select(cls).where(cls.avatar == os.path.join('/media/256', afile.thumbnail))
            db.session.scalars(stmt);
            other_files = db.session.scalar(db.select(func.count()).select_from(stmt))
            if other_files:
                return True
    return other_files

@bp.route('/<entity_type>/<int:entity_id>/<int:id>', methods=['DELETE'])
@login_required
def delete_attachment(entity_type, entity_id, id):
    afile = File.get(id)
    child_deleted = Relation.remove(entity_id, entity_type, id, 'file', delete_child=True)
    try:
        if child_deleted:
            other_files = get_related_files(afile)
            if not other_files:
                delete_disk_files_for(afile)
            else:
                other_files = db.session.execute(query).scalars()
                for other_file in other_files:
                    print("*", other_file.path, other_file.name)
    except Exception as e:
        print("Error deleting files")
        traceback.print_exc()
    return {'result': 'ok'}
