from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required

from ..models import Game
from .. import db

bp = Blueprint('game', __name__)

# get games
@bp.route('/', methods=['GET'])
def get_games():
  events = Game.search({})
  events_data = map(lambda s: s.to_json(), events)
  return list(events_data)

# get game
@bp.route('/<slug>', methods=['GET'])
def get_game(slug):
  return Game.by_slug(slug).to_json()

# create event
@bp.route('/', methods=['POST'])
@login_required
def post_game():
  params = request.get_json()
  new_event = Game.create(params)
  return new_event.to_json(), 201
  #return {"success": True, "id": new_event.id}, 201

# update event
@bp.route('/<int:id>', methods=['POST'])
@login_required
def update_game(id):
  params = request.get_json()
  edited_event = Game.update(id, params)
  return edited_event.to_json()

@bp.route('/<int:id>', methods=['DELETE'])
@login_required
def delete_event(id):
    Game.remove(id, True, True)
    return {'result': 'ok'}

