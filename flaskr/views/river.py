from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required

from .. import db
from ..utils.fill_json import fill_users, fill_entities
from ..models import River

bp = Blueprint('river', __name__)

# get river
@bp.route('/', methods=['GET'])
def get_river():
    args = request.args
    params = River.parse_json(args)
    params = {k: v for k, v in params.items() if not v == None}

    elmts = River.search(params, limit=args.get('limit'), offset=args.get('offset'))

    elmts = list(elmts)
    elmts = list(map(lambda s: s.to_json(), elmts))
    fill_users(elmts)
    fill_entities(elmts)
    return elmts
