from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required

from ..models import Venue
from .. import db

bp = Blueprint('venue', __name__)

###################################
# Venue
@bp.route('/', methods=['GET'])
def get_venues():
  elements = Venue.all()
  return list(map(lambda s: s.to_json(), elements))

@bp.route('/', methods=['POST'])
@login_required
def create_venue():
  params = request.get_json()
  new_cal = Venue.create(params)
  return {"result": "ok", "id": new_cal.id}

@bp.route('/<int:calid>', methods=['POST'])
@login_required
def update_venue(calid):
  args = request.get_json()
  cal = Venue.update(calid, args)
  return calendar_item

@bp.route('/<calid>', methods=['GET'])
def get_venue(calid):
  return Venue.get(calid)


