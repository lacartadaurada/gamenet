from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required
import flask_login

from flask_wtf.csrf import generate_csrf

from ..models import Bookmark, Relation
from .. import db

bp = Blueprint('bookmark', __name__)

import os
import re
import string
import random

###################################
# Bookmarks

@bp.route('/', methods=['POST'])
@login_required
def post_bookmark():
  params = request.get_json()
  new_cal = Bookmark.create(params)
  return new_cal.to_json(), 201

@bp.route('/<int:id>', methods=['POST'])
@login_required
def update_bookmark(id):
  params = request.get_json()
  new_cal = Bookmark.update(id, params)
  return new_cal.to_json()

@bp.route('/', methods=['GET'])
def get_bookmarks():
  #csrf = generate_csrf()
  #print(csrf)
  entity_id = request.args.get('id', None)
  entity_type = request.args.get('type', None)

  elements = Bookmark.list_related(entity_id, entity_type)
  return list(map(lambda s: s.to_json(), elements))

@bp.route('/<int:id>', methods=['DELETE'])
@login_required
def delete_bookmark(id):
    Bookmark.remove(id, True)
    return {'result': 'ok'}
