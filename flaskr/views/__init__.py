from .upload import bp as upload_bp
from .comment import bp as comment_bp
from .calendar import bp as calendar_bp
from .event import bp as event_bp
from .venue import bp as venue_bp
from .bookmark import bp as bookmark_bp
from .attachment import bp as attachment_bp
from .frontpage import bp as frontpage_bp
from .user import bp as user_bp
from .media import bp as media_bp
from .game import bp as game_bp
from .annotation import bp as annotation_bp
from .ical import bp as ical_bp
from .site_message import bp as site_message_bp
from .accounting import bp as accounting_bp
from .group import bp as group_bp
from .river import bp as river_bp

def register_blueprints(app):
    app.register_blueprint(frontpage_bp, url_prefix='/')
    app.register_blueprint(upload_bp, url_prefix='/')
    app.register_blueprint(comment_bp, url_prefix='/comment')
    app.register_blueprint(calendar_bp, url_prefix='/calendar')
    app.register_blueprint(event_bp, url_prefix='/calendar')
    app.register_blueprint(venue_bp, url_prefix='/venue')
    app.register_blueprint(bookmark_bp, url_prefix='/bookmark')
    app.register_blueprint(attachment_bp, url_prefix='/attachment')
    app.register_blueprint(user_bp, url_prefix='/user')
    app.register_blueprint(media_bp, url_prefix='/media')
    app.register_blueprint(game_bp, url_prefix='/game')
    app.register_blueprint(annotation_bp, url_prefix='/annotation')
    app.register_blueprint(ical_bp, url_prefix='/ical')
    app.register_blueprint(site_message_bp, url_prefix='/site/message')
    app.register_blueprint(accounting_bp, url_prefix='/accounting')
    app.register_blueprint(group_bp, url_prefix='/group')
    app.register_blueprint(river_bp, url_prefix='/river')

