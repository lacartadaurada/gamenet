from flask import Blueprint, request
from flask_login import login_required
import flask_login

from ..models import Annotation, EntityLink, User
from ..models import mapping as class_mapping
from ..utils.fill_json import fill_users
from .. import db

from collections import defaultdict

bp = Blueprint('annotation', __name__)

###################################
# Annotations

@bp.route('/<entity_type>/<int:entity_id>/<annotation_type>', methods=['GET'])
@login_required
def get_user_annotations(entity_type, entity_id=None, annotation_type=None):
  params = {'entity_type': entity_type}
  params['annotation_type'] = annotation_type
  params['entity_id'] = entity_id
  params['user_id'] = flask_login.current_user.id

  elements = Annotation.search(params)
  elements = list(map(lambda s: s.to_json(), elements))
  res = fill_users(elements)

  return res

@bp.route('/<entity_type>/<int:entity_id>/', methods=['GET'])
def get_user_entity_annotations(entity_type, entity_id):
  return get_user_annotations(entity_type, entity_id)

def fill_entities(elements):
  res = []
  pertype = defaultdict(list)
  for element in elements:
      entity = element['entity']
      pertype[entity['type']].append(entity['id'])

  entities = {}
  for entity_type in pertype.keys():
      cls = class_mapping[entity_type]
      elmts = db.session.scalars(db.select(cls).filter(cls.id.in_(pertype[entity_type])))
      for elmt in elmts:
          key = entity_type+str(elmt.id)
          entities[key] = elmt.to_avatar_json()
          entities[key]['type'] = entity_type
  for element in elements:
      element['entity'] = entities[element['entity']['type']+str(element['entity']['id'])]

  return elements

@bp.route('/search/', methods=['POST'])
def search_annotations():
  args = request.get_json()
  params = {}
  params['entity_id'] = args.get('entity', {}).get('id')
  params['entity_type'] = args.get('entity', {}).get('type')
  params['user_id'] = args.get('userId')
  params['annotation_type'] = args.get('type')

  elements = Annotation.search(params)
  elements = list(map(lambda s: s.to_json(), elements))
  res = fill_entities(elements)
  res = fill_users(res)

  return res

@bp.route('/<entity_type>/<int:entity_id>/<annotation_type>', methods=['POST'])
@login_required
def post_annotation(entity_type, entity_id, annotation_type):
  if Annotation.has_annotation(entity_type, entity_id, annotation_type, flask_login.current_user.id):
      raise "Already annotated"
  res = Annotation.create(entity_type=entity_type, entity_id=entity_id, annotation_type=annotation_type)
  return res.to_json(), 201

@bp.route('/<entity_type>/<int:entity_id>/<annotation_type>', methods=['DELETE'])
@login_required
def delete_annotation(entity_type, entity_id, annotation_type):
    Annotation.remove(entity_type, entity_id, annotation_type)
    return {'result': 'ok'}
