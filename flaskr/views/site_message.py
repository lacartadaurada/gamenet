from flask import Blueprint, request

from ..models import UnownedMessage
from ..notify import notify_text

bp = Blueprint('site_message', __name__)

###################################
# Site messages

@bp.route('/', methods=['POST'])
def post_message():
  params = request.get_json()
  params['entity'] = {'id': 1, 'type': 'group'}
  new_elmt = UnownedMessage.create(params)
  notify_text("New site message arrived!")
  return new_elmt.to_json(), 201

@bp.route('/', methods=['GET'])
def get_messages():
  entity_id = request.args.get('id', None)
  entity_type = request.args.get('type', None)
  elements = Comment.list_related(entity_id, entity_type)
  return list(map(lambda s: s.to_json(), elements))


