from flask import Blueprint, request
from flask_login import login_required
import flask_login

from ..models import AccountingRecord
from .. import db

bp = Blueprint('accounting', __name__)

# get accounting_records
@bp.route('/<int:group_id>/', methods=['GET'])
def get_accounting_records(group_id):
    if not flask_login.current_user.member_of(group_id):
        return {}, 401

    elements = AccountingRecord.search(AccountingRecord.group_id == group_id)
    elements_data = map(lambda s: s.to_json(), elements)
    return list(elements_data)

# get accounting_record
@bp.route('/<int:group_id>/<id>', methods=['GET'])
def get_accounting_record(group_id, id):
    if not flask_login.current_user.member_of(group_id):
        return {}, 401

    return AccountingRecord.get(id).to_json()

# create accounting_record
@bp.route('/<int:group_id>/', methods=['POST'])
@login_required
def post_accounting_record(group_id):
    if not flask_login.current_user.member_of(group_id):
        return {}, 401

    args = request.get_json()
    new_accounting_record = AccountingRecord.create(group_id, args)
    return new_accounting_record.to_json(), 201

# update accounting_record
@bp.route('/<int:group_id>/<int:id>', methods=['POST'])
@login_required
def update_accounting_record(group_id, id):
    if not flask_login.current_user.member_of(group_id):
        return {}, 401

    args = request.get_json()
    edited = AccountingRecord.update(id, args)
    return edited.to_json()

@bp.route('/<int:group_id>/<int:id>', methods=['DELETE'])
@login_required
def delete_accounting_record(group_id, id):
    if not flask_login.current_user.member_of(group_id):
        return {}, 401

    AccountingRecord.update(id, {'type': 'deleted'})
    return {'result': 'ok'}

