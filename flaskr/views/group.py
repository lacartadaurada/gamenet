from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required, current_user

from ..models import Group, GroupMember, User
from .. import db

bp = Blueprint('group', __name__)

# get groups
@bp.route('/', methods=['GET'])
def get_groups():
    #args = request.get_json()
    args = request.args
    params = Group.parse_json(args)
    if args.get('userId'):
        user_id = args.get('userId')
        groups = Group.groups_for_user(user_id)
        groups_data = map(lambda s: s.to_avatar_json(), groups)
    else:
        groups = Group.search(params)
        groups_data = map(lambda s: s.to_json(), groups)
    return list(groups_data)

# get group
@bp.route('/<slug>', methods=['GET'])
def get_group(slug):
    return Group.by_slug(slug).to_json()

# create group
@bp.route('/', methods=['POST'])
@login_required
def post_group():
    args = request.get_json()
    new_group = Group.create(args)
    return new_group.to_json(), 201

# update group
@bp.route('/<int:id>', methods=['POST'])
@login_required
def update_group(id):
    args = request.get_json()
    edited_group = Group.update(id, args)
    return edited_group.to_json()

@bp.route('/<int:id>', methods=['DELETE'])
@login_required
def delete_group(id):
    Group.remove(id, True, True)
    return {'result': 'ok'}

#########
# Group Members

# get group members
@bp.route('/<int:id>/members/', methods=['GET'])
def get_group_members(id):
    if current_user.id == 0:
        return []
    args = request.args
    users = db.session.scalars(db.select(User).join(GroupMember, GroupMember.user_id==User.id).join(Group, Group.id == GroupMember.group_id).where(Group.id == id))
    users_data = map(lambda s: s.to_avatar_json(), users)
    return list(users_data)


@bp.route('/<int:id>/members/', methods=['POST'])
@login_required
def add_group_member(id):
    args = request.get_json()
    role = args.get('permissions', 'member')
    user_id = request['user_id']
    params =  {'user_id': user_id,
               'group_id': id,
               'permissions': role}
    group_member = GroupMember.search(GroupMember.user_id == user_id, GroupMember.group_id == id).first()
    if not group_member:
        new_membership = GroupMember.create({}, params)
    return {'result': 'ok'}

# update group member
@bp.route('/<int:id>/members/<int:user_id>', methods=['POST'])
@login_required
def update_group_member(id, user_id):
    args = request.get_json()
    group_member = GroupMember.search(GroupMember.user_id == user_id, GroupMember.group_id == id).first()
    if args.get('permissions'):
        group_member.permissions = args['permissions']
        db.session.commit()
    return {'result': 'ok'}

# delete group member
@bp.route('/<int:id>/members/<int:user_id>', methods=['DELETE'])
@login_required
def delete_group_member(id, user_id):
    args = request.get_json()
    group_member = GroupMember.search(GroupMember.user_id == user_id, GroupMember.group_id == id).first()
    if group_member:
        GroupMember.remove(group_member.id)
    return {'result': 'ok'}

