from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required

from ..models import Comment, Relation

bp = Blueprint('comment', __name__)

import os
import re
import string
import random

###################################
# Calendars

@bp.route('/', methods=['POST'])
@login_required
def post_comment():
  params = request.get_json()
  new_cal = Comment.create(params)
  return new_cal.to_json(), 201

@bp.route('/', methods=['GET'])
def get_comments():
  entity_id = request.args.get('id', None)
  entity_type = request.args.get('type', None)
  elements = Comment.list_related(entity_id, entity_type)
  return list(map(lambda s: s.to_json(), elements))

@bp.route('/count', methods=['GET'])
def get_comments_count():
  entity_id = request.args.get('id', None)
  entity_type = request.args.get('type', None)
  elements = Comment.count(Comment.entity_id == entity_id, Comment.entity_type == entity_type)
  return elements

@bp.route('/<int:id>', methods=['DELETE'])
@login_required
def delete_comment(id):
    Comment.remove(id)
    return {'result': 'ok'}  
