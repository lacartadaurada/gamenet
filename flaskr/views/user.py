from flask import Blueprint, request
from flask import redirect, render_template
from flask import current_app as app
from flask_login import login_required
import flask_login

from werkzeug.exceptions import abort

from ..models import User
from .. import db

bp = Blueprint('user', __name__)

@bp.route('/', methods=['GET'])
@login_required
def get_current_user():
  return flask_login.current_user.to_json()

@bp.route('/<int:id>', methods=['GET'])
def get_user(id):
  user = User.get(id)
  return user.to_json(True)

@bp.route('/', methods=['POST'])
@login_required
def update_user():
  args = request.get_json()
  user = User.update(flask_login.current_user.id, args)
  return user.to_json()

