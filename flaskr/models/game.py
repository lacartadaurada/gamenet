from sqlalchemy import func
import flask_login

from .. import db

from .base import Model
from .mixin.entity import Entity
from .mixin.slug import Slug

class Game(Entity, Slug, Model):
    __tablename__ = 'games'

    avatar = db.Column(db.String(125))
    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text)
    max_players = db.Column(db.Integer, default=0)
    min_players = db.Column(db.Integer, default=0)
    max_best_players = db.Column(db.Integer, default=0)
    min_best_players = db.Column(db.Integer, default=0)
    max_duration = db.Column(db.Integer, default=0)
    min_duration = db.Column(db.Integer, default=0)
    difficulty = db.Column(db.DECIMAL(4,2), default=0)
    rating = db.Column(db.DECIMAL(4,2), default=0)
    release_date = db.Column(db.DateTime(timezone=True),
                                   server_default=func.now())

    allowed_attributes = Entity.allowed_attributes+['avatar', 'title', 'description', 'min_duration', 'max_duration', 'difficulty', 'rating',
                                                    'max_players', 'min_players', 'release_date', 'date_end', 'max_best_players', 'min_best_players']

    def to_json(self):
        data = self.dump_attributes('title', 'description', 'rating', 'difficulty', 'avatar',
                                    releaseDate=self.isoformat(self.release_date),
                                    players={'min': self.min_players, 'max': self.max_players},
                                    best={'min': self.min_best_players, 'max': self.max_best_players},
                                    duration={'min': self.min_duration, 'max': self.max_duration})
        data.update(Model.to_json(self))
        data.update(Slug.to_json(self))
        data.update(Entity.to_json(self))
        return data

    @classmethod
    def parse_json(cls, args, group_id=None, default_visibility='members', default_permissions='user'):
        release_date = cls.parse_date(args, 'releaseDate')
        params = cls.fetch_attributes(args, 'title', 'difficulty', 'rating', 'avatar', release_date=release_date)
        params['description'] = cls.sanitized(args, 'description')

        params.update(Entity.parse_json(args, group_id, default_visibility, default_permissions))

        players = args.get('players')
        if players:
            params['min_players'] = players.get('min')
            params['max_players'] = players.get('max')
        best = args.get('best')
        if players:
            params['min_best_players'] = players.get('min')
            params['max_best_players'] = players.get('max')
        duration = args.get('duration')
        if duration:
            params['min_duration'] = duration.get('min')
            params['max_duration'] = duration.get('max')
        return params

    @classmethod
    def create(cls, args):
        params = {}
        params['slug'] = cls.generate_slug(args['title'])

        default_group_id = 1
        params.update(cls.parse_json(args, default_group_id))

        return super(cls, cls).create(args, params)

