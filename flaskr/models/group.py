from sqlalchemy import ForeignKey

from .. import db
from .base import Model
from .mixin.permissions import Permissions
from .mixin.visibility import Visibility
from .mixin.date_mixin import DateMixin
from .mixin.slug import Slug
from .mixin.entity_interface import EntityInterface
from .group_member import GroupMember

class Group(EntityInterface, Slug, Permissions, Visibility, DateMixin, Model):
    __tablename__ = 'groups'
    title = db.Column(db.String(100), nullable=False)
    user_id = db.Column(db.Integer, ForeignKey("users.id"), nullable=False, server_default="1") # need default for migrations
    avatar = db.Column(db.String(125))
    description = db.Column(db.Text())
    parent_group_id = db.Column(db.Integer, ForeignKey("groups.id"), nullable=True)

    allowed_attributes = ['visibility', 'permissions', 'title', 'avatar']

    def __repr__(self):
        return f'<Group {self.title}>'

    def is_member(self, user):
        return not self.session.scalars(db.select(GroupMember).filter(GroupMember.user_id == user.id, GroupMember.group_id == self.id)).first() == None

    def to_avatar_json(self):
        return {'avatar': self.avatar, 'id': self.id, 'name': self.title, 'slug': self.slug, 'subtype': 'group'}

    @classmethod
    def groups_for_user(cls, user_id):
        groups = db.session.scalars(db.select(Group).join(GroupMember, GroupMember.group_id == Group.id).where(user_id == GroupMember.user_id))
        return groups

    @classmethod
    def create(cls, args):
        params = cls.parse_json(args)
        params['slug'] = cls.generate_slug(args['title'])

        return super(cls, cls).create(args, params)

    @classmethod
    def parse_json(cls, args, default_visibility='members', default_permissions='user'):
        params = cls.fetch_attributes(args, 'title', 'avatar', user_id=args.get('userId'))

        params['description'] = cls.sanitized(args, 'description')
        params.update(EntityInterface.parse_json(args, None, default_visibility, default_permissions))
        return params

    def to_json(self):
        data = self.dump_attributes('title', 'avatar', userId=self.user_id)

        data.update(Model.to_json(self))
        data.update(Visibility.to_json(self))
        data.update(DateMixin.to_json(self))
        data.update(Permissions.to_json(self))
        data.update(Slug.to_json(self))
        return data
