from sqlalchemy import Enum, func

from .. import db
from .entity_link import EntityLink
from .enums import EntityTypeEnum
from . import mapping

class Relation(EntityLink):
    __tablename__ = 'relations'
    child_entity_id = db.Column(db.Integer, nullable=False)
    child_entity_type = db.Column(Enum(EntityTypeEnum), nullable=False)

    allowed_attributes = ['entity_id', 'entity_type', 'child_entity_id', 'child_entity_type']

    def get_child_entity(self):
        return self.__class__.get_entity(self.child_entity_id, self.child_entity_type)

    def get_parent_entity(self):
        return self.__class__.get_entity(self.entity_id, self.entity_type)

    @classmethod
    def get_entity_type(cls, entity):
        return entity.__class__.__name__.lower()

    @classmethod
    def create_relation(cls, parent_entity, child_entity):
        params = {'entity_id': parent_entity.id,
                  'entity_type': cls.get_entity_type(parent_entity),
                  'child_entity_id': child_entity.id,
                  'child_entity_type': cls.get_entity_type(child_entity)}
        new_relation = cls(**params)

        db.session.add(new_relation)
        db.session.commit()
        return new_relation

    @classmethod
    def parse_json(cls, args):
        params = EntityLink.parse_json(args)
        params.update({'child_entity_id': args['child']['id'],
                       'child_entity_type': args['child']['type']})
        return params

    def to_json(self):
        data = EntityLink.to_json(self)
        data.update({'child': {'type': self.child_entity_type.name, 'id': self.child_entity_id}})
        return data

    @classmethod
    def is_related(cls, parent_entity, child_entity):
        from . import class_map
        query = db.select(Relation.id).where(Relation.entity_id == parent_entity.id,
                                             Relation.entity_type == class_map(parent_entity.__class__),
                                             Relation.child_entity_type == class_map(child_entity.__class__),
                                             Relation.child_entity_id == child_entity.id)
        other_relations = db.session.scalar(db.select(func.count()).select_from(query))
        print('relations', other_relations, class_map(parent_entity.__class__), class_map(child_entity.__class__), parent_entity.id, child_entity.id)
        return other_relations > 0

    @classmethod
    def get_linked(cls, child_entity_type, child_entity_id):
        rel = cls.search(Relation.child_entity_type == child_entity_type, Relation.child_entity_id == child_entity_id).first()
        if rel:
            return rel.entity
        else:
            return None

    @classmethod
    def list_related(cls, child_entity_type, entity_id, entity_type):
        query = db.select(Relation.child_entity_id).where(Relation.entity_type == entity_type, Relation.child_entity_type == child_entity_type)
        if isinstance(entity_id, list):
            query = query.where(Relation.entity_id.in_(entity_id))
        else:
            query = query.where(Relation.entity_id == entity_id)
        ids = db.session.execute(query).scalars()

        child_class = mapping[child_entity_type]
        query = db.select(child_class).where(child_class.id.in_(ids))

        return db.session.execute(query).scalars()

    @classmethod
    def remove(cls, entity_id, entity_type, child_id, child_type, delete_child):
        from . import mapping
        # TODO: ensure parents/children are editable first?
        stmt = db.delete(Relation).where(Relation.entity_id == entity_id,
                                         Relation.entity_type == entity_type,
                                         Relation.child_entity_type == child_type,
                                         Relation.child_entity_id == child_id)
        db.session.execute(stmt)

        child_deleted = False
        if delete_child:
            query = db.select(Relation).where(Relation.child_entity_type == child_type,
                                              Relation.child_entity_id == child_id)

            other_relations = db.session.scalar(db.select(func.count()).select_from(query))
            print("other relations", other_relations)
            if not other_relations:
                child_cls = mapping[child_type]
                
                child_cls.remove(child_id, commit=False)
                #stmt = db.delete(child_cls).where(child_cls.id == child_id)
                #db.session.execute(stmt)
                child_deleted = True

        db.session.commit()
        return child_deleted
