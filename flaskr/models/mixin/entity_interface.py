from sqlalchemy import ForeignKey, func
from sqlalchemy.orm import declarative_mixin
from flask import abort

from ... import db
from .visibility import Visibility
from .date_mixin import DateMixin
from .owned import Owned
from .permissions import Permissions
from ..annotations import Annotation
from ..user import User

@declarative_mixin
class EntityInterface():
    def to_avatar_json(self):
        from .. import class_map
        return {'avatar': self.avatar, 'id': self.id, 'title': self.title, 'slug': self.slug, 'subtype': class_map(self.__class__)}

    def get_linked(self, relation_name=None):
        from ..relation import Relation
        from .. import class_map
        if not relation_name:
            relation_name = class_map(self.__class__)
        return Relation.get_linked(relation_name, self.id)

    @classmethod
    def get(cls, id):
        #event = db.get_or_404(cls, id)
        query = (cls.id == id) & cls.get_visibility_filter()
        event = db.one_or_404(db.select(cls).filter(query))
        return event

    @classmethod
    def parse_json(cls, args, group_id=None, default_visibility='members', default_permissions='user'):
        params = Visibility.parse_json(args, default_visibility)
        params.update(DateMixin.parse_json(args))
        params.update(Permissions.parse_json(args, default_permissions))
        if group_id:
            params.update(Owned.parse_json(args, group_id))
        return params

    @classmethod
    def update(cls, id, args, group_id=None):
        from .. import River
        params = cls.parse_json(args, group_id, None, None)

        elmt = db.get_or_404(cls, id)
        if not elmt.is_editable():
            raise abort(403, {"error": "no permissions"})
        elmt.assign(params)
        River.add_action('update', elmt, False)

        db.session.commit()
        return elmt

    @classmethod
    def remove(cls, id, delete_relations=False, delete_annotations=False):
        from .. import class_map
        from ..relation import Relation
        super(cls, cls).base_remove(id, False)

        # NOTE deleting from all parents..
        if delete_relations:
            class_type = class_map(cls)
            stmt = db.delete(Relation).where(Relation.child_entity_id == id, Relation.child_entity_type == class_type)
            res = db.session.execute(stmt)

        if delete_annotations:
            class_type = class_map(cls)
            stmt = db.delete(Annotation).where(Annotation.entity_id == id, Annotation.entity_type == class_type)
            res = db.session.execute(stmt)

        db.session.commit()
        return {'result': 'ok'}

    @classmethod
    def entities_annotation_count(cls, *extra_queries):
        # total annotations for an entity or a group of entities
        from .. import class_map
        cls_type = class_map(cls)
        stmt = db.select(cls.id, Annotation.annotation_type, func.count(Annotation.annotation_type).label("count"))
        stmt = stmt.select_from(Annotation).join(cls, (cls.id == Annotation.entity_id) & (Annotation.entity_type == cls_type))
        if extra_queries:
            stmt = stmt.filter(*extra_queries)
        stmt = stmt.group_by(Annotation.annotation_type)
        res = list(db.session.execute(stmt).all())
        # [(42, <AnnotationTypeEnum.going: 3>, 1)]
        # [(entity_id, annotation_type, count), ... ]
        return res

    @classmethod
    def entity_annotation_count(cls, entity_id, *extra_queries):
        return self.entities_annotation_count(cls.id == entity_id, *extra_queries)[-1][2]

    """
    @classmethod
    def entity_annotations(cls, *extra_queries):
        from .. import class_map
        cls_type = class_map(cls)
        # list annotations by user and entity type
        # TODO: needs permissions if not current user
        stmt = db.select(Annotation.annotation_type, Annotation.entity_id)
        stmt = stmt.select_from(Annotation).join(User, User.id == Annotation.user_id).join(cls, (cls.id == Annotation.entity_id) & (Annotation.entity_type == cls_type))
        # there's no extra_queries or last element of extra_queries is False we
        # don't apply visibility filters
        if not extra_queries or (extra_queries and extra_queries[-1] is not False):
            extra_queries.append(cls.get_visibility_filter())
        if extra_queries and extra_queries[-1] is False:
            extra_queries = extra_queries[:-1]
        if extra_queries:
            stmt = stmt.filter(*extra_queries)
        res = list(db.session.execute(stmt).all())
        # [(<AnnotationTypeEnum.like: 0>, 67), (<AnnotationTypeEnum.have: 1>, 67) ... ]
        # [(annotation_type, entity_id), ]
        return res

    @classmethod
    def user_entity_annotations(cls, user_id, *extra_queries):
        return cls.entity_annotations(User.id == user_id, *extra_queries)
    """
