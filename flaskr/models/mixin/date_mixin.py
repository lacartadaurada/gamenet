from sqlalchemy.orm import declarative_mixin, declared_attr

from datetime import datetime, timezone

from ... import db

@declarative_mixin
class DateMixin:
    created_at = db.Column(db.DateTime, default=datetime.now)
    updated_at = db.Column(db.DateTime, default=datetime.now)

    def isoformat(self, date):
        return date.astimezone(timezone.utc).isoformat(timespec="milliseconds").replace("+00:00", "Z")

    @classmethod
    def set_updated_at(cls, params):
        params['updated_at'] = datetime.now()

    @classmethod
    def parse_json(cls, args):
        return {}

    def to_json(self):
        data = {'created': self.isoformat(self.created_at),
                'updated': self.isoformat(self.updated_at)}
        return data
