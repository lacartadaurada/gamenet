from sqlalchemy import Enum, or_
from sqlalchemy.orm import declarative_mixin, declared_attr
from sqlalchemy.sql.expression import true
import flask_login

from ..enums import VisibilityEnum
from ..group_member import GroupMember
from ... import db

@declarative_mixin
class Visibility:
    visibility = db.Column(Enum(VisibilityEnum), server_default='public')

    def to_json(self):
        return {'visibility': self.visibility.name}

    @classmethod
    def parse_json(cls, args, default='members'):
        params = {'visibility': args.get('visibility', default)}
        return params

    @classmethod
    def get_visibility_filter(cls):
        from ..group import Group
        current_user = flask_login.current_user
        if current_user and current_user.admin:
            return true()
        # public
        visibility_filters = (cls.visibility == 'public',)
        if current_user and current_user.id > 0:
            # private
            private_filter = cls.user_id == current_user.id
            # members
            visibility_filters += (cls.visibility == 'members', private_filter,)
            # groups
            groups = db.session.execute(db.select(GroupMember.id).filter(GroupMember.user_id == current_user.id)).scalars()
            if groups and not cls == Group:
                visibility_filters += (cls.group_id.in_(groups) & (cls.visibility == 'group'),)
        if len(visibility_filters) > 1:
            visibility_filter = or_(*visibility_filters)
        else:
            visibility_filter = visibility_filters[0]
        return visibility_filter & (cls.visibility != None)


