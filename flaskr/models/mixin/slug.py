from sqlalchemy.orm import declarative_mixin

from slugify import slugify
from ... import db



@declarative_mixin
class Slug:
    slug = db.Column(db.String(100), nullable=False)

    def to_json(self):
        return {'slug': self.slug}

    @classmethod
    def generate_slug(cls, name, prev_id=None, raw=False):
        found = False
        base_slug = slugify(name)
        if raw:
            return base_slug
        slug = base_slug
        append = 0
        while append < 50:
            if append:
                slug = base_slug + '-' + str(append)
            query = db.select(cls).where(cls.slug == slug)
            if prev_id:
                query = query.where(cls.id != prev_id)
            elmt = db.session.execute(query).scalar()
            if elmt == None:
                return slug
            append += 1
        raise "Can't find slug"

    @classmethod
    def by_slug(cls, slug):
        query = (cls.slug == slug) & cls.get_visibility_filter()
        elmt = db.one_or_404(db.select(cls).filter(query))
        return elmt


