from sqlalchemy import ForeignKey
from sqlalchemy.orm import declarative_mixin, declared_attr
import flask_login

from ... import db

@declarative_mixin
class Owned:
    @declared_attr
    def user_id(cls):
        return db.Column(db.Integer, ForeignKey("users.id"), nullable=False)
    @declared_attr
    def group_id(cls):
        return db.Column(db.Integer, ForeignKey("groups.id"), nullable=False)

    def to_json(self):
        return {'userId': self.user_id, 'groupId': self.group_id}

    @classmethod
    def parse_json(cls, args, group_id):
        args = {'user_id': flask_login.current_user.id,
                'group_id': group_id}
        return args

