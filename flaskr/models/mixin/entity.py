from sqlalchemy import ForeignKey, func
from sqlalchemy.orm import declarative_mixin, declared_attr
from flask import abort

from ... import db
from .visibility import Visibility
from .date_mixin import DateMixin
from .owned import Owned
from .permissions import Permissions
from ..annotations import Annotation
from ..user import User
from ..base import Model
from .entity_interface import EntityInterface

@declarative_mixin
class Entity(EntityInterface, Visibility, DateMixin, Owned, Permissions):
    allowed_attributes = ['visibility', 'permissions']

    def to_json(self):
        data = Visibility.to_json(self)
        data.update(DateMixin.to_json(self))
        data.update(Owned.to_json(self))
        data.update(Permissions.to_json(self))
        user = User.get(self.user_id)
        data['user'] = user.to_json(True)
        return data


