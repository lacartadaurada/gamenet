from sqlalchemy import Enum
from sqlalchemy.orm import declarative_mixin

import flask_login

from ..enums import PermissionEnum
from ..group_member import GroupMember
from ... import db

@declarative_mixin
class Permissions:
    permissions = db.Column(Enum(PermissionEnum), server_default='user')

    def to_json(self):
        user = flask_login.current_user
        return {'permissions': self.permissions.name, 'editable': user.can_edit(self)}

    def is_editable(self):
        return self.editable_by(flask_login.current_user)

    def is_owned(self):
        return self.user_id == flask_login.current_user.id

    def editable_by(self, user):
        if user.admin:
            return True
        if self.permissions == PermissionEnum.user or self.user_id == user.id:
            return self.user_id == user.id
        elif self.permissions == PermissionEnum.public:
            return True
        elif self.permissions == PermissionEnum.group:
            if user.id == 0:
                return False
            query = db.select(GroupMember.group_id).filter((GroupMember.group_id == self.group_id) & (GroupMember.user_id == user.id))
            return db.session.execute(query).first() is not None
        elif self.permissions == PermissionEnum.members:
            # for now user needs to be logged in
            return user.id > 0

    @classmethod
    def parse_json(cls, args, default='user'):
        return {'permissions': args.get('permissions', default)}

