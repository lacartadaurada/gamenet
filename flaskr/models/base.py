from .. import db
from datetime import datetime, timezone
from dateutil.parser import isoparse
from ..utils import sanitizer

from sqlalchemy.sql.expression import true

import inspect

class Model(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    allowed_attributes = []

    @classmethod
    def sanitized(cls, args, attr):
        res = args.get(attr)
        if res:
            return sanitizer.sanitize(res)
        return res

    def dump_attributes(self, *attribute_list, **kwargs):
        data = {}
        for attribute in attribute_list:
            data[attribute] = getattr(self, attribute)
        for key, value in kwargs.items():
            data[key] = value
        return data

    @classmethod
    def fetch_attributes(cls, args, *attribute_list, **kwargs):
        params = {}
        for attribute in attribute_list:
            params[attribute] = args.get(attribute)
        for key, value in kwargs.items():
            params[key] = value
        return params

    @classmethod
    def parse_extra_queries(cls, extra_queries):
        params = {}
        if extra_queries and isinstance(extra_queries[0], dict):
            params = extra_queries[0]
            extra_queries = None
        return params, extra_queries

    @classmethod
    def search(cls, *extra_queries, **kwargs):
        params, extra_queries = cls.parse_extra_queries(extra_queries)

        if hasattr(cls, 'get_visibility_filter'):
            query = cls.get_visibility_filter()
        else:
            query = true()
        for par_name, value in params.items():
            if isinstance(value, list):
                query = query & (getattr(cls, par_name).in_(value))
            else:
                query = query & (getattr(cls, par_name) == value)
        stmt = db.select(cls).filter(query)
        if extra_queries:
            stmt = stmt.filter(*extra_queries)
        stmt = stmt.order_by(cls.id.desc())
        if 'offset' in kwargs:
            stmt = stmt.offset(kwargs['offset'])
        if 'limit' in kwargs:
            stmt = stmt.fetch(kwargs['limit'])
        return db.session.execute(stmt).scalars()

    def assign(self, attributes, allowed=None):
        from .river import River
        print("assign", attributes)
        update_river = False
        if 'visibility' in attributes and not attributes['visibility'] == self.visibility:
            update_river = attributes['visibility']
        if hasattr(self, 'updated_at'):
            self.__class__.set_updated_at(attributes)
        attributes = {k: v for k, v in attributes.items() if v is not None}
        if allowed:
            attributes = {k: v for k, v in attributes.items() if k in allowed}
        elif self.allowed_attributes:
            attributes = {k: v for k, v in attributes.items() if k in self.allowed_attributes}
        for k,v in attributes.items():
            setattr(self, k, v)
        if update_river:
            River.update_visibility(self, update_river)

    def to_json(self):
        return {'id': self.id}

    def save(self, commit=True):
        # Save this model to the database.
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def related(self, child_type):
        from . import class_map
        from .relation import Relation
        my_type = class_map(self.__class__)
        return Relation.get_related(child_type, self.id, my_type)

    @classmethod
    def get(cls, id):
        event = db.get_or_404(cls, id)
        return event

    @classmethod
    def create(cls, args, params={}, commit=True, update_river=True):
        from .river import River
        #params.update(cls.parse_json(args))
        if 'updated_at' in params:
            del params['updated_at']
        new_comment = cls(**params)

        db.session.add(new_comment)
        if commit:
            db.session.commit()
        if update_river:
            River.add_action('create', new_comment, True)
        return new_comment

    @classmethod
    def update(cls, id, args, commit=True):
        from .river import River
        params = cls.parse_json(args)

        elmt = db.get_or_404(cls, id)

        elmt.assign(params)
        River.add_action('update', elmt, False)
        if commit:
            db.session.commit()
        return elmt

    @classmethod
    def all(cls, *extra_queries):
        stmt = db.select(cls)
        if extra_queries:
            stmt = stmt.filter(*extra_queries)
        res = db.session.execute(stmt).scalars()
        return res

    @classmethod
    def base_remove(cls, id, commit=True):
        query = db.select(cls).where(cls.id == id)
        elmt = db.session.execute(query).scalar()

        if not elmt.is_editable():
            return 'nok', 403
        if not elmt.is_owned():
            return 'nok', 403

        stmt = db.delete(cls).where(cls.id == id)
        db.session.execute(stmt)
        if commit:
            db.session.commit()
 
    @classmethod
    def remove(cls, id, commit=True):
        cls.base_remove(id, commit)

    @classmethod
    def parse_date(cls, args, key):
        if key in args and args[key]:
            return isoparse(args[key])
        return None


