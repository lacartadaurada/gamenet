from .. import db

import inspect

class TelegramSubscription(db.Model):
    __tablename__ = 'telegram_subscriptions'
    id = db.Column(db.Integer, primary_key=True)
    app_id = db.Column(db.String(128))

    @classmethod
    def is_subscribed(cls, app_id):
        return not db.session.scalar(db.select(cls).filter(cls.app_id == str(app_id))) == None

    @classmethod
    def create(cls, app_id, commit=True):
        new_elmt = cls(app_id=str(app_id))

        db.session.add(new_elmt)
        if commit:
            db.session.commit()
        return new_elmt

    @classmethod
    def list(cls):
        stmt = db.select(cls.app_id)
        return db.session.scalars(stmt)

    @classmethod
    def remove(cls, app_id, commit=True):
        stmt = db.delete(cls).where(cls.app_id == str(app_id))
        db.session.execute(stmt)
        if commit:
            db.session.commit()
