from sqlalchemy import Enum

from .. import db
from .base import Model

from .enums import EntityTypeEnum

class EntityLink(Model):
    __abstract__ = True
    entity_id = db.Column(db.Integer)
    entity_type = db.Column(Enum(EntityTypeEnum))

    @classmethod
    def get_entity(cls, entity_type=None, entity_id=None, args=None):
        from . import mapping
        if not args == None:
            entity_type = args['entity']['type']
            entity_id = args['entity']['id']
        elif entity_type==None or entity_id==None:
            raise Exception("Need to provide some args")
        query_cls = mapping[entity_type]
        if not query_cls:
            raise Exception("No entity class with this type", entity_type)

        #visibility_filter = query_cls.get_visibility_filter()
        #entity = db.one_or_404(db.select(query_cls).filter(query).filter(cls.id == entity_id))
        entity = db.get_or_404(query_cls, entity_id)
        return entity

    @property
    def entity(self):
        return self.__class__.get_entity(self.entity_type.name, self.entity_id)

    @classmethod
    def get_for_entity(cls, entity_id, entity_type, *extra_queries):
        params, extra_queries = cls.parse_extra_queries(extra_queries)

        query = (cls.entity_id==entity_id) & (cls.entity_type==entity_type)
        for par_name, value in params.items():
            if isinstance(value, list):
                query = query & (getattr(cls, par_name).in_(value))
            else:
                query = query & (getattr(cls, par_name) == value)

        stmt = db.select(cls).filter(query)
        if hasattr(cls, 'get_visibility_filter'):
            visibility_filter = cls.get_visibility_filter()
            stmt = stmt.filter(visibility_filter)

        if extra_queries:
            stmt = stmt.filter(*extra_queries)

        return db.session.execute(stmt).scalars()

    @classmethod
    def parse_json(cls, args, entity_id_required=True, entity_type_required=True):
        params = {}
        if entity_id_required:
            params['entity_id'] = args['entity']['id']
        else:
            params['entity_id'] = args.get('entity', {}).get('id')
        if entity_type_required:
            params['entity_type'] = args['entity']['type']
        else:
            params['entity_type'] = args.get('entity', {}).get('type')

        return params

    def to_json(self):
        return {'id': self.id,
                'entity': {'type': self.entity_type.name, 'id': self.entity_id}}


