from sqlalchemy import Enum, event

from .. import db
from .base import Model
from .enums import FileSectionsEnum
from .mixin.entity import Entity
from .relation import Relation

class File(Entity, Model):
    __tablename__ = 'files'

    name = db.Column(db.String(100), nullable=False)
    path = db.Column(db.String(255), nullable=False)
    alt = db.Column(db.String(100))
    mime = db.Column(db.String(100))
    thumbnail = db.Column(db.String(100))
    digest = db.Column(db.BLOB, nullable=False)
    size = db.Column(db.Integer, nullable=False) # bytes
    category = db.Column(db.String(16))
    section = db.Column(Enum(FileSectionsEnum), server_default='generic')

    allowed_attributes = Entity.allowed_attributes+['alt']

    def to_json(self):
        data = Model.to_json(self)
        data.update(Entity.to_json(self))

        data.update({'name': self.name,
                     'alt': self.alt,
                     'size': self.size,
                     'avatar': self.get_avatar(),
                     'mime': self.mime})
        return data

    def to_avatar_json(self):
        linked = self.get_linked()
        res = {'avatar': self.get_avatar(), 'id': self.id, 'name': self.name, 'mime': self.mime, 'subtype': 'file'}
        if linked:
            res['entity'] = linked.to_avatar_json()
        return res

    def get_avatar(self):
        if self.thumbnail:
            return '/media/256/'+self.name
        else:
            return '/media/'+self.name

    @classmethod
    def exists(cls, name):
        query = db.select(File).filter(File.name == name)
        afile = db.session.scalars(query).first()
        return afile is not None

    @classmethod
    def find_by_digest(cls, digest, section=None, category=None):
        query = db.select(File).filter(File.digest == digest)
        if section:
            query = query.filter(File.section == section)
        if category:
            query = query.filter(File.category == category)
        return db.session.scalars(query).first()

    @classmethod
    def parse_json(cls, args, group_id=None, vis=None, perms=None):
        params = Entity.parse_json(args, group_id, vis, perms)

        params.update({'name': args.get('name'),
                       'path': args.get('path'),
                       'category': args.get('category'),
                       'digest': args.get('digest'),
                       'thumbnail': args.get('thumbnail'),
                       'mime': args.get('mime'),
                       'section': args.get('section'),
                       'size': args.get('size'),
                       'alt': args.get('alt')})

        return params

    @classmethod
    def create(cls, args, update_river=True):
        params = {}

        params.update(cls.parse_json(args, args.get('group_id', 1)))

        return super(cls, cls).create(args, params, update_river=update_river)

    @classmethod
    def list_related(cls, entity_id, entity_type):
        return Relation.list_related('file', entity_id, entity_type)

@event.listens_for(File, 'after_delete')
def receive_after_delete(mapper, connection, target):
    print("after_delete", target)

def after_delete_listener(mapper, connection, target):
    # 'target' is the inserted object
    print("after delete", target)

event.listen(File, 'after_delete', after_delete_listener)

