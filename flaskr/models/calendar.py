import enum
from datetime import datetime, timezone
from dateutil.parser import isoparse

from sqlalchemy import func, ForeignKey, Enum
import flask_login

from .. import db
from .base import Model
from .mixin.owned import Owned

class Calendar(Owned, Model):
    __tablename__ = 'calendars'
    avatar = db.Column(db.String(125))
    title = db.Column(db.String(100), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now)

    def to_json(self):
        data = Model.to_json(self)
        data.update(Owned.to_json(self))
        data.update({'title': self.title})
        return date

    @classmethod
    def create(cls, args):
        params = Owned.parse_json(args)
        params.update({'title': args['title']})

        new_cal = super(Model, cls).create(args, params)
        return new_cal

    @classmethod
    def update(cls, id, args):
        cal = db.get_or_404(cls, id)
        cal.title = args['title']
        db.session.commit()
        return cal

