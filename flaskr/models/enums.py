import enum

class ValidationTokenType(enum.Enum):
    email = 0
    password = 1
    otp = 2
    access = 3

class RegistrationStatus(enum.Enum):
    disabled = 0
    open = 1
    closed = 2

class RegistrationValueType(enum.Enum):
    disabled = 0
    enabled = 1
    required = 2

class AccountingRecordType(enum.Enum):
    generic = 0
    member_fees = 1
    donation = 2
    event_expense = 3
    event_income = 4
    coinpot = 5
    deleted = 6
    place = 7
    consumables = 8

class FileSectionsEnum(enum.Enum):
    generic = 0
    avatar = 1
    document = 2

class BookmarkTypeEnum(enum.Enum):
    generic = 0
    instagram = 1
    facebook = 2
    whatsapp = 3
    youtube = 4
    bgg = 5
    email = 6
    telegram = 7
    reddit = 8
    twitch = 9
    signal = 10
    steam = 11
    wikipedia = 12
    vimeo = 13
    github = 14

class GroupPermissionsEnum(enum.Enum):
    observer = 0
    member = 1
    editor = 2
    admin = 3

class EventTypeEnum(enum.Enum):
    single = 0
    league = 1
    tournament = 2
    social = 3 # non-gaming
    event = 4 # non-gaming
    workshop = 5
    presentation = 6
    proposal = 7

class VisibilityEnum(enum.Enum):
    public = 0
    members = 1
    group = 2
    private = 3
    admin = 4

class PermissionEnum(enum.Enum):
    public = 0
    members = 1
    group = 2
    user = 3

# https://stackoverflow.com/questions/2676133/best-way-to-do-enum-in-sqlalchemy
class EntityTypeEnum(enum.Enum):
    user = 0
    event = 1
    calendar = 2
    group = 3
    bookmark = 4
    file = 5
    game = 6
    river = 7
    accounting_record = 8
    unowned_message = 9
    unowned_registration = 10
    comment = 11

class AnnotationTypeEnum(enum.Enum):
    like = 0
    have = 1
    wannaplay = 2
    going = 3

