from datetime import datetime

import flask_login

from .. import db
from .base import Model

class Venue(Model):
    __tablename__ = 'venues'
    title = db.Column(db.String(100), nullable=False)
    url = db.Column(db.String(100))
    description = db.Column(db.Text)
    is_global = db.Column(db.Boolean, default=False, nullable=False)
    avatar = db.Column(db.String(125))
    # https://stackoverflow.com/questions/15965166/what-are-the-lengths-of-location-coordinates-latitude-and-longitude
    latitude = db.Column(db.DECIMAL(11, 8))
    longitude = db.Column(db.DECIMAL(11, 8))
    created_at = db.Column(db.DateTime, default=datetime.now)
    allowed_attributes = ['title', 'url', 'description', 'is_global', 'avatar', 'latitude', 'longitude']

    def __repr__(self):
        return f'<Venue {self.title}>'

    def to_json(self):
        res = self.dump_attributes('id', 'title', 'url')
        res['global'] = self.is_global
        if self.latitude and self.longitude:
            res['geo'] = {'lat': self.latitude,'long': self.longitude}
        return res

    @classmethod
    def create(cls, args):
        params = {'user_id': flask_login.current_user.id,
                  'title': args['title'],
                  'is_global': args['global'],
                  'latitude': args.get('geo', {}).get('lat'),
                  'longitude': args.get('geo', {}).get('long'),
                  'description': args.get('title', None)}
        new_cal = super(cls, cls).create(args, params)
        return new_cal

