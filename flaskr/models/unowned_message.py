from sqlalchemy import func, Enum

from .. import db

from .unowned_data import UnownedData

class UnownedMessage(UnownedData):
    __tablename__ = 'unowned_messages'

    title = db.Column(db.String(125), nullable=False)
    description = db.Column(db.Text)

    allowed_attributes = ['title', 'description']

    def to_json(self):
        data = self.dump_attributes('title', 'description')
        data.update(UnownedData.to_json(self))
        return data

    @classmethod
    def parse_json(cls, args, group_id=None, default_visibility='members', default_permissions='user'):
        params = cls.fetch_attributes(args, 'title', 'description')
        params.update(UnownedData.parse_json(args))
        return params


