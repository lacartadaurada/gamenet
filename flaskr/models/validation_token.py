from .. import db

from sqlalchemy import Enum, ForeignKey
from .base import Model
from .enums import ValidationTokenType
from datetime import datetime
import secrets
import hashlib

class ValidationToken(Model):
    __tablename__ = 'validation_tokens'
    user_id = db.Column(db.Integer, ForeignKey("users.id"), nullable=False)
    code = db.Column(db.String(64), nullable=False)
    action = db.Column(Enum(ValidationTokenType), nullable=False)
    created_at = db.Column(db.DateTime, default=datetime.now)

    def isoformat(self, date):
        return date.astimezone(timezone.utc).isoformat(timespec="milliseconds").replace("+00:00", "Z")

    def to_json(self):
        data = Model.to_json(self)
        data.update(self.dump_attributes('code',
                                         action = self.action.name,
                                         userId = self.user_id,
                                         created_at = self.isoformat(self.created_at)))
        return data

    @classmethod
    def create_code(cls):
        return hashlib.sha3_256(secrets.token_bytes(64)).hexdigest()
        #return hashlib.shake_256(secrets.token_bytes(64)).hexdigest(16)
        #return secrets.token_hex(20)
        #return secrets.token_urlsafe(20)

    @classmethod
    def create(cls, user_id, action):
        params = {'user_id': user_id, 'action': action, 'code': cls.create_code()}
        return super(cls, cls).create({}, params)

    @classmethod
    def parse_json(cls, args):
        params = cls.fetch_attributes(args, 'code', 'action', user_id=args.get('userId'))
        return params

