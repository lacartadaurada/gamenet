from datetime import datetime

from flask_login import UserMixin, AnonymousUserMixin
from sqlalchemy.orm import relationship
from sqlalchemy.ext.hybrid import hybrid_property
from werkzeug.security import generate_password_hash, check_password_hash
import flask_login

from .. import db
from .enums import PermissionEnum
from .base import Model
from .group_member import GroupMember


class User(Model, UserMixin):
    __tablename__ = 'users'

    username = db.Column(db.String(60), unique=True, nullable=False)
    display_name = db.Column(db.String(60))
    private_name = db.Column(db.String(60))
    motto = db.Column(db.Text)
    email = db.Column(db.String(255), unique=True, nullable=True)
    password_hash = db.Column(db.String(255), nullable=False)
    avatar = db.Column(db.String(100))
    activated = db.Column(db.Boolean, default=False)
    email_validated = db.Column(db.Boolean, default=False)
    quota = db.Column(db.Integer, default=2000) # MB
    spent_quota = db.Column(db.Integer, default=0) # MB
    created_at = db.Column(db.DateTime, default=datetime.now)
    events = relationship("Event", back_populates="user")
    admin = db.Column(db.Boolean, default=False)

    allowed_attributes = ['email', 'display_name', 'avatar', 'private_name', 'motto']

    def get_group_ids(self):
        return db.session.scalars(db.select(GroupMember.group_id).where(self.id == GroupMember.user_id))

    def get_groups(self):
        from .group import Group
        return Group.groups_for_user(self.id)

    def member_of(self, group_id):
        member = db.session.scalars(db.select(GroupMember).filter(GroupMember.user_id == self.id)).first()
        return not member == None

    def to_avatar_json(self):
        if flask_login.current_user.id > 0:
            name = self.private_name or self.display_name or self.username
        else:
            name = self.display_name or self.username
        return {'avatar': self.avatar, 'id': self.id, 'name': name, 'subtype': 'user'}

    def to_json(self, minimal=False):
        args = {'id': self.id,
                'avatar': self.avatar,
                'name': self.display_name or self.username}
        if minimal:
            return args
        args['motto'] = self.motto
        if flask_login.current_user.id == self.id:
            args['username'] = self.username
            args['email'] = self.email
            args['quota'] = self.quota
            args['spent_quota'] = self.spent_quota
            args['editable'] = True
        if flask_login.current_user.id > 0:
            args['privateName'] = self.private_name
        return args

    @classmethod
    def parse_json(cls, args, group_id=None):
        params = {'display_name': args.get('name'),
                  'private_name': args.get('privateName'),
                  'email': args.get('email'),
                  'motto': args.get('motto'),
                  'avatar': args.get('avatar')}
        return params
 
    def can_edit(self, entity):
        return entity.editable_by(self)

    @hybrid_property
    def password(self):
        return self.password_hash

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    @classmethod
    def authenticate(cls, user_id, password):
        user = cls.query.filter(db.or_(cls.username == user_id, cls.email == user_id)).first()
        if user is not None and check_password_hash(user.password, password) and user.activated:
            return user

    def __str__(self):
        return '<User: %s>' % self.username


class AnonymousUser(AnonymousUserMixin):
    @property
    def id(self):
        return 0
    @property
    def username(self):
        return "anon"
    @property
    def admin(self):
        return False
    @property
    def avatar(self):
        return "anon"
    def can_edit(self, entity):
        return False
    def member_of(self, group_id):
        return False
    def get_group_ids(self):
        return []
    def get_groups(self):
        return []
