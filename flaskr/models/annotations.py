from datetime import datetime
from sqlalchemy import func, ForeignKey, Enum
import flask_login

from .. import db
from .base import Model
from .mixin.permissions import Permissions
from .entity_link import EntityLink

from .enums import AnnotationTypeEnum

class Annotation(EntityLink):
    __tablename__ = 'annotations'
    user_id = db.Column(db.Integer, ForeignKey("users.id"), nullable=False)
    annotation_type = db.Column(Enum(AnnotationTypeEnum), nullable=False)

    def to_json(self):
        data = EntityLink.to_json(self)
        data.update({'type': self.annotation_type.name, 'userId': self.user_id})
        return data

    @classmethod
    def has_annotation(cls, entity_type, entity_id, annotation_type, user_id=None):
        params = {'entity_id': entity_id,
                  'entity_type': entity_type,
                  'annotation_type': annotation_type}
        if user_id:
            params['user_id'] = user_id
        stmt = db.select(cls)
        for par_name, value in params.items():
            stmt = stmt.filter(getattr(cls, par_name) == value)
        return not db.session.execute(stmt).first() == None


    @classmethod
    def create(cls, entity_type, entity_id, annotation_type, commit=True):
        params = {'entity_id': entity_id,
                  'entity_type': entity_type,
                  'annotation_type': annotation_type,
                  'user_id': flask_login.current_user.id}
        new_elmt = cls(**params)

        db.session.add(new_elmt)
        if commit:
            db.session.commit()
        return new_elmt

    @classmethod
    def remove(cls, entity_type, entity_id, annotation_type, commit=True):
        stmt = db.delete(cls).where(cls.entity_type == entity_type,
                                    cls.entity_id == entity_id,
                                    cls.annotation_type == annotation_type,
                                    cls.user_id == flask_login.current_user.id)
        db.session.execute(stmt)
        if commit:
            db.session.commit()
        return {'result': 'ok'}

    @classmethod
    def search(cls, params):
        params = {k: v for k, v in params.items() if v is not None}
        stmt = db.select(cls)
        for par_name, value in params.items():
            if isinstance(value, list):
                stmt = stmt.filter(getattr(cls, par_name).in_(value))
            else:
                stmt = stmt.filter(getattr(cls, par_name) == value)
        return db.session.execute(stmt).scalars()


