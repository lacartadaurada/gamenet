import enum
from datetime import datetime, timezone
from dateutil.parser import isoparse

from sqlalchemy import func, ForeignKey, Enum
import flask_login

from .. import db

from .enums import GroupPermissionsEnum
from .base import Model
from .mixin.owned import Owned

class GroupMember(Model, Owned):
    __tablename__ = 'group_members'
    permissions = db.Column(Enum(GroupPermissionsEnum), server_default='observer')
    created_at = db.Column(db.DateTime, default=datetime.now)

