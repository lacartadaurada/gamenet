from sqlalchemy import func, Enum

from .. import db

from .unowned_data import UnownedData

class UnownedRegistration(UnownedData):
    __tablename__ = 'unowned_registrations'

    email = db.Column(db.String(125), nullable=False)
    age = db.Column(db.Integer)
    phone = db.Column(db.String(30))
    notes = db.Column(db.Text)

    def to_json(self):
        data = self.dump_attributes('email', 'age', 'phone', 'notes')
        data.update(UnownedData.to_json(self))
        return data

    @classmethod
    def parse_json(cls, args):
        params = cls.fetch_attributes(args, 'email', 'age', 'phone', 'notes')
        params.update(UnownedData.parse_json(args))
        return params


