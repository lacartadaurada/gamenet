from datetime import datetime
from dateutil.parser import isoparse

from sqlalchemy import func, ForeignKey, Enum, or_, and_
from sqlalchemy.orm import relationship
import flask_login

from .. import db

from .base import Model
from .enums import EventTypeEnum, VisibilityEnum, RegistrationStatus
from .group_member import GroupMember
from .calendar import Calendar
from .mixin.entity import Entity
from .mixin.slug import Slug

class Event(Entity, Slug, Model):
    __tablename__ = 'events'

    avatar = db.Column(db.String(125))
    color = db.Column(db.String(7))
    calendar_id = db.Column(db.Integer, ForeignKey("calendars.id"), nullable=False)
    venue_id = db.Column(db.Integer, ForeignKey("venues.id"))
    event_type = db.Column(Enum(EventTypeEnum), server_default='single')
    title = db.Column(db.String(100), nullable=False)
    all_day = db.Column(db.Boolean)
    description = db.Column(db.Text)
    max_participants = db.Column(db.Integer, default=0)
    min_participants = db.Column(db.Integer, default=0)
    participants = db.Column(db.Integer, default=0)
    date_start = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    date_end = db.Column(db.DateTime(timezone=True),
                           server_default=func.now())
    registration_status = db.Column(Enum(RegistrationStatus), server_default='disabled')

    user = relationship("User", back_populates="events")

    allowed_attributes = Entity.allowed_attributes+['avatar', 'color', 'venue_id', 'event_type', 'title', 'all_day', 'description',
                                                    'max_participants', 'min_participants', 'participants', 'date_start', 'date_end', 'registration_status']

    def to_json(self):
        data = {'title': self.title,
                'start': self.isoformat(self.date_start),
                'end': self.isoformat(self.date_end),
                'description': self.description,
                'participants': {'current': self.participants, 'min': self.min_participants, 'max': self.min_participants},
                'allDay': self.all_day,
                'color': self.color,
                'avatar': self.avatar,
                'eventType': self.event_type.name,
                'venueId': self.venue_id,
                'registration': self.registration_status.name}
        data.update(Model.to_json(self))
        data.update(Slug.to_json(self))
        data.update(Entity.to_json(self))
        if self.color:
            data['color'] = self.color
        return data

    @classmethod
    def parse_json(cls, args, group_id=None, default_visibility='members', default_permissions='user'):
        date_start = cls.parse_date(args, 'startDate')
        date_end = cls.parse_date(args, 'endDate')
        params = {'date_start': date_start,
                  'date_end': date_end,
                  'title': args.get('title'),
                  'avatar': args.get('avatar'),
                  'venue_id': args.get('venueId', 1),
                  'all_day': args.get('allDay'),
                  'color': cls.get_color(args),
                  'event_type': args.get('eventType'),
                  'registration_status': args.get('registration')}

        params['description'] = cls.sanitized(args, 'description')
        params.update(Entity.parse_json(args, group_id, default_visibility, default_permissions))

        participants = args.get('participants')
        if participants:
            params['participants'] = participants.get('current')
            params['min_participants'] = participants.get('min')
            params['max_participants'] = participants.get('max')
        return params

    @classmethod
    def get_color(cls, args):
        color = args.get('color')
        if color == '#3788d8':
            return None
        return color

    @classmethod
    def create(cls, calendar_id, args):
        calendar = db.get_or_404(Calendar, calendar_id)

        params = {'calendar_id': calendar_id}
        params.update(cls.parse_json(args, calendar.group_id))

        params['slug'] = cls.generate_slug(args['title'])

        return super(cls, cls).create(args, params)

    @classmethod
    def get_for_calendar(cls, calendar_id, start=None, end=None):
        base_filter = (cls.calendar_id==calendar_id)
        visibility_filter = cls.get_visibility_filter()

        query = db.select(cls).filter(base_filter)
        query = query.filter(visibility_filter)
        if start and end:
            start_date = datetime.fromisoformat(start)
            end_date = datetime.fromisoformat(end)
            query = query.filter(cls.date_start >= start_date, cls.date_start <= end_date)

        return db.session.scalars(query)

    @classmethod
    def get_upcoming(cls, calendar_id, max_elements):
        base_filter = (cls.calendar_id==calendar_id)
        visibility_filter = cls.get_visibility_filter()

        query = db.select(cls).filter(base_filter)
        query = query.filter(visibility_filter)
        query = query.filter(cls.date_end > datetime.now())
        query = query.order_by(Event.date_start.asc())
        query = query.limit(max_elements)

        return db.session.scalars(query)

