from sqlalchemy import func

from .. import db

from .mixin.date_mixin import DateMixin
from .entity_link import EntityLink

class UnownedData(DateMixin, EntityLink):
    __abstract__ = True

    sender = db.Column(db.String(50), nullable=False)

    def to_json(self):
        data = {'sender': self.sender}
        data.update(DateMixin.to_json(self))
        data.update(EntityLink.to_json(self))
        return data

    @classmethod
    def parse_json(cls, args):
        params = {'sender': args.get('sender')}

        params.update(DateMixin.parse_json(args))
        params.update(EntityLink.parse_json(args))
        return params

    @classmethod
    def create(cls, args):
        params = cls.parse_json(args)

        return super(UnownedData, cls).create(args, params)

    @classmethod
    def count(cls, *extra_queries):
        stmt = db.select(cls.entity_id, func.count(cls.id).label('count'))
        if extra_queries:
            stmt = stmt.filter(*extra_queries)
        stmt = stmt.group_by(cls.entity_id)
        return db.session.execute(stmt).all()

    @classmethod
    def list_related(cls, entity_id, entity_type, *extra_queries):
        return cls.get_for_entity(entity_id, entity_type, *extra_queries)

