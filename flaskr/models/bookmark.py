from sqlalchemy import Enum

from .. import db
from .base import Model
from .relation import Relation

from .mixin.entity import Entity
from .enums import BookmarkTypeEnum

class Bookmark(Entity, Model):
    __tablename__ = 'bookmarks'

    title = db.Column(db.String(100))
    url = db.Column(db.String(255), nullable=False)
    type = db.Column(Enum(BookmarkTypeEnum), server_default='generic')

    allowed_attributes = Entity.allowed_attributes+['title', 'url', 'type']

    def to_avatar_json(self):
        data = Entity.to_json(self)
        data['url'] = self.url
        data['entity'] = self.get_linked().to_avatar_json()
        data['subtype'] = 'bookmark'
        return data

    def to_json(self):
        data = Model.to_json(self)
        data.update(Entity.to_json(self))

        data.update(self.dump_attributes('title', 'url', type=self.type.name))
        return data

    @classmethod
    def is_email(cls, url):
        if '@' in url and url.split('@') == 2 and url.split('@')[1].split('.') > 0:
            return True
        return False

    @classmethod
    def determine_type(cls, url):
        if 'facebook.com' in url:
            return 'facebook'
        elif 'instagram.com' in url:
            return 'instagram'
        elif 'youtube.com' in url:
            return 'youtube'
        elif 'boardgamegeek.com' in url:
            return 'bgg'
        elif url.replace(" ", "").isdigit():
            return 'whatsapp'
        elif cls.is_email(url):
            return 'email'
        return 'generic'

    @classmethod
    def parse_json(cls, args, group_id=None, vis=None, perms=None):
        entity = Relation.get_entity(args['entity']['type'], args['entity']['id'])

        params = {}
        params.update(Entity.parse_json(args, entity.group_id, vis, perms))

        params.update(cls.fetch_attributes(args, 'url', 'title', type=args.get('type', cls.determine_type(args['url'])) ))
        return params

    @classmethod
    def create(cls, args):
        params = cls.parse_json(args)

        entity = Relation.get_entity(args['entity']['type'], args['entity']['id'])

        new_bookmark = super(cls, cls).create(args, params)
        new_relation = Relation.create_relation(entity, new_bookmark)

        return new_bookmark

    @classmethod
    def list_related(cls, entity_id, entity_type):
        return Relation.list_related('bookmark', entity_id, entity_type)


