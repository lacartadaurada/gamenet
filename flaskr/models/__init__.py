mapping = {}

from .user import User, AnonymousUser
from .calendar import Calendar
from .group_member import GroupMember
from .event import Event
from .venue import Venue
from .group import Group
from .annotations import Annotation
from .entity_link import EntityLink
from .comment import Comment
from .file import File
from .bookmark import Bookmark
from .game import Game
from .relation import Relation
from .telegram_subscription import TelegramSubscription
from .unowned_message import UnownedMessage
from .unowned_registration import UnownedRegistration
from .accounting_record import AccountingRecord
from .registration_config import RegistrationConfig
from .river import River
from .validation_token import ValidationToken

# entity_link parents
mapping['user'] = User
mapping['event'] = Event
mapping['calendar'] = Calendar
mapping['file'] = File
mapping['bookmark'] = Bookmark
mapping['game'] = Game
mapping['comment'] = Comment
mapping['accounting_record'] = AccountingRecord
mapping['unowned_message'] = UnownedMessage
mapping['unowned_registration'] = UnownedRegistration

def class_map(a_class):
    return list(mapping.keys())[list(mapping.values()).index(a_class)]
