import enum
from datetime import datetime
from dateutil.parser import isoparse

from sqlalchemy import func, ForeignKey, Enum, or_, and_
import flask_login

from .. import db

from .base import Model
from .enums import AccountingRecordType
from .group_member import GroupMember
from .calendar import Calendar
from .mixin.entity import Entity
from .mixin.slug import Slug

class AccountingRecord(Entity, Model):
    __tablename__ = 'accounting_records'

    title = db.Column(db.String(100), nullable=False)
    description = db.Column(db.Text)
    amount = db.Column(db.DECIMAL(8,2), nullable=False)
    type = db.Column(db.Enum(AccountingRecordType), server_default='generic')
    paid_by = db.Column(db.String(20))
    paid_by_id = db.Column(db.ForeignKey("users.id"))
    paid_to = db.Column(db.String(20))
    paid_to_id = db.Column(db.ForeignKey("users.id"))
    pending = db.Column(db.Boolean, default=False)
    payment_date = db.Column(db.DateTime(timezone=True),
                                   server_default=func.now())
    bill_date = db.Column(db.DateTime(timezone=True),
                                server_default=func.now())

    allowed_attributes = Entity.allowed_attributes+['title', 'description', 'amount', 'type', 'paid_by', 'paid_by_id', 'paid_to', 'paid_to_id', 'payment_date', 'bill_date']

    def to_avatar_json(self):
        return {'id': self.id, 'title': self.title, 'subtype': 'accounting_record'}

    def to_json(self):
        record_type = None
        if self.type:
            record_type = self.type.name
        data = {'title': self.title,
                'description': self.description,
                'amount': self.amount,
                'paymentDate': self.isoformat(self.payment_date),
                'billDate': self.isoformat(self.bill_date),
                'paidBy': self.paid_by,
                'paidById': self.paid_by_id,
                'paidTo': self.paid_to,
                'paidToId': self.paid_to_id,
                'type': record_type}
        if self.pending:
            data['pending'] = True
        data.update(Model.to_json(self))
        data.update(Entity.to_json(self))
        return data

    @classmethod
    def parse_json(cls, args, group_id=None, default_visibility='members', default_permissions='user'):
        payment_date = cls.parse_date(args, 'paymentDate')
        bill_date = cls.parse_date(args, 'billDate')
        params = {'title': args.get('title'),
                  'description': args.get('description'),
                  'amount': args.get('amount'),
                  'payment_date': payment_date,
                  'bill_date': bill_date,
                  'paid_by_id': args.get('paidById'),
                  'paid_by': args.get('paidBy'),
                  'paid_to_id': args.get('paidToId'),
                  'paid_to': args.get('paidTo'),
                  'pending': args.get('pending', False),
                  'type': args.get('type', 'generic')}

        params.update(Entity.parse_json(args, group_id, default_visibility, default_permissions))
        return params

    @classmethod
    def create(cls, group_id, args):
        params = {}

        default_group_id = group_id
        params.update(cls.parse_json(args, default_group_id))
        params['visibility'] = 'group'
        params['permissions'] = 'group'

        return super(cls, cls).create(args, params)

