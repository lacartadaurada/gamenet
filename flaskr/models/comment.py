from .. import db

from sqlalchemy import func
from .entity_link import EntityLink
from .mixin.entity import Entity

class Comment(Entity, EntityLink):
    __tablename__ = 'comments'
    content = db.Column(db.Text)

    def to_avatar_json(self):
        data = Entity.to_json(self)

        data.update({'content': self.content})
        data['entity'] = self.entity.to_avatar_json()
        data['subtype'] = 'comment'
        return data

    def to_json(self):
        data = EntityLink.to_json(self)

        data.update(Entity.to_json(self))
        data.update({'content': self.content})
        return data

    @classmethod
    def create(cls, args):
        params = cls.parse_json(args)
        params['visibility'] = 'public'

        return super(cls, cls).create(args, params)

    @classmethod
    def parse_json(cls, args):
        entity = cls.get_entity(args=args)

        params = EntityLink.parse_json(args)
        params.update(Entity.parse_json(args, entity.group_id))
        params.update({'content': args['content']})
        return params

    @classmethod
    def count(cls, *extra_queries):
        stmt = db.select(cls.entity_id, func.count(cls.id).label('count'))
        stmt.filter(cls.get_visibility_filter())
        if extra_queries:
            stmt = stmt.filter(*extra_queries)
        stmt = stmt.group_by(cls.entity_id)
        return db.session.execute(stmt).all()

    @classmethod
    def list_related(cls, entity_id, entity_type):
        return cls.get_for_entity(entity_id, entity_type)

