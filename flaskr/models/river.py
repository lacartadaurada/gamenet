import traceback

from sqlalchemy import func, ForeignKey, Enum, or_, and_, null
import flask_login

from .. import db

from .entity_link import EntityLink
from .mixin.visibility import Visibility
from .mixin.date_mixin import DateMixin
from datetime import datetime, timedelta

enabled = True

class River(EntityLink, Visibility, DateMixin):
    __tablename__ = 'river'

    user_id = db.Column(db.Integer, ForeignKey("users.id"), nullable=False)
    group_id = db.Column(db.Integer, ForeignKey("groups.id"))
    action = db.Column(db.String(16))

    @classmethod
    def update_visibility(cls, entity, new_visibility):
        from . import class_map
        try:
            entity_type = class_map(entity.__class__)
        except:
            print("failed updating river visibility")
            return
        print("updating river visibility")
        stmt = db.update(cls).where(cls.entity_id == entity.id, cls.entity_type==entity_type).values(visibility=new_visibility)
        return db.session.execute(stmt)

    @classmethod
    def add_action(cls, action, entity, commit=True):
        if not flask_login.current_user.id:
            return
        try:
            return cls.add_action_internal(action, entity, commit)
        except:
            traceback.print_exc()

    @classmethod
    def add_action_internal(cls, action, entity, commit=True):
        from . import class_map
        if not enabled:
            return False
        try:
            entity_type = class_map(entity.__class__)
        except:
            return
        try:
            group_id = entity.group_id
        except:
            group_id = None
        try:
            visibility = entity.visibility
        except:
            if group_id:
                visibility = 'group'
            else:
                visibility = null()
        cut_date = datetime.today() - timedelta(hours=1)
        res = cls.search(cls.action == action, cls.entity_id==entity.id, cls.entity_type==entity_type, cls.updated_at >= cut_date, cls.user_id==flask_login.current_user.id).first()
        if res:
            res.updated_at = datetime.today()
            if commit:
                db.session.commit()
            return
        else:
            return cls.create(commit, entity_id=entity.id, entity_type=entity_type, action=action, group_id=group_id, visibility=visibility)

    def to_json(self):
        data = self.dump_attributes('action', userId=self.user_id, groupId=self.group_id)
        data.update(EntityLink.to_json(self))
        data.update(DateMixin.to_json(self))
        return data

    @classmethod
    def parse_json(cls, args, group_id=None, default_visibility='members', default_permissions='user'):
        params = cls.fetch_attributes(args, 'action', user_id=args.get('userId'), group_id=args.get('groupId'))

        params.update(EntityLink.parse_json(args, False, False))
        params.update(DateMixin.parse_json(args))

        return params

    @classmethod
    def create(cls, commit=True, **params):
        params['user_id'] = flask_login.current_user.id

        return super(cls, cls).create({}, params, commit=commit)

