from sqlalchemy import func, Enum

from .. import db

from .entity_link import EntityLink
from .enums import RegistrationValueType
from flask import abort



class RegistrationConfig(EntityLink):
    __tablename__ = 'registration_configs'

    title = db.Column(db.String(100))
    description = db.Column(db.Text)
    email = db.Column(Enum(RegistrationValueType))
    age = db.Column(Enum(RegistrationValueType))
    phone = db.Column(Enum(RegistrationValueType))

    allowed_attributes = ['title', 'description', 'age', 'phone', 'email']

    def to_json(self):
        data = self.dump_attributes('title', 'description', age=self.age.name, phone=self.phone.name, email=self.email.name)
        data.update(EntityLink.to_json(self))
        return data

    @classmethod
    def parse_json(cls, args, entity_id_required=None):
        params = cls.fetch_attributes(args, 'title', 'age', 'phone', 'email')
        params['description'] = cls.sanitized(args, 'description')
        params.update(EntityLink.parse_json(args, entity_id_required))
        return params

    @classmethod
    def create_or_update(cls, args, commit=True):
        params = cls.parse_json(args)

        parent = cls.get_entity(params['entity_type'], params['entity_id'])
        if not parent.is_editable():
            raise abort(403, {"error": "no permissions"})

        reg_config = cls.get_for_entity(params['entity_id'], params['entity_type']).first()
        if not reg_config:
            return cls.create(args, params, parent)
        else:
            reg_config.assign(params)
            if commit:
                db.session.commit()
        return reg_config

    @classmethod
    def get_for_slug(cls, slug, entity_type):
        from . import mapping
        entity_cls = mapping[entity_type]

        #query = (cls.entity_id==entity_id) & (cls.entity_type==entity_type)
        query = (entity_cls.slug == slug) & (cls.entity_type==entity_type)

        stmt = db.select(cls).join(entity_cls, entity_cls.id == cls.entity_id).filter(query)
        if hasattr(entity_cls, 'get_visibility_filter'):
            visibility_filter = entity_cls.get_visibility_filter()
            stmt = stmt.filter(visibility_filter)

        return db.session.execute(stmt).scalars()

    @classmethod
    def create(cls, args, params=None, parent=None):
        if not params:
            params = cls.parse_json(args)

        if not parent:
            parent = cls.get_entity(params['entity_id'], params['entity_type'])
            if not parent.is_editable():
                raise abort(403, {"error": "no permissions"})

            others = cls.get_for_entity(params['entity_id'], params['entity_type'])
            if others:
                raise abort(400, {"error": "ya existe configuracion"})

        return super(cls, cls).create(args, params)

