+++
title = "Trastienda"
description = "No la lies"
hidesubtitle = true
angularapp = true
hidetitle = true
+++

## La Trastienda de La Carta Daurada

{{< rawhtml >}}

<div ng-controller='LoginController'>

<br>
    <div class="row">
      <div class="col-md-4 mb-3">
        <div class="card h-100">
          <!--<img class="card-img-top" src="/images/local-2.jpg" alt="">-->
          <div class="card-body">
            <h4 class="card-title">Calendario</h4>
	    <!--<p class="card-text">Todos los miércoles en nuestro local a partir de las 17:30, juegos de mesa para todas las edades.</p>-->
	    <div class="card-text">
        <p>Gestión de eventos, organización de partidas, disponibilidad...</p>
        <p><a href="/calendario/">calendario</a></p>
        <p><a href="/actividades/">próximas actividades</a></p>
        <p ng-if="login.isLoggedIn"><span ng-cloak="login.isLoggedIn"><a href="/eventos/new/">añadir evento</a></span></p>
      </div>
	    <div class="know-more"></div>
          </div>
          <!--<div class="card-footer">
            <a href="#" class="btn btn-primary">Find Out More!</a>
          </div>-->
        </div>
      </div>
      <div class="col-md-4 mb-3">
        <div class="card h-100">
          <!--<img class="card-img-top" src="/images/local-4.jpg" alt="">-->
          <div class="card-body">
            <h4 class="card-title">Juegos</h4>
            <div class="card-text">
              <p>Gestión de juegos.</p>
              <p><a href="/juegos/">Listado de juegos</a></p>
            </div>
          </div>
          <!--<div class="card-footer">
            <a href="#" class="btn btn-primary">Find Out More!</a>
          </div>-->
        </div>
      </div>
      <div class="col-md-4 mb-3">
        <div class="card h-100">
          <!--<img class="card-img-top" src="/images/local-5.jpg" alt="">-->
          <div class="card-body">
            <h4 class="card-title">Tu cuenta</h4>
            <div class="card-text">
              <p>Gestiona tu cuenta en este sitio web.</p>
              <div ng-cloak ng-if="login.loginReady">
                <a ng-show="!login.isLoggedIn" class="btn btn-primary" href="/login/">login</a>
                <a ng-show="!login.isLoggedIn" class="btn btn-secondary" href="/registro/">registro</a>
                <p ng-show="login.isLoggedIn"><a href="/profile/">perfil</a></p>
                <p ng-show="login.isLoggedIn"><a href="/profile/edit/">editar perfil</a></p>
                <button ng-show="login.isLoggedIn" class="btn btn-primary" ng-click="logout()">logout</button>
              </div>
            </div>
            <div class="know-more">
            </div>
          </div>
          <!--<div class="card-footer">
            <a href="#" class="btn btn-primary">Find Out More!</a>
          </div>-->
        </div>
      </div>
      <div ng-if="login.isLoggedIn&&login.whoami.groups.includes(1)">
          <a href="/accounting/"><i class="fa fa-chart-bar"></i> Tesorería</a>
      </div>
    </div>


</div>

{{< /rawhtml >}}


