+++
title = "Registro"
description = "Registrate"
author = "LaCartaDaurada"
date = "2023-09-14"
hidesubtitle = true
+++

{{< rawhtml >}}

<div class="container">
  <div ng-controller="SignupController">
    <form name="signupForm" novalidate>
      <div class="mb-3">
        <label for="username" class="form-label">Usuario</label>{{signupModel.username}}
        <input type="text" class="form-control" id="username" name="username" aria-describedby="usernameHelp" placeholder="Usuario" ng-pattern="/^[a-zà-ú][a-zà-ú0-9]+$/" ng-model="signupModel.username" required>
        <small ng-if="signupForm.username.$error.pattern" ng-class="{'text-danger': signupForm.username.$error.pattern&&signupForm.$submitted}" id="usernameHelp" class="form-text">Solo son válidas las minúsculas y números, y la primera letra no puede ser un número.</small>
      </div>
      <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" placeholder="Email" ng-model="signupModel.email">
        <small ng-if="!(signupForm.email.$error.email&&signupForm.$submitted)" id="emailHelp" class="form-text">El email no es obligatorio..</small>
        <span ng-if="signupForm.email.$error.email&&signupForm.$submitted" class="form-text text-danger">Tiene que ser un email válido</span>
      </div>
      <div class="mb-3">
        <label for="password" class="form-label">Contraseña</label>
        <input type="password" class="form-control" id="password" placeholder="Contraseña" ng-model="signupModel.password" required>
      </div>
      <div class="mb-3">
        <label for="password_confirm" class="form-label">Confirma la Contraseña</label>
        <input type="password" ng-class="{'ng-invalid': signupForm.$submitted&&signupModel.password!=signupModel.password_confirm}" class="form-control" id="password_confirm" placeholder="Contraseña" ng-model="signupModel.password_confirm" required>
        <small ng-if="!(signupModel.password!=signupModel.password_confirm&&signupForm.$submitted)" id="emailHelp" class="form-text">Para asegurarse de que la has escrito bien.</small>
        <span ng-if="signupModel.password!=signupModel.password_confirm&&signupForm.$submitted" class="form-text text-danger">Las contraseñas no coinciden!</span>
      </div>
      <button type="submit" ng-click="submit(signupForm)" class="btn btn-primary">Vamos</button>
      <button type="submit" ng-click="cancelSignup()" class="btn btn-secondary">Cancelar</button>
    </form>
    <div class="mt-3 alert alert-danger" role="alert" ng-if="status" ng-cloak>
      <i class="fas fa-exclamation-circle"></i> {{ status }}
    </div>
  </div>
</div>

{{< /rawhtml >}}
