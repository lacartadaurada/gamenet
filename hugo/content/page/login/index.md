+++
title = "Login"
description = "Entra al sitio"
hidesubtitle = true
+++

{{< rawhtml >}}

<div class="container-md">
  <div ng-controller="LoginController">
    <form name="loginForm" novalidate>
      <div class="mb-3">
        <div class="xcol-1">
          <label for="exampleInputEmail1" class="form-label">Usuario</label>
        </div>
        <div class="xcol-6">
          <input type="text" class="form-control" aria-describedby="usernameHelp" placeholder="Email o usuario" ng-model="loginModel.user_id" required autofocus>
        </div>
      </div>
      <div class="mb-3">
        <div class="xcol-1">
          <label for="exampleInputPassword1" class="form-label">Contraseña</label>
        </div>
        <div class="xcol-6">
          <input type="password" class="form-control" placeholder="Password" ng-model="loginModel.password" required>
        </div>
      </div>
      <button type="submit" ng-click="submit(loginForm)" class="btn btn-primary">Vamos</button>
      <button type="submit" ng-click="cancelLogin()" class="btn btn-secondary">Cancelar</button>
    </form>
    <div class="mt-3 alert alert-danger" role="alert" ng-if="status" ng-cloak>
      <i class="fas fa-exclamation-circle"></i> {{ status }}
    </div>
  </div>
</div>

{{< /rawhtml >}}
