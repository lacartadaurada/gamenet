---
slug: "flask"
pagetitle: "{% block title %}{% endblock %} - La Carta Daurada"
title: "La Carta Daurada"
hidetitle: true
hidesubtitle: true
angularapp: true
nocard: true
noseo: true
description: "Asociación Cultural. Juegos de mesa, estrategia y rol."
aliases: ["about-us","contact", "acerca-de"]
---

{{< rawhtml >}}
                                                <section class="content">
                                                        <header>
                                                                {% block header %}{% endblock %}
                                                        </header>
                                                        {% block content %}{% endblock %}
                                                </section>

{{< /rawhtml >}}
